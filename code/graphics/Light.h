#ifndef LIGHT_H
#define LIGHT_H

#include <glm/glm.hpp>
#include "Shader.h"



struct DirLight
{
	//real32 strength;

	glm::vec3 direction;

	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	
	void Render(Shader shader);
};

struct PointLight
{
	//real32 strength;
	glm::vec3 position;

	// attenuation const
	real32 constant;
	real32 linear;
	real32 quadratic;

	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

	void Render(Shader shader, int32 idx);
};

struct SpotLight
{
	glm::vec3 position;
	glm::vec3 direction;

	real32 cutOff;
	real32 outerCutOff;

	// attenuation const
	real32 constant;
	real32 linear;
	real32 quadratic;

	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;

	void Render(Shader shader, int32 idx);
};

#endif
