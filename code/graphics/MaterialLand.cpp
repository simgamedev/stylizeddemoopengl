#include "MaterialLand.h"


MaterialLand::MaterialLand(string blendMapPath,
						   string diffuseLayer1Path,
						   string specularLayer1Path,
						   string normalLayer1Path,
						   string diffuseLayer2Path,
						   string specularLayer2Path,
						   string normalLayer2Path,
						   string diffuseLayer3Path,
						   string specularLayer3Path,
						   string normalLayer3Path,
						   string diffuseLayer4Path,
						   string specularLayer4Path,
						   string normalLayer4Path,
						   glm::vec2 uvScale)
	: blendMapPath_(blendMapPath)
	, diffuseLayer1Path_(diffuseLayer1Path)
	, specularLayer1Path_(specularLayer1Path)
	, normalLayer1Path_(normalLayer1Path)
	, diffuseLayer2Path_(diffuseLayer2Path)
	, specularLayer2Path_(specularLayer2Path)
	, normalLayer2Path_(normalLayer2Path)
	, diffuseLayer3Path_(diffuseLayer3Path)
	, specularLayer3Path_(specularLayer3Path)
	, normalLayer3Path_(normalLayer3Path)
	, diffuseLayer4Path_(diffuseLayer4Path)
	, specularLayer4Path_(specularLayer4Path)
	, normalLayer4Path_(normalLayer4Path)
	, uvScale_(uvScale)
{
}

void
MaterialLand::LoadTextures()
{
	// blendmap
	string blendMapDirectory = blendMapPath_.substr(0, blendMapPath_.find_last_of("/"));
	string blendMapFilename = blendMapPath_.substr(blendMapPath_.find_last_of("/") + 1, blendMapPath_.length());
	textures_.push_back(LoadTexture(blendMapDirectory,
									blendMapFilename,
									aiTextureType_AMBIENT,
									false));
	// layer1
	string diffuseLayer1Directory = diffuseLayer1Path_.substr(0, diffuseLayer1Path_.find_last_of("/"));
	string diffuseLayer1Filename = diffuseLayer1Path_.substr(diffuseLayer1Path_.find_last_of("/") + 1, diffuseLayer1Path_.length());
	textures_.push_back(LoadTexture(diffuseLayer1Directory,
									diffuseLayer1Filename,
									aiTextureType_DIFFUSE,
									false));

	string specularLayer1Directory = specularLayer1Path_.substr(0, specularLayer1Path_.find_last_of("/"));
	string specularLayer1Filename = specularLayer1Path_.substr(specularLayer1Path_.find_last_of("/") + 1, specularLayer1Path_.length());
	textures_.push_back(LoadTexture(specularLayer1Directory,
									specularLayer1Filename,
									aiTextureType_SPECULAR,
									false));

	string normalLayer1Directory = normalLayer1Path_.substr(0, normalLayer1Path_.find_last_of("/"));
	string normalLayer1Filename = normalLayer1Path_.substr(normalLayer1Path_.find_last_of("/") + 1, normalLayer1Path_.length());
	textures_.push_back(LoadTexture(normalLayer1Directory,
									normalLayer1Filename,
									aiTextureType_NORMALS,
									false));
	// layer2
	string diffuseLayer2Directory = diffuseLayer2Path_.substr(0, diffuseLayer2Path_.find_last_of("/"));
	string diffuseLayer2Filename = diffuseLayer2Path_.substr(diffuseLayer2Path_.find_last_of("/") + 1, diffuseLayer2Path_.length());
	textures_.push_back(LoadTexture(diffuseLayer2Directory,
									diffuseLayer2Filename,
									aiTextureType_DIFFUSE,
									false));

	string specularLayer2Directory = specularLayer2Path_.substr(0, specularLayer2Path_.find_last_of("/"));
	string specularLayer2Filename = specularLayer2Path_.substr(specularLayer2Path_.find_last_of("/") + 1, specularLayer2Path_.length());
	textures_.push_back(LoadTexture(specularLayer2Directory,
									specularLayer2Filename,
									aiTextureType_SPECULAR,
									false));

	string normalLayer2Directory = normalLayer2Path_.substr(0, normalLayer2Path_.find_last_of("/"));
	string normalLayer2Filename = normalLayer2Path_.substr(normalLayer2Path_.find_last_of("/") + 1, normalLayer2Path_.length());
	textures_.push_back(LoadTexture(normalLayer2Directory,
									normalLayer2Filename,
									aiTextureType_NORMALS,
									false));
	// layer3
	string diffuseLayer3Directory = diffuseLayer3Path_.substr(0, diffuseLayer3Path_.find_last_of("/"));
	string diffuseLayer3Filename = diffuseLayer3Path_.substr(diffuseLayer3Path_.find_last_of("/") + 1, diffuseLayer3Path_.length());
	textures_.push_back(LoadTexture(diffuseLayer3Directory,
									diffuseLayer3Filename,
									aiTextureType_DIFFUSE,
									false));

	string specularLayer3Directory = specularLayer3Path_.substr(0, specularLayer3Path_.find_last_of("/"));
	string specularLayer3Filename = specularLayer3Path_.substr(specularLayer3Path_.find_last_of("/") + 1, specularLayer2Path_.length());
	textures_.push_back(LoadTexture(specularLayer3Directory,
									specularLayer3Filename,
									aiTextureType_SPECULAR,
									false));

	string normalLayer3Directory = normalLayer3Path_.substr(0, normalLayer3Path_.find_last_of("/"));
	string normalLayer3Filename = normalLayer3Path_.substr(normalLayer3Path_.find_last_of("/") + 1, normalLayer3Path_.length());
	textures_.push_back(LoadTexture(normalLayer3Directory,
									normalLayer3Filename,
									aiTextureType_NORMALS,
									false));
	// layer4
	string diffuseLayer4Directory = diffuseLayer4Path_.substr(0, diffuseLayer4Path_.find_last_of("/"));
	string diffuseLayer4Filename = diffuseLayer4Path_.substr(diffuseLayer4Path_.find_last_of("/") + 1, diffuseLayer4Path_.length());
	textures_.push_back(LoadTexture(diffuseLayer4Directory,
									diffuseLayer4Filename,
									aiTextureType_DIFFUSE,
									false));

	string specularLayer4Directory = specularLayer4Path_.substr(0, specularLayer4Path_.find_last_of("/"));
	string specularLayer4Filename = specularLayer4Path_.substr(specularLayer4Path_.find_last_of("/") + 1, specularLayer2Path_.length());
	textures_.push_back(LoadTexture(specularLayer4Directory,
									specularLayer4Filename,
									aiTextureType_SPECULAR,
									false));

	string normalLayer4Directory = normalLayer4Path_.substr(0, normalLayer4Path_.find_last_of("/"));
	string normalLayer4Filename = normalLayer4Path_.substr(normalLayer4Path_.find_last_of("/") + 1, normalLayer4Path_.length());
	textures_.push_back(LoadTexture(normalLayer4Directory,
									normalLayer4Filename,
									aiTextureType_NORMALS,
									false));
	for (Texture tex : textures_)
		tex.Load();
}

Texture
MaterialLand::LoadTexture(string directory, string filename, aiTextureType type, bool hasTransparency)
{
	Texture tex(directory, filename, type, hasTransparency);
	return tex;
}
