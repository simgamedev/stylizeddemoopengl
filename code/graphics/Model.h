#ifndef MODEL_H
#define MODEL_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtc/matrix_transform.hpp>


#include <vector>
#include "Mesh.h"

#include "../physics/RigidBody.h"
#include "../physics/Bounds.h"
#include "Material.h"
#include "MaterialLand.h"


#define DYNAMIC_DRAW	(uint8)1
#define STATIC_DRAW		(uint8)2
#define NO_TEXTURE		(uint8)4
#define HAS_TRANSPARENCY (uint8)8
#define IS_LANDSCAPE (uint8)16
#define ALL_MESHES_SHARE_ONE_MAT (uint8)32

enum class ModelSrcType {
	FBX,
	OBJ,
	GLTF,
};




class Scene;
class Model
{
	friend Mesh;
public:
	Model(string modelID,
		  BoundType boundType,
		  uint32 maxNumInstances,
		  uint8 flags = 0);
	void LoadFromFile(std::string filepath);
	virtual void Init();
	virtual void Render(Shader shader, real32 dt, Scene* scene);
	RigidBody& GetRigidBody() { return rigidBody_; }
	RigidBody* SpawnInstance(glm::vec3 pos, glm::vec3 size, glm::vec3 rotation, real32 mass);
	void InitInstances();
	void RemoveInstance(string instanceID);
	void RemoveInstance(uint32 idx);
	RigidBody* GetInstance(const string& instanceID);
	RigidBody* GetInstance(const int32 index);
	int32 GetInstanceIdx(string instanceID);
	string GetModelID() const { return modelID_; }
	std::vector<BoundingRegion> GetBoundingRegions() { return boundingRegions_; }
	uint32 GetNumInstances() { return numInstances_; }
	void Cleanup();
	// textures
	bool AssignMaterial(MaterialLand* material);
	bool AssignMaterial(Material* material);
	bool HasTransparency();
	ModelSrcType GetSrcType() const { return srcType_; }
	//std::vector<std::pair<string, Material*>> GetMaterials() { return materials_; }
	void AddNewMatSlot(const string& matName);
	bool IsLandscape();
protected:
	string modelID_;
	bool bNoTex_;
	BoundType boundType_;
	RigidBody rigidBody_;
	std::string filename_;
	std::string directory_;

	std::vector<Mesh> meshes_;
	std::vector<BoundingRegion> boundingRegions_;

	std::vector<Texture> texturesLoaded_;
	// assimp
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
protected:
	std::vector<Texture> loadTextures(aiMaterial* mat, aiTextureType type, bool hasTransparency = false);
	BufferObject posVBO_;
	BufferObject sizeVBO_;
	BufferObject rotationVBO_;
	BufferObject modelMatricesVBO_;
	BufferObject normalMatricesVBO_;
	uint8 flags_;

	std::vector<RigidBody*> instances_;
	uint32 maxNumInstances_;
	uint32 numInstances_;
	ModelSrcType srcType_;

	bool NewMatSlot(string matName);
	std::vector<std::pair<string, Material*>> materials_;
	MaterialLand* materialLand_;
	//std::map<string, Material*> materials_;
	uint32 numMaterials_;
	uint32 numMatSlots_;
	int32 nextMatSlot_;
};

#endif
