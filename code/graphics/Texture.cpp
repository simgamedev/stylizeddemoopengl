#include "Texture.h"

#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


Texture::Texture()
	: bHasTransparency_(false)
{}

Texture::Texture(string directory, string filename, aiTextureType type, bool hasTransparency)
	: directory_(directory)
	, filename_(filename)
	, type_(type)
	, bHasTransparency_(hasTransparency)
{
	Generate();
}

void
Texture::Generate()
{
	glGenTextures(1, &id_);
}

void
Texture::Load(bool flip)
{
	stbi_set_flip_vertically_on_load(flip);


	int32 width, height, nChannels;
	unsigned char* data = stbi_load((directory_ + "/" + filename_).c_str(), &width, &height, &nChannels, 0);


	GLenum pixelDataFormat = GL_RGB;
	switch(nChannels)
	{
		case 1:
		{
			pixelDataFormat = GL_RED;
		} break;

		case 4:
		{
			pixelDataFormat = GL_RGBA;
		} break;
	}

	if(data)
	{
		glBindTexture(GL_TEXTURE_2D, id_);
		glTexImage2D(GL_TEXTURE_2D, 0, pixelDataFormat, width, height, 0, pixelDataFormat,
					 GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		
		/*
		string fileExtenstion = filename_.substr(filename_.find_last_of('.') + 1, filename_.length());
		if (fileExtenstion == "TGA" || fileExtenstion == "tga")
		{
			bHasTransparency_ = false;
		}
		*/
	}
	else
	{
		std::cout << "Failed to load image: " 
			<< directory_ 
			<< "/"
			<< filename_ 
			<< std::endl;
	}

	stbi_image_free(data);
}


void
Texture::Bind()
{
	glBindTexture(GL_TEXTURE_2D, id_);
}
