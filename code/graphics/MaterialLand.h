#ifndef MATERIAL_LAND_H
#define MATERIAL_LAND_H

#include "Texture.h"
#include <vector>
#include <glm/glm.hpp>
#include "../algorithms/Bitwise.hpp"

class MaterialLand
{
public:
	// only with Diffuse map
	MaterialLand(string blendMapPath,
				 string diffuseLayer1Path,
				 string specularLayer1Path,
				 string normalLayer1Path,
				 string diffuseLayer2Path,
				 string specularLayer2Path,
				 string normalLayer2Path,
				 string diffuseLayer3Path,
				 string specularLayer3Path,
				 string normalLayer3Path,
				 string diffuseLayer4Path,
				 string specularLayer4Path,
				 string normalLayer4Path,
				 glm::vec2 uvScale = glm::vec2(1.0f, 1.0f));
	void LoadTextures();
	glm::vec2 GetUVScale() { return uvScale_; }
	uint32 GetNumLayers() { return numLayers_; }
	std::vector<Texture>& GetTextures() { return textures_; }
private:
	std::vector<Texture> textures_;

	string blendMapPath_;

	string diffuseLayer1Path_;
	string specularLayer1Path_;
	string normalLayer1Path_;

	string diffuseLayer2Path_;
	string specularLayer2Path_;
	string normalLayer2Path_;

	string diffuseLayer3Path_;
	string specularLayer3Path_;
	string normalLayer3Path_;

	string diffuseLayer4Path_;
	string specularLayer4Path_;
	string normalLayer4Path_;

	glm::vec2 uvScale_;
	uint32 numLayers_;
	uint8 activeTextures_;

	Texture LoadTexture(string directory, string filename, aiTextureType type, bool hasTransparency);
};
#endif
