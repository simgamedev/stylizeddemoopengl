#ifndef TEXTURE_H
#define TEXTURE_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <assimp/scene.h>

#include "../common.h"


class Texture
{
public:
	Texture();
	Texture(string directory, string filename, aiTextureType type, bool hasTransparency = false);

	void Bind();
	int32 GetID() const { return id_;  }
	string GetFilename() const { return filename_; }
	void Load(bool flip = true);
	aiTextureType GetType() const { return type_; }
	bool HasTransparency() const { return bHasTransparency_; }
private:
	void Generate();
	uint32 id_;
	aiTextureType type_;
	string directory_;
	string filename_;
	bool bHasTransparency_;
};

#endif
