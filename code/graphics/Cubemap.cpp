#include "Cubemap.h"


Cubemap::Cubemap()
	: bHasTextures_(false)
{
	
}

void
Cubemap::LoadTextures(string directory,
					  string front,
					  string right,
					  string back,
					  string left,
					  string top,
					  string bottom)
{
	directory_ = directory;
	bHasTextures_ = true;
	textureNames_ = { right, left, top, bottom, front, back };

	glGenTextures(1, &texID_);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texID_);

	int32 width, height, nChannels;

	for(uint32 i = 0; i < 6; i++)
	{
		uint8* data = stbi_load((directory_ + "/" + textureNames_[i]).c_str(),
								&width, &height, &nChannels, 0);
		GLenum colorMode = GL_RED;
		switch(nChannels) {
			case 3:
			{
				colorMode = GL_RGB;
			} break;
			case 4:
			{
				colorMode = GL_RGBA;
			} break;
		}

		if(data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
						 0, colorMode, width, height, 0, colorMode, GL_UNSIGNED_BYTE, data);
		}
		else
		{
			std::cout << "Failed to load texture at "
					  << directory_
					  << "/"
					  << textureNames_[i]
					  << std::endl;
		}

		stbi_image_free(data);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

}

void
Cubemap::Init()
{
	real32 skyboxVertices[] = {
        // positions          
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
 
        -1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,
 
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
 
        -1.0f, -1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,
 
        -1.0f,  1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,
 
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f,  1.0f
    };

	// vao
	vao_.Generate();
	vao_.Bind();

	// vbo
	vao_["VBO"] = BufferObject(GL_ARRAY_BUFFER);
	vao_["VBO"].Generate();
	vao_["VBO"].Bind();
	vao_["VBO"].SetData<real32>(36 * 3, skyboxVertices, GL_STATIC_DRAW);

	vao_["VBO"].SetAttribPointer<GLfloat>(0, 3, GL_FLOAT, 3, 0);
	vao_["VBO"].UnBind();

	vao_.UnBind();
}

void
Cubemap::Render(Shader shader, Scene* scene)
{
	glDepthMask(GL_FALSE);

	shader.Use();

	// remove translation from the view matrix
	glm::mat4 view = glm::mat4(glm::mat3(scene->GetActiveCamera()->GetViewMatrix()));
	shader.SetMat4("view", view);
	shader.SetMat4("projection", scene->GetProjectionMatrix());

	if(bHasTextures_)
	{
		glBindTexture(GL_TEXTURE_CUBE_MAP, texID_);
	}

	vao_.Bind();
	vao_.Draw(GL_TRIANGLES, 0, 36);
	vao_.UnBind();


	glDepthMask(GL_TRUE);
}

void
Cubemap::Cleanup()
{
	vao_.Cleanup();
}


