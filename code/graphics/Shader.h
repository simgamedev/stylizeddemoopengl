#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h>
#include <fstream>
#include <sstream>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <assimp/scene.h>

#include "../common.h"

class Shader
{
public:
	Shader(const string& name,
		   const string& vertexShaderPath,
		   const string& fragmentShaderPath);
	void Use();
	uint32 GetID() { return id_; }

	string LoadShaderSrc(const char* filepath);
	GLuint CompileShader(const char* filepath, GLenum type);

	// uniforms
	void SetMat3(const string& name, glm::mat3 val);
	void SetMat4(const string& name, glm::mat4 val);
	void SetInt(const string& name, int32 val);
	void SetBool(const string& name, bool val);
	void SetFloat(const string& name, real32 val);
	void Set2Float(const std::string& name, real32 x, real32 y);
	void Set2Float(const std::string& name, glm::vec2 val);
	void Set3Float(const std::string& name, glm::vec3 val);
	void Set3Float(const std::string& name, real32 x, real32 y, real32 z);
	void Set4Float(const std::string& name, real32 x, real32 y, real32 z, real32 w);
	void Set4Float(const std::string& name, aiColor4D color);
	void Set4Float(const std::string& name, glm::vec4 val);
private:
	uint32 id_;
	string name_;
	string vertShaderPath_;
	string fragShaderPath_;
};

#endif

