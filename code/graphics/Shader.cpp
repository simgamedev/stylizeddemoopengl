#include "Shader.h"

Shader::Shader(const string& name,
			   const string& vertexShaderPath,
			   const string& fragmentShaderPath)
	: name_(name)
	, vertShaderPath_(vertexShaderPath)
	, fragShaderPath_(fragmentShaderPath)
{
	int32 success;
	char infoLog[512];

	GLuint vertexShader = CompileShader(vertexShaderPath.c_str(), GL_VERTEX_SHADER);
	GLuint fragmentShader = CompileShader(fragmentShaderPath.c_str(), GL_FRAGMENT_SHADER);

	id_ = glCreateProgram();
	glAttachShader(id_, vertexShader);
	glAttachShader(id_, fragmentShader);
	glLinkProgram(id_);

	// link error
	glGetProgramiv(id_, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(id_, 512, NULL, infoLog);
		std::cout << "Shader linking error: " << infoLog << std::endl;
	}
	// link error

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void
Shader::Use()
{
	//std::cout << "Using shader: " << name_ << ", id: " << id_ << std::endl;
	glUseProgram(id_);
}


string
Shader::LoadShaderSrc(const char* filepath)
{
	std::ifstream file;
	std::stringstream buf;
	string ret = "";


	file.open(filepath);
	if (file.is_open())
	{
		buf << file.rdbuf();
		ret = buf.str();
	}
	else
	{
		std::cout << "Failed to open file:" << filepath << std::endl;
	}

	file.close();
	return ret;

}

GLuint
Shader::CompileShader(const char* filepath, GLenum type)
{
	int32 success;
	char infoLog[512];

	GLuint ret = glCreateShader(type);
	string shaderSrcStr = LoadShaderSrc(filepath);
	const GLchar* shaderSrc = shaderSrcStr.c_str();

	glShaderSource(ret, 1, &shaderSrc, NULL);
	glCompileShader(ret);

	// shader error
	glGetShaderiv(ret, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(ret, 512, NULL, infoLog);
		std::cout << "Error with shader compilation: " << filepath << std::endl <<
		infoLog << std::endl;
	}

	return ret;
}

void
Shader::SetMat4(const string& name, glm::mat4 val)
{
	glUniformMatrix4fv(glGetUniformLocation(id_, name.c_str()),
					   1, GL_FALSE, glm::value_ptr(val));
}

void
Shader::SetMat3(const string& name, glm::mat3 val)
{
	glUniformMatrix3fv(glGetUniformLocation(id_, name.c_str()),
					   1, GL_FALSE, glm::value_ptr(val));
}

void
Shader::SetInt(const string& name, int32 val)
{
	glUniform1i(glGetUniformLocation(id_, name.c_str()),
				val);
}

void
Shader::SetBool(const string& name, bool val)
{
	glUniform1i(glGetUniformLocation(id_, name.c_str()),
				(int32)val);
}

void
Shader::SetFloat(const string& name, real32 val)
{
	glUniform1f(glGetUniformLocation(id_, name.c_str()),
				val);
}

void
Shader::Set2Float(const std::string& name, real32 x, real32 y)
{
	glUniform2f(glGetUniformLocation(id_, name.c_str()), x, y);
}

void
Shader::Set2Float(const std::string& name, glm::vec2 val)
{
	Set2Float(name, val.x, val.y);
}


void
Shader::Set3Float(const std::string& name, glm::vec3 val)
{
	Set3Float(name, val.x, val.y, val.z);
}

void
Shader::Set3Float(const std::string& name, real32 x, real32 y, real32 z)
{
	glUniform3f(glGetUniformLocation(id_, name.c_str()), x, y, z);
}


void
Shader::Set4Float(const std::string& name, real32 x, real32 y, real32 z, real32 w)
{
	glUniform4f(glGetUniformLocation(id_, name.c_str()),
				x, y, z, w);
}

void
Shader::Set4Float(const std::string& name, aiColor4D color)
{
	glUniform4f(glGetUniformLocation(id_, name.c_str()), color.r,
				color.g,
				color.b,
				color.a);
}
void
Shader::Set4Float(const std::string& name, glm::vec4 val)
{
	glUniform4f(glGetUniformLocation(id_, name.c_str()), val.x,
				val.y,
				val.z,
				val.w);
}
