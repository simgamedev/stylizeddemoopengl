#include "Material.h"


Material::Material(const string& name,
				   const string& diffusePath,
				   bool hasTransparency,
				   glm::vec2 uvScale)
	: diffusePath_(diffusePath)
	, activeTextures_(0)
	, uvScale_(uvScale)
	, bHasTransparency_(hasTransparency)
	, name_("")
{
}

Material::Material(const string& name,
				   const string& diffusePath,
				   const string& normalPath,
				   bool hasTransparency,
				   glm::vec2 uvScale)
	: diffusePath_(diffusePath)
	, normalPath_(normalPath)
	, uvScale_(uvScale)
	, bHasTransparency_(hasTransparency)
	, name_("")
{
	Bitwise::TurnOnBitByFlag<uint8>(&activeTextures_, MAT_HAS_NORMAL_TEXTURE);
}

Material::Material(const string& name,
				   const string& diffusePath,
				   const string& specularPath,
				   const string& normalPath,
				   bool hasTransparency,
				   glm::vec2 uvScale)
	: diffusePath_(diffusePath)
	, specularPath_(specularPath)
	, normalPath_(normalPath)
	, uvScale_(uvScale)
	, bHasTransparency_(hasTransparency)
	, name_("")
{
	Bitwise::TurnOnBitByFlag<uint8>(&activeTextures_, MAT_HAS_NORMAL_TEXTURE);
	Bitwise::TurnOnBitByFlag<uint8>(&activeTextures_, MAT_HAS_SPECULAR_TEXTURE);
}

std::vector<Texture>&
Material::GetTextures()
{
	return textures_;
}

void
Material::LoadTextures()
{
	string diffuseDirectory = diffusePath_.substr(0, diffusePath_.find_last_of("/"));
	string diffuseFilename = diffusePath_.substr(diffusePath_.find_last_of("/") + 1, diffusePath_.length());
	Texture diffuse(diffuseDirectory, diffuseFilename, aiTextureType_DIFFUSE, bHasTransparency_);
	diffuse.Load();
	textures_.push_back(diffuse);

	if(HasSpecularMap())
	{
		string specularDirectory = specularPath_.substr(0, specularPath_.find_last_of("/"));
		string specularFilename = specularPath_.substr(specularPath_.find_last_of("/") + 1, specularPath_.length());
		Texture specular(specularDirectory, specularFilename, aiTextureType_SPECULAR);
		specular.Load();
		textures_.push_back(specular);
	}

	if(HasNormalMap())
	{
		string normalDirectory = normalPath_.substr(0, normalPath_.find_last_of("/"));
		string normalFilename = normalPath_.substr(normalPath_.find_last_of("/") + 1, normalPath_.length());
		Texture normal(normalDirectory, normalFilename, aiTextureType_NORMALS);
		normal.Load();
		textures_.push_back(normal);
	}
}

void
Material::SetUVScale(glm::vec2 uvScale)
{
	uvScale_ = uvScale;
}

bool
Material::HasNormalMap()
{
	return Bitwise::IsBitOnByFlag<uint8>(&activeTextures_, MAT_HAS_NORMAL_TEXTURE);
}

bool
Material::HasSpecularMap()
{
	return Bitwise::IsBitOnByFlag<uint8>(&activeTextures_, MAT_HAS_SPECULAR_TEXTURE);
}
