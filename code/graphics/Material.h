#ifndef MATERIAL_H
#define MATERIAL_H

#include "Texture.h"
#include <vector>
#include <glm/glm.hpp>
#include "../algorithms/Bitwise.hpp"

#define MAT_HAS_NORMAL_TEXTURE (uint8)1
#define MAT_HAS_SPECULAR_TEXTURE (uint8)2

class Material
{
public:
	// only with Diffuse map
	Material(const string& name,
			 const string& diffsuePath,
			 bool hasTransparency = false,
			 glm::vec2 uvScale = glm::vec2(1.0f, 1.0f));
	// Diffuse&Normal map
	Material(const string& name,
			 const string& diffusePath,
			 const string& normalPath,
			 bool hasTransparency = false,
			 glm::vec2 uvScale = glm::vec2(1.0f, 1.0f));
	// Diffsue&Specular&Normal map
	Material(const string& name,
			const string& diffusePath,
			 const string& normalPath,
		     const string& specularPath,
			 bool hasTransparency = false,
			 glm::vec2 uvScale = glm::vec2(1.0f, 1.0f));
	void LoadTextures();
	std::vector<Texture>& GetTextures();
	void SetUVScale(glm::vec2 uvScale);
	glm::vec2 GetUVScale() { return uvScale_; }
	bool HasNormalMap();
	bool HasSpecularMap();
	string GetName() { return name_; }
private:
	bool bHasTransparency_;
	std::vector<Texture> textures_;
	string diffusePath_;
	string specularPath_;
	string normalPath_;
	glm::vec2 uvScale_;
	uint8 activeTextures_;
	string name_;
};

#endif
