#ifndef MATERIAL_SIMPLE_H
#define MATERIAL_SIMPLE_H

#include "../common.h"
#include <glm/glm.hpp>

struct MaterialSimple {
    // lighting values
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    real32 shininess;

    static MaterialSimple emerald;
    static MaterialSimple jade;
    static MaterialSimple obsidian;
    static MaterialSimple pearl;
    static MaterialSimple ruby;
    static MaterialSimple turquoise;
    static MaterialSimple brass;
    static MaterialSimple bronze;
    static MaterialSimple chrome;
    static MaterialSimple copper;
    static MaterialSimple gold;
    static MaterialSimple silver;
    static MaterialSimple black_plastic;
    static MaterialSimple cyan_plastic;
    static MaterialSimple green_plastic;
    static MaterialSimple red_plastic;
    static MaterialSimple white_plastic;
    static MaterialSimple yellow_plastic;
    static MaterialSimple black_rubber;
    static MaterialSimple cyan_rubber;
    static MaterialSimple green_rubber;
    static MaterialSimple red_rubber;
    static MaterialSimple white_rubber;
    static MaterialSimple yellow_rubber;

    static MaterialSimple Mix(MaterialSimple m1, MaterialSimple m2, float mix = 0.5f);
};
#endif
