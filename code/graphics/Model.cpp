#include "Model.h"
#include "../Scene.h"
#include "../algorithms/Bitwise.hpp"
#include "../Utilities.h"
#include <glm/gtx/quaternion.hpp>



Model::Model(string modelID,
			 BoundType boundType,
			 uint32 maxNumInstances,
			 uint8 flags)
	: modelID_(modelID)
	, boundType_(boundType)
	, flags_(flags)
	, numInstances_(0)
	, maxNumInstances_(maxNumInstances)
	, numMatSlots_(0)
	, numMaterials_(0)
	, nextMatSlot_(0)
{
}

void
Model::Init()
{
}

void
Model::LoadFromFile(string filepath)
{
	// ===> Determine model source type
	string fileExtension_ = filepath.substr(filepath.find_last_of(".") + 1, filepath.length());
	if (fileExtension_ == "FBX" || fileExtension_ == "fbx")
		srcType_ = ModelSrcType::FBX;
	if (fileExtension_ == "OBJ" || fileExtension_ == "obj")
		srcType_ = ModelSrcType::OBJ;
	if (fileExtension_ == "GLTF" || fileExtension_ == "gltf")
		srcType_ = ModelSrcType::GLTF;
	// ===< Determine model source type

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(filepath, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "Failed to load model at "
			<< filepath << std::endl 
			<< importer.GetErrorString() << std::endl;
		return;
	}

	directory_ = filepath.substr(0, filepath.find_last_of("/"));
	processNode(scene->mRootNode, scene);
	if (modelID_ == "Container02")
		int32 a = 100;
}

void
Model::processNode(aiNode* node, const aiScene* scene)
{
	for (uint32 i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* aiMesh = scene->mMeshes[node->mMeshes[i]];
		Mesh newMesh = processMesh(aiMesh, scene);
		newMesh.pModel_ = this;
		meshes_.push_back(newMesh);
		// FIXME:
		boundingRegions_.push_back(newMesh.GetBoundingRegion());
	}

	// process all childen nodes
	for (uint32 i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}
}

Mesh
Model::processMesh(aiMesh* assimpMesh, const aiScene* scene)
{
	std::vector<Vertex> vertices;
	std::vector<uint32> indices;
	std::vector<Texture> textures;

	BoundingRegion br(boundType_);
	glm::vec3 brMin((real32)(~0));
	glm::vec3 brMax(-(real32)(~0));

	// ====================> LOAD VERTICES
	for (uint32 i = 0; i < assimpMesh->mNumVertices; i++)
	{
		Vertex vertex;
		// ===> POSITIONs
		vertex.pos = glm::vec3(
			assimpMesh->mVertices[i].x,
			assimpMesh->mVertices[i].y,
			assimpMesh->mVertices[i].z
		);
		// calculate bounding region
		for (int32 j = 0; j < 3; j++)
		{
			if (vertex.pos[j] < brMin[j])
				brMin[j] = vertex.pos[j];
			if (vertex.pos[j] > brMax[j])
				brMax[j] = vertex.pos[j];
		}
		// ===< POSITIONs

		// ===> NORMAL
		vertex.normal = glm::vec3(
			assimpMesh->mNormals[i].x,
			assimpMesh->mNormals[i].y,
			assimpMesh->mNormals[i].z
		);
		// ===< NORMAL

		// ===> TEXTURE COORDS
		if (assimpMesh->mTextureCoords[0])
		{
			// ===> NOTE: Inverted UV Coords(V only)
			if (srcType_ == ModelSrcType::GLTF)
			{
				vertex.texCoord = glm::vec2(
					assimpMesh->mTextureCoords[0][i].x,
					assimpMesh->mTextureCoords[0][i].y
				);
			}
			else if (srcType_ == ModelSrcType::FBX || 
					 srcType_ == ModelSrcType::OBJ)
			{
				vertex.texCoord = glm::vec2(
					assimpMesh->mTextureCoords[0][i].x,
					1.0f - assimpMesh->mTextureCoords[0][i].y
				);
			}
			// ===< NOTE: Inverted UV Coords(V only)
		}
		else
		{
			vertex.texCoord = glm::vec2(0.0f);
		}
		// ===< TEXTURE COORDS

		// ===> TANGENTS
		if(assimpMesh->mTangents)
		{
			vertex.tangent = {
				assimpMesh->mTangents[i].x,
				assimpMesh->mTangents[i].y,
				assimpMesh->mTangents[i].z
			};
		}
		// ===< TANGENTS
		// ===> TODO: mNumAnimMeshes, mAABB
		// ===< TODO: mNumAnimMehses, mAABB
		vertices.push_back(vertex);
	}
	// ====================< LOAD VERTICES

	// ====================> CREATE BOUDNING REGION
	if (boundType_ == BoundType::AABB)
	{
		// FIXME:
		br.SetMin(brMin);
		//br.SetOriginalMin(brMin);
		br.SetMax(brMax);
		//br.SetOriginalMax(brMax);
	}
	else
	{
		// FIXME:
		br.SetCenter(BoundingRegion(brMin, brMax).CalcCenter());
		real32 maxRadiusSquared = 0.0f;
		for (uint32 i = 0; i < assimpMesh->mNumVertices; i++)
		{
			real32 radiusSquared = 0.0f;
			for (int k = 0; k < 3; k++)
			{
				radiusSquared += Math::Square(vertices[i].pos[k] - br.GetCenter()[k]);
			}
			if (radiusSquared > maxRadiusSquared)
			{
				maxRadiusSquared = radiusSquared;
			}
		}
		// FIXME:
		br.SetRadius(Math::Sqrt(maxRadiusSquared));
	}
	// ====================< CREATE BOUNDING REGION

	// ====================> LOAD INDICES
	for (uint32 i = 0; i < assimpMesh->mNumFaces; i++)
	{
		aiFace face = assimpMesh->mFaces[i];
		for (uint32 j = 0; j < face.mNumIndices; j++)
		{
			indices.push_back(face.mIndices[j]);
		};
	}
	// ====================< LOAD INDICES

	Mesh result;
	// ====================> LOAD TEXTURES/MATERIALS
	if (assimpMesh->mMaterialIndex >= 0)
	{
		aiMaterial* aiMat = scene->mMaterials[assimpMesh->mMaterialIndex];

		if (modelID_ == "Container02")
			int32 a = 100;
		// ===> MESH HAS NO TEXTURE
		if (Bitwise::IsBitOnByFlag<uint8>(&flags_, NO_TEXTURE))
		{
			// diffuse color
			aiColor4D diffuseCol(1.0f);
			aiGetMaterialColor(aiMat, AI_MATKEY_COLOR_DIFFUSE, &diffuseCol);

			// specular color
			aiColor4D specularCol(1.0f);
			aiGetMaterialColor(aiMat, AI_MATKEY_COLOR_SPECULAR, &specularCol);

			result = Mesh(br, diffuseCol, specularCol);
		}
		// ===< MESH HAS NO TEXTURE
		// ===> ASSUME GLTF MESH HAS TEXTRE !!!!!!!!!!!!EMBEDED TEXTURE FOR GLTF
		else if(srcType_ == ModelSrcType::GLTF)
			// NOTE: you have to manuulay set materials/textures for fbx/obj models
		{
			// diffuse maps
			bool hasTransparency = Bitwise::IsBitOnByFlag<uint8>(&flags_, HAS_TRANSPARENCY);
			std::vector<Texture> diffuseMaps = loadTextures(aiMat, aiTextureType_DIFFUSE, hasTransparency);
			textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

			// specular maps
			std::vector<Texture> specularMaps = loadTextures(aiMat, aiTextureType_SPECULAR);
			textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

			// normal maps
			// NOTE: for .obj use aiTextureType_HEIGHT
			std::vector<Texture> normalMaps = loadTextures(aiMat, aiTextureType_NORMALS);
			textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());

			result = Mesh(br, textures);
		}
		// ===< ASSUME GLTF MESH HAS TEXTRE !!!!!!!!!!!!EMBEDED TEXTURE FOR GLTF
		// ===> ASSUME FBX/OBJ model has seperate manually assigned materials,
		// ===> SO we preallocate some material slots for it
		else if (srcType_ == ModelSrcType::FBX ||
				 srcType_ == ModelSrcType::OBJ)
		{
			aiString matName;
			aiMat->Get(AI_MATKEY_NAME, matName);
			NewMatSlot(string(matName.C_Str()));
		}
		// ===< ASSUME FBX/OBJ models have seperate manually assigned materials
	}
	// ====================< LOAD TEXTURES/MATERIALS

	result.LoadData(vertices, indices);
	return result;
}

std::vector<Texture>
Model::loadTextures(aiMaterial* mat, aiTextureType type, bool hasTransparency)
{
	std::vector<Texture> textures;

	for (uint32 i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString texFilename;
		mat->GetTexture(type, i, &texFilename);
		std::cout << directory_ << "/" << texFilename.C_Str() << std::endl;
		
		// prevent duplicates
		bool skip = false;
		for (uint32 j = 0; j < texturesLoaded_.size(); j++)
		{
			if (std::strcmp(texturesLoaded_[j].GetFilename().data(), texFilename.C_Str()) == 0)
			{
				textures.push_back(texturesLoaded_[j]);
				skip = true;
				break;
			}
		}

		if (!skip)
		{
			Texture tex(directory_, texFilename.C_Str(), type, hasTransparency);
			tex.Load(false);
			textures.push_back(tex);
			texturesLoaded_.push_back(tex);
		}
	}

	return textures;
}

RigidBody*
Model::SpawnInstance(glm::vec3 pos, glm::vec3 size, glm::vec3 rotation, real32 mass)
{
	if (numInstances_ >= maxNumInstances_)
	{
		return nullptr;
	}

	// new
	instances_.push_back(new RigidBody(modelID_, pos, size, rotation, mass));
	return instances_[numInstances_++];
}

void
Model::InitInstances()
{
	glm::mat4* modelMatricesData = nullptr;
	glm::mat3* normalMatricesData = nullptr;
	GLenum usage = GL_DYNAMIC_DRAW;

	std::vector<glm::mat4> modelMatrices;
	std::vector<glm::mat3> normalMatrices;

	if (Bitwise::IsBitOnByFlag(&flags_, STATIC_DRAW))
	{
		for (uint32 i = 0; i < numInstances_; i++)
		{
			// ==========> MODEL MATRIX
			glm::mat4 transMatrix = glm::translate(glm::mat4(1.0f), instances_[i]->GetPos());
			glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), instances_[i]->GetSize());
			// ===> QUATERNION WAY
			glm::vec3 eulerInRadians(glm::radians(instances_[i]->GetEulerRotation().x),
									 glm::radians(instances_[i]->GetEulerRotation().y),
									 glm::radians(instances_[i]->GetEulerRotation().z));
			glm::quat quatRot(eulerInRadians);
			glm::mat4 rotMatrix = glm::toMat4(quatRot);
			// NOTE: order must be SRT(glsl:MVP)(backward)
			glm::mat4 modelMatrix = transMatrix * rotMatrix * scaleMatrix;
			// ===< QUATERNION WAY
			// ===> EULER WAY
			//glm::mat4 rotMatrix = glm::rotate(glm::mat4(1.0f), instances_[i]->GetEulerRotation().y, glm::vec3(0.0f, 1.0f, 0.0f));
			//rotMatrix = glm::rotate(rotMatrix, instances_[i]->GetEulerRotation().z, glm::vec3(0.0f, 0.0f, 1.0f));
			//rotMatrix = glm::rotate(rotMatrix, instances_[i]->GetEulerRotation().x, glm::vec3(1.0f, 0.0f, 0.0f));
			// ===< EULER WAY
			modelMatrices.push_back(modelMatrix);
			// ==========< MODEL MATRIX
			// ===> NORMAL MATRIX
			normalMatrices.push_back(glm::transpose(glm::inverse(glm::mat3(modelMatrix))));
		}

		//if (positions.size() > 0)
		if(modelMatrices.size() > 0)
		{
			modelMatricesData = &modelMatrices[0];
			normalMatricesData = &normalMatrices[0];
		}

		usage = GL_STATIC_DRAW;
	}

	modelMatricesVBO_ = BufferObject(GL_ARRAY_BUFFER);
	modelMatricesVBO_.Generate();
	modelMatricesVBO_.Bind();
	modelMatricesVBO_.SetData<glm::mat4>(MAX_INSTANCES, modelMatricesData, usage);

	normalMatricesVBO_ = BufferObject(GL_ARRAY_BUFFER);
	normalMatricesVBO_.Generate();
	normalMatricesVBO_.Bind();
	normalMatricesVBO_.SetData<glm::mat3>(MAX_INSTANCES, normalMatricesData, usage);


	for (uint32 i = 0, size = meshes_.size(); i < size; i++)
	{
		// FIXME: reference
		meshes_[i].GetVAO().Bind();

		modelMatricesVBO_.Bind();
		// ===> TODO/FIXME: modify VBO::SetAttribPointer
		std::size_t vec4Size = sizeof(glm::vec4);
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(1 * vec4Size));
		glEnableVertexAttribArray(6);
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
		glEnableVertexAttribArray(7);
		glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));
		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);
		glVertexAttribDivisor(6, 1);
		glVertexAttribDivisor(7, 1);
		// ===<

		// ===>
		normalMatricesVBO_.Bind();
		// ===> TODO/FIXME: modify VBO::SetAttribPointer
		std::size_t vec3Size = sizeof(glm::vec3);
		glEnableVertexAttribArray(8);
		glVertexAttribPointer(8, 3, GL_FLOAT, GL_FALSE, 3 * vec3Size, (void*)0);
		glEnableVertexAttribArray(9);
		glVertexAttribPointer(9, 3, GL_FLOAT, GL_FALSE, 3 * vec3Size, (void*)(1 * vec3Size));
		glEnableVertexAttribArray(10);
		glVertexAttribPointer(10, 3, GL_FLOAT, GL_FALSE, 3 * vec3Size, (void*)(2 * vec3Size));
		glVertexAttribDivisor(8, 1);
		glVertexAttribDivisor(9, 1);
		glVertexAttribDivisor(10, 1);
		// ===<

		meshes_[i].GetVAO().UnBind();
	}
}

void
Model::RemoveInstance(string instanceID)
{
	int32 idx = GetInstanceIdx(instanceID);
	if (idx != -1)
	{
		instances_.erase(instances_.begin() + idx);
		numInstances_--;
	}
}

void
Model::RemoveInstance(uint32 idx)
{
	instances_.erase(instances_.begin() + idx);
	numInstances_--;
}

int32
Model::GetInstanceIdx(string instanceID)
{
	for (uint32 i = 0; i < numInstances_; i++)
	{
		if (instances_[i]->GetInstanceID() == instanceID)
			return i;
	}

	return -1;
}



void
Model::Render(Shader shader, real32 dt, Scene* scene)
{
	// FIXME: shader.Use()
	shader.Use();
	// ===> DYANMIC DRAW
	if (!Bitwise::IsBitOnByFlag(&flags_, STATIC_DRAW))
	{
		// update vbo data
		std::vector<glm::vec3> positions, sizes;
		bool doUpdate = Bitwise::IsBitOnByFlag(&flags_, DYNAMIC_DRAW);

		for (int32 i = 0; i < numInstances_; i++)
		{
			if (doUpdate)
			{
				instances_[i]->Update(dt);
				Bitwise::TurnOnBitByFlag(instances_[i]->GetState(), INSTANCE_MOVED);
			}
			else
			{
				Bitwise::TurnOffBitByFlag(instances_[i]->GetState(), INSTANCE_MOVED);
			}

			positions.push_back(instances_[i]->GetPos());
			sizes.push_back(instances_[i]->GetSize());
		}

		posVBO_.Bind();
		posVBO_.UpdateData<glm::vec3>(0, numInstances_, &positions[0]);

		sizeVBO_.Bind();
		sizeVBO_.UpdateData<glm::vec3>(0, numInstances_, &sizes[0]);
	}
	// ===< DYANMIC DRAW

	// ===> TEMP
	if (Bitwise::IsBitOnByFlag(&flags_, NO_TEXTURE))
		shader.SetFloat("materialSimple.shininess", 0.5f);
	else
		shader.SetFloat("material.shininess", 0.5f);
	// ===< REMOVE
	shader.SetBool("hasTransparency", false);
	//shader.Set2Float("uvScale", glm::vec2(1.0f, 1.0f));
	// ===< TEMP

	if (modelID_ == "Container02")
		int32 a = 100;
	if (modelID_ == "StylizedCharacter4")
		int32 a = 100;
	for (uint32 i = 0, numMeshes = meshes_.size(); i < numMeshes; i++)
	{
		// FIXME?: NOTE: when numMeshes_ != numMatSlots_, it means that all the meshes share ONE MATERIAL
		//if(numMeshes == numMaterials_)
		if (numMeshes == numMatSlots_)
		{
			if(Bitwise::IsBitOnByFlag<uint8>(&flags_, ALL_MESHES_SHARE_ONE_MAT))
				meshes_[i].Render(shader, numInstances_, 0);
			else
			meshes_[i].Render(shader, numInstances_, i);
		}
		else
			meshes_[i].Render(shader, numInstances_, 0);
	}
}

void
Model::Cleanup()
{
	for (Mesh mesh : meshes_)
	{
		mesh.Cleanup();
	}

	posVBO_.Cleanup();
	sizeVBO_.Cleanup();
	rotationVBO_.Cleanup();
	modelMatricesVBO_.Cleanup();
	normalMatricesVBO_.Cleanup();
}





RigidBody*
Model::GetInstance(const string& instanceID)
{
	int32 idx = GetInstanceIdx(instanceID);
	if (idx != -1)
	{
		return instances_[idx];
	}
	
	return nullptr;
}

RigidBody*
Model::GetInstance(const int32 index)
{
	return instances_[index];
}

bool
Model::AssignMaterial(Material* material)
{
	if (Bitwise::IsBitOnByFlag<uint8>(&flags_, IS_LANDSCAPE))
	{
		std::cout << "You should assign a MaterialLand to a landscape model" << std::endl;
		return false;
	}
	
	if(nextMatSlot_ > numMatSlots_ - 1)
	{
		// TODO: wrap spdlog in
		std::cout << "not enough mat slots for model: " << modelID_ << std::endl;
		return false;
	}

	materials_[nextMatSlot_++].second = material;
	numMaterials_++;
	return true;
}

bool
Model::AssignMaterial(MaterialLand* matLand)
{
	if (!Bitwise::IsBitOnByFlag<uint8>(&flags_, IS_LANDSCAPE))
	{
		std::cout << "You can't assign a MaterialLand to a non-landscape model" << std::endl;
		return false;
	}

	materialLand_ = matLand;

	return true;
}

bool
Model::HasTransparency()
{
	return Bitwise::IsBitOnByFlag<uint8>(&flags_, HAS_TRANSPARENCY);
}



/*********************************************************************************
 * Manually Create a New Material Slot
 *********************************************************************************/
void
Model::AddNewMatSlot(const string& matName)
{
	materials_.push_back(std::make_pair(matName, nullptr));
	numMatSlots_++;
}



/*********************************************************************************
 * Create a New Material Slot automatically while loading meshes
 *********************************************************************************/
bool
Model::NewMatSlot(string matName)
{
	/*
	for (auto& itr : materials_)
	{
		if (itr.first == matName)
			return false;
	}
	*/

	materials_.push_back(std::make_pair(matName, nullptr));
	numMatSlots_++;
	return true;
}

bool
Model::IsLandscape()
{
	return Bitwise::IsBitOnByFlag<uint8>(&flags_, IS_LANDSCAPE);
}
