#ifndef VERTEXMEMORY_H
#define VERTEXMEMORY_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <map>

class BufferObject
{
public:

	BufferObject(GLenum type = GL_ARRAY_BUFFER)
		: type_(type)
	{
	}

public:
	void Generate()
	{
		glGenBuffers(1, &id_);
	}
	void Bind()
	{
		glBindBuffer(type_, id_);
	}

	template<typename T>
	void SetData(GLuint numElements, T* data, GLenum usage)
	{
		glBufferData(type_, numElements * sizeof(T), data, usage);
	}

	// update data
	template<typename T>
	void UpdateData(GLintptr offset, GLuint numElements, T* data)
	{
		glBufferSubData(type_, offset, numElements * sizeof(T), data);
	}

	// attrib pointer
	template<typename T>
	void SetAttribPointer(GLuint idx, GLint size, GLenum type, GLuint stride, GLuint offset, GLuint divisor = 0)
	{
		glEnableVertexAttribArray(idx);
		glVertexAttribPointer(idx, size, type, GL_FALSE, stride * sizeof(T), (void*)(offset * sizeof(T)));

		if(divisor > 0)
		{
			glVertexAttribDivisor(idx, divisor);
		}
	}

	void UnBind()
	{
		glBindBuffer(type_, 0);
	}

	void Cleanup()
	{
		glDeleteBuffers(1, &id_);
	}


	GLuint id_;
	GLenum type_;

};




class VertexArrayObject
{
public:
	BufferObject& operator[](const char* key)
	{
		return bufferObjects_[key];
	}

	void Draw(GLenum mode, GLuint first, GLuint count)
	{
		glDrawArrays(mode, first, count);
	}

	void Draw(GLenum mode, GLuint count, GLenum type, GLint indices, GLuint numInstances = 1)
	{
		//glDrawElementsInstanced(mode, count, type, (void*)indices, numInstances);
		glDrawElementsInstanced(mode, count, type, 0, numInstances);
	}

	void Generate()
	{
		glGenVertexArrays(1, &id_);
	}

	void Bind()
	{
		glBindVertexArray(id_);
	}

	void UnBind()
	{
		glBindVertexArray(0);
	}

	void Cleanup()
	{
		glDeleteVertexArrays(1, &id_);
		for(auto& pair : bufferObjects_)
		{
			pair.second.Cleanup();
		}
	}

private:
	GLuint id_;
	std::map<string, BufferObject> bufferObjects_;
};

#endif
