#ifndef MESH_H
#define MESH_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <vector>
#include <glm/glm.hpp>

#include "Shader.h"
#include "Texture.h"
#include "VertexMemory.hpp"
#include "../physics/Bounds.h"

struct Vertex {
	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec2 texCoord;
	glm::vec3 tangent;

	static std::vector<Vertex> VerticesArrayToVector(real32* vertices, int32 numVertices);

	static void CalcTangentVectors(std::vector<Vertex>& vertices, std::vector<uint32>& indices);
};

class Model;
class Mesh
{
	friend Model;
public:
	Mesh();
	Mesh(BoundingRegion br, std::vector<Texture> textures = {});
	Mesh(BoundingRegion br, aiColor4D diffuseColor, aiColor4D specularColor);
	void LoadData(std::vector<Vertex> vertices, std::vector<uint32> indices);

	void Render(Shader shader, uint32 numInstances, int32 idxInModel = -1);
	std::vector<uint32> GetIndices() const { return indices_; }
	void Cleanup();
	BoundingRegion GetBoundingRegion() const { return br_; }
	VertexArrayObject& GetVAO() { return vao_; }
private:
	VertexArrayObject vao_;
	bool bNoTex_;

	std::vector<Vertex> vertices_;
	std::vector<uint32> indices_;
	std::vector<Texture> textures_;
	aiColor4D diffuseColor_;
	aiColor4D specularColor_;

	BoundingRegion br_;
	Model* pModel_;
};

#endif
