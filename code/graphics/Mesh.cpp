#include "Mesh.h"
#include "Model.h"

std::vector<Vertex>
Vertex::VerticesArrayToVector(real32* vertices, int32 numVertices)
{
	std::vector<Vertex> ret(numVertices);

	//int32 stride = sizeof(Vertex) / sizeof(real32);
	int32 stride = 8;
	for (int32 i = 0; i < numVertices; i++)
	{
		// position
		ret[i].pos = glm::vec3(
			vertices[i * stride + 0],
			vertices[i * stride + 1],
			vertices[i * stride + 2]);
		// normal
		ret[i].normal = glm::vec3(
			vertices[i * stride + 3],
			vertices[i * stride + 4],
			vertices[i * stride + 5]);
		// uv
		ret[i].texCoord = glm::vec2(
			vertices[i * stride + 6],
			vertices[i * stride + 7]);
	}

	return ret;
}

void
AverageVectors(glm::vec3& baseVec, glm::vec3 addition, uint8 existingContributions)
{
	if (!existingContributions)
	{
		baseVec = addition;
	}
	else
	{
		real32 f = 1 / ((real32)existingContributions + 1);

		baseVec *= (real32)(existingContributions)*f;

		baseVec += addition * f;
	}
}

void
Vertex::CalcTangentVectors(std::vector<Vertex>& vertices, std::vector<uint32>& indices)
{
	uint8* counts = (uint8*)malloc(vertices.size() * sizeof(uint8));
	for (uint32 i = 0, len = vertices.size();  i < len; i++)
	{
		counts[i] = 0;
	}

	// iterate through indices and calculate vectors for each face
	for (uint32 i = 0, len = indices.size(); i < len; i += 3)
	{
		Vertex v1 = vertices[indices[i + 0]];
		Vertex v2 = vertices[indices[i + 1]];
		Vertex v3 = vertices[indices[i + 2]];

		glm::vec3 edge1 = v2.pos - v1.pos;
		glm::vec3 edge2 = v3.pos - v1.pos;

		glm::vec2 deltaUV1 = v2.texCoord - v1.texCoord;
		glm::vec2 deltaUV2 = v3.texCoord - v1.texCoord;

		real32 oneOverDet = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);

		glm::vec3 tangent = {
			oneOverDet * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x),
			oneOverDet * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y),
			oneOverDet * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z)
		};

		AverageVectors(vertices[indices[i + 0]].tangent, tangent, counts[indices[i + 0]]++);
		AverageVectors(vertices[indices[i + 1]].tangent, tangent, counts[indices[i + 1]]++);
		AverageVectors(vertices[indices[i + 2]].tangent, tangent, counts[indices[i + 2]]++);
	}



	// 
}

Mesh::Mesh()
	: pModel_(nullptr)
	, bNoTex_(false)
{
}


Mesh::Mesh(BoundingRegion br, std::vector<Texture> textures)
	: br_(br)
	, textures_(textures)
	, bNoTex_(false)
	, pModel_(nullptr)
{
}

Mesh::Mesh(BoundingRegion br, aiColor4D diffuseColor, aiColor4D specularColor)
	: br_(br)
	, diffuseColor_(diffuseColor)
	, specularColor_(specularColor)
	, bNoTex_(true)
	, pModel_(nullptr)
{
}

void 
Mesh::LoadData(std::vector<Vertex> vertices, std::vector<uint32> indices)
{
	vertices_ = vertices;
	indices_ = indices;

	vao_.Generate();
	vao_.Bind();

	// ===> INDICES
	vao_["EBO"] = BufferObject(GL_ELEMENT_ARRAY_BUFFER);
	vao_["EBO"].Generate();
	vao_["EBO"].Bind();
	vao_["EBO"].SetData<GLuint>(indices_.size(), &indices_[0], GL_STATIC_DRAW);
	// ===< INDICES

	// ===> VERTICES
	vao_["VBO"] = BufferObject(GL_ARRAY_BUFFER);
	vao_["VBO"].Generate();
	vao_["VBO"].Bind();
	vao_["VBO"].SetData<Vertex>(vertices_.size(), &vertices_[0], GL_STATIC_DRAW);
	// ===< VERTICES

	// ===> ATTRIB POINTER
	vao_["VBO"].Bind();
	// aPos
	vao_["VBO"].SetAttribPointer<GLfloat>(0, 3, GL_FLOAT, 11, 0);
	// aNormal
	vao_["VBO"].SetAttribPointer<GLfloat>(1, 3, GL_FLOAT, 11, 3);
	// aTexCoord
	vao_["VBO"].SetAttribPointer<GLfloat>(2, 2, GL_FLOAT, 11, 6);
	// aTangent
	vao_["VBO"].SetAttribPointer<GLfloat>(3, 3, GL_FLOAT, 11, 8);
	vao_["VBO"].UnBind();
	// ===< ATTRIB POINTER

	vao_.UnBind();
}

void
Mesh::Render(Shader shader, uint32 numInstances, int32 idxInModel)
{
	shader.SetBool("material.noNormalMap", true);
	// ==========> CASE: NO TEXTURE
	if (bNoTex_)
	{
		shader.Set4Float("materialSimple.diffuse", diffuseColor_);
		shader.Set4Float("materialSimple.specular", specularColor_);
		shader.SetBool("noTex", true);
	}
	// ==========< CASE: NO TEXTURE
	// ==========> CASE: HAS TEXTURE
	else
	{
		// ===> EMBEDED TEXTUERS(We assume that gltf models always have embedded textures)
		// ===> (we also assue that we only load landscape models from fbx/obj)
		if (pModel_->GetSrcType() == ModelSrcType::GLTF)
		{
			shader.SetBool("noTex", false);
			uint32 diffuseIdx = 0;
			uint32 specularIdx = 0;
			uint32 normalIdx = 0;
			// textures
			for (uint32 i = 0; i < textures_.size(); i++)
			{
				glActiveTexture(GL_TEXTURE0 + i);

				string name;
				switch (textures_[i].GetType())
				{
					case aiTextureType_DIFFUSE:
					{
						// FIXME:
						name = "material.diffuse" + std::to_string(diffuseIdx++);
						if (textures_[i].HasTransparency())
							shader.SetBool("hasTransparency", true);
					} break;

					case aiTextureType_SPECULAR:
					{
						name = "material.specular" + std::to_string(specularIdx++);
					} break;

					case aiTextureType_NORMALS:
					{
						name = "material.normal" + std::to_string(normalIdx++);
						shader.SetBool("material.noNormalMap", false);
					} break;

					default:
					{
					} break;
				}

				shader.SetInt(name, i);
				textures_[i].Bind();
			}
		}
		// ===< EMBEDED TEXTURES
		// ===> MANNUALLY ASSIGNED MATERIAL
		else if (pModel_->GetSrcType() == ModelSrcType::FBX ||
				 pModel_->GetSrcType() == ModelSrcType::OBJ)
		{
			if(pModel_->IsLandscape())
			{
				int32 numLayers = pModel_->materialLand_->GetNumLayers();
				uint32 diffuseIdx = 1;
				uint32 specularIdx = 1;
				uint32 normalIdx = 1;
				// textures
				std::vector<Texture> textures = pModel_->materialLand_->GetTextures();
				shader.SetInt("material.numLayers", textures.size());
				for (uint32 i = 0; i < textures.size(); i++)
				{
					glActiveTexture(GL_TEXTURE0 + i);

					string name;
					switch (textures[i].GetType())
					{
						// FIXME: aiTextureType_AMBIENT to represent blendMap??
						case aiTextureType_AMBIENT:
						{
							// FIXME:
							name = "material.blendMap";
						} break;

						case aiTextureType_DIFFUSE:
						{
							// FIXME:
							name = "material.diffuseLayer" + std::to_string(diffuseIdx++);
						} break;

						case aiTextureType_SPECULAR:
						{
							name = "material.specularLayer" + std::to_string(specularIdx++);
						} break;

						case aiTextureType_NORMALS:
						{
							name = "material.normalLayer" + std::to_string(normalIdx++);
						} break;

						default:
						{
						} break;
					}

					shader.SetInt(name, i);
					textures[i].Bind();
					// set uv scale
					shader.Set2Float("uvScale", pModel_->materialLand_->GetUVScale());
					glm::vec2 theUVScale = pModel_->materialLand_->GetUVScale();
					int32 a = 100;
				}
			}
			else
			{
				shader.SetBool("noTex", false);
				uint32 diffuseIdx = 0;
				uint32 specularIdx = 0;
				uint32 normalIdx = 0;
				// textures
				std::vector<Texture> textures = pModel_->materials_[idxInModel].second->GetTextures();
				for (uint32 i = 0; i < textures.size(); i++)
				{
					glActiveTexture(GL_TEXTURE0 + i);

					string name;
					switch (textures[i].GetType())
					{
						case aiTextureType_DIFFUSE:
						{
							// FIXME:
							name = "material.diffuse" + std::to_string(diffuseIdx++);
							if (textures[i].HasTransparency())
								shader.SetBool("hasTransparency", true);
						} break;

						case aiTextureType_SPECULAR:
						{
							name = "material.specular" + std::to_string(specularIdx++);
						} break;

						case aiTextureType_NORMALS:
						{
							name = "material.normal" + std::to_string(normalIdx++);
							shader.SetBool("material.noNormalMap", false);
						} break;

						default:
						{
						} break;
					}

					shader.SetInt(name, i);
					textures[i].Bind();
					// set uv scale
					if (pModel_->GetModelID() == "BlenderFloor")
						int32 a = 100;
					shader.Set2Float("uvScale", pModel_->materials_[idxInModel].second->GetUVScale());
					if (pModel_->materials_[idxInModel].second->HasNormalMap())
					{
						shader.SetBool("material.noNormalMap", false);
					}
				}
				// ===< MANNUALLY ASSIGNED MATERIAL
			}
		}
		// ==========< CASE: HAS TEXTURE

		vao_.Bind();
		vao_.Draw(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, 0, numInstances);
		vao_.UnBind();

		glActiveTexture(GL_TEXTURE0);
	}
}


void
Mesh::Cleanup()
{
	vao_.Cleanup();
}



