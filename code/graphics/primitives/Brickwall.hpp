#ifndef BRICKWALL_HPP
#define BRICKWALL_HPP

#include "Plane.hpp"
#include "../../Utilities.h"

class Brickwall : public Plane
{
public:
	Brickwall(uint32 maxNumInstances)
		: Plane(maxNumInstances)
	{
	}
	void Init()
	{
		std::vector<Texture> textures = {
			Texture(Utils::GetAssetsDirectory() + "textures", "brickwall_diffuse.jpg", aiTextureType_DIFFUSE),
			Texture(Utils::GetAssetsDirectory() + "textures", "brickwall_normal.jpg", aiTextureType_NORMALS),
			Texture(Utils::GetAssetsDirectory() + "textures", "brickwall_specular.jpg", aiTextureType_SPECULAR)
		};

		for(Texture t : textures)
		{
			t.Load();
		}

		Plane::Init(textures);
	}

	void Render(Shader shader, real32 dt, Scene* scene)
	{
		Model::Render(shader, dt, scene);
	}
	
	
};

#endif
