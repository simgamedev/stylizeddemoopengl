#ifndef LAMP_HPP
#define LAMP_HPP

#include "Cube.hpp"
#include "../Light.h"
#include <glm/glm.hpp>

class Lamp : public Cube
{
public:
	Lamp(uint32 maxNumInstances,
		 glm::vec3 lightColor = glm::vec3(1.0f)) // defaults to white light
		: Cube(maxNumInstances)
		, lightColor_(lightColor)
	{
		modelID_ = "lamp";
	}


	glm::vec3 GetLightColor() const { return lightColor_; }
private:
	glm::vec3 lightColor_;
};

#endif
