#ifndef BOX_HPP
#define BOX_HPP

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <vector>
#include "../../common.h"

#include "../Shader.h"

#include "../VertexMemory.hpp"

// FIXME: put it in another place
#define MAX_INSTANCES 100

class Box
{
public:
	std::vector<glm::vec3> positions_;
	std::vector<glm::vec3> sizes_;

	void Init()
	{
			vertices_ = {
					// position             x   y   z   i
					0.5f,  0.5f,  0.5f, // +   +   +   0
					-0.5f,  0.5f,  0.5f, // -   +   +   1
					-0.5f, -0.5f,  0.5f, // -   -   +   2
					0.5f, -0.5f,  0.5f, // +   -   +   3
					0.5f,  0.5f, -0.5f, // +   +   -   4
					-0.5f,  0.5f, -0.5f, // -   +   -   5
					-0.5f, -0.5f, -0.5f, // -   -   -   6
					0.5f, -0.5f, -0.5f  // +   -   -   7
			};

			indices_ = { // 12 lines
					// front face (+ve z)
					0, 1,
					1, 2,
					2, 3,
					3, 0,
					// back face (-ve z)
					4, 5,
					5, 6,
					6, 7,
					7, 4,
					// right face (+ve x)
					0, 4,
					3, 7,
					// left face (-ve x)
					1, 5,
					2, 6
			};

			vao_.Generate();
			vao_.Bind();

			vao_["EBO"] = BufferObject(GL_ELEMENT_ARRAY_BUFFER);
			vao_["EBO"].Generate();
			vao_["EBO"].Bind();
			vao_["EBO"].SetData<GLuint>(indices_.size(), &indices_[0], GL_STATIC_DRAW);

			vao_["VBO"] = BufferObject(GL_ARRAY_BUFFER);
			vao_["VBO"].Generate();
			vao_["VBO"].Bind();
			vao_["VBO"].SetData<GLfloat>(vertices_.size(), &vertices_[0], GL_STATIC_DRAW);
			vao_["VBO"].SetAttribPointer<GLfloat>(0, 3, GL_FLOAT, 3, 0);

			vao_["posVBO"] = BufferObject(GL_ARRAY_BUFFER);
			vao_["posVBO"].Generate();
			vao_["posVBO"].Bind();
			vao_["posVBO"].SetData<glm::vec3>(MAX_INSTANCES, NULL, GL_DYNAMIC_DRAW);
			vao_["posVBO"].SetAttribPointer<glm::vec3>(1, 3, GL_FLOAT, 1, 0, 1);

			vao_["sizeVBO"] = BufferObject(GL_ARRAY_BUFFER);
			vao_["sizeVBO"].Generate();
			vao_["sizeVBO"].Bind();
			vao_["sizeVBO"].SetData<glm::vec3>(MAX_INSTANCES, NULL, GL_DYNAMIC_DRAW);
			vao_["sizeVBO"].SetAttribPointer<glm::vec3>(2, 3, GL_FLOAT, 1, 0, 1);
			vao_["sizeVBO"].UnBind();

			vao_.UnBind();
	}

	void Render(Shader shader)
	{
		shader.SetMat4("model", glm::mat4(1.0f));

		// update data
		int32 numInstances = std::min(MAX_INSTANCES, (int32)positions_.size());

		if(numInstances != 0)
		{
			vao_["posVBO"].Bind();
			vao_["posVBO"].UpdateData<glm::vec3>(0, numInstances, &positions_[0]);

			vao_["sizeVBO"].Bind();
			vao_["sizeVBO"].UpdateData<glm::vec3>(0, numInstances, &sizes_[0]);
		}

		vao_.Bind();
		vao_.Draw(GL_LINES, indices_.size(), GL_UNSIGNED_INT, 0, numInstances);
		vao_.UnBind();
	}

	void Cleanup()
	{
		vao_.Cleanup();
	}

	VertexArrayObject vao_;
private:

	std::vector<real32> vertices_;
	std::vector<uint32> indices_;
};

#endif
