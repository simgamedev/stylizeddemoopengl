#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "../Model.h"
#include "../../Utilities.h"


class Sphere : public Model
{
public:
	Sphere(uint32 maxNumInstances)
		: Model("sphere", BoundType::SPHERE, maxNumInstances, NO_TEXTURE | DYNAMIC_DRAW)
	{
	}

	void Init()
	{
		LoadFromFile(Utils::GetAssetsDirectory() + "models/sphere/scene.gltf");
	}
};

#endif
