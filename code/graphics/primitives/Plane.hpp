#ifndef PLANE_HPP
#define PLANE_HPP
#include "../Model.h"

class Plane : public Model
{
public:
	Plane(uint32 maxNumInstances)
		: Model("plane", BoundType::AABB, maxNumInstances, STATIC_DRAW) {}

	void Init(std::vector<Texture> textures)
	{
		int32 numVertices = 4;
		real32 vertices[] = {
			// position			normal				texcoord
			 0.5f,  0.5f, 0.0f, 0.0f, 0.0f, 1.0f,	1.0f, 1.0f, // top right
			-0.5f,  0.5f, 0.0f, 0.0f, 0.0f, 1.0f,	0.0f, 1.0f, // top left
			-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f,	0.0f, 0.0f, // bottom left
			 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f,	1.0f, 0.0f  // bottom right
		};

		std::vector<uint32> indices = {
			0, 1, 3,
			1, 2, 3
		};

		BoundingRegion br(glm::vec3(-0.5f, -0.5f, 0.0f),
						  glm::vec3(0.5f, 0.5f, 0.0f));


		std::vector<Vertex> vertexList = Vertex::VerticesArrayToVector(vertices, numVertices);
		Vertex::CalcTangentVectors(vertexList, indices);

		Mesh mesh(br, textures);
		mesh.LoadData(vertexList, indices);

		meshes_.push_back(mesh);
		boundingRegions_.push_back(br);
	}
private:
};

#endif
