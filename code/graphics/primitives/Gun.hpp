#ifndef GUN_HPP
#define GUN_HPP

#include "../Model.h"
#include "../../camera/Camera.h"
#include "../../Scene.h"

class Gun : public Model
{
public:
	Gun(uint32 maxNumInstances)
		: Model("m4a1", BoundType::AABB, maxNumInstances, STATIC_DRAW | NO_TEXTURE) {}

	void Render(Shader shader, real32 dt, Scene* scene, glm::mat4 modelMat)
	{
		//glm::mat4 modelMat = glm::mat4(1.0f);

		//  position
		/*
		pos_ = scene->GetActiveCamera()->GetPos() +
			glm::vec3(scene->GetActiveCamera()->GetFront() * 0.2f) -
			glm::vec3(scene->GetActiveCamera()->GetUp() * 0.0605f);
		modelMat = glm::translate(modelMat, pos_);

		// scale
		modelMat = glm::scale(modelMat, size_);

		// FIXME:
		shader.SetMat4("model", modelMat);
		*/

		Model::Render(shader, dt, scene);
	}

};

#endif
