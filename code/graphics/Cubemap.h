#ifndef CUBEMAP_H
#define CUBEMAP_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "stb_image.h"


#include <string>
#include <vector>
#include "Shader.h"
#include "VertexMemory.hpp"
#include "../Scene.h"


class Cubemap
{
public:
	Cubemap();
	void LoadTextures(string directory,
					  string front = "front.png",
					  string right = "right.png",
					  string back = "back.png",
					  string left = "left.png",
					  string top = "top.png",
					  string bottom = "bottom.png");
	void Init();
	void Render(Shader shader, Scene* scene);
	void Cleanup();
private:
	uint32 texID_;
	string directory_;
	std::vector<string> textureNames_;
	bool bHasTextures_;

	VertexArrayObject vao_;
};

#endif
