#pragma once
#define WIN64
//#define APPLE
#ifdef WIN64
#define _WIN32_WINNT 0x0A00
#define NOMINMAX
#include <windows.h>
#include <Shlwapi.h>
#endif

#ifdef APPLE
#include <mach-o/dyld.h>
#include <limits.h>
#include <dirent.h>
#endif


#include <iostream>
#include <sstream>
#include <algorithm>
#include "common.h"
#include <cstdlib>
#include <cmath>
#include <glm/glm.hpp>


namespace Utils
{
	//#define WIN32_LEAN_AND_MEAN
#ifdef WIN64
	inline string GetAssetsDirectory()
	{
		HMODULE hModule = GetModuleHandle(nullptr);
		if(hModule)
		{
			char path[256];
			GetModuleFileNameA(hModule, path, sizeof(path));
			PathRemoveFileSpecA(path);
			strcat_s(path, "\\");
		    string result(path);
			std::size_t pos = result.find("build");
			result = result.substr(0, pos);
			result = result + "assets\\";
			return(result);
		}
		return "";
	}

	
	template <typename T>
	string ToString(const T aValue, const int32 n = 2)
	{
		std::ostringstream out;
		out.precision(n);
		out << std::fixed << aValue;
		return out.str();
	}
#endif
}

namespace Math
{
	inline real32 Square(real32 x)
	{
		return x * x;
	}

	inline real32 Sqrt(real32 x)
	{
		return sqrt(x);
	}

	//template<typename T>
	inline real32 Distance(glm::vec3 a, glm::vec3 b)
	{
		return glm::distance(a, b);
	}
}
