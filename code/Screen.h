#ifndef SCREEN_H
#define SCREEN_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "common.h"

class Screen
{
public:
	static uint32 SCR_WIDTH;
	static uint32 SCR_HEIGHT;

	static void FramebufferSizeCallback(GLFWwindow* window, int32 width, int32 height);

	Screen();

	bool Init();
	void SetParameters();

	//void Update();
	//void NewFrame();
	void BeginDraw();
	void EndDraw();

	bool ShouldClose();
	void SetShouldClose(bool shouldSlose);
private:
	GLFWwindow* pWindow_;
};
#endif
