#include "Scene.h"

#include "graphics/Model.h"
#include "algorithms/Bitwise.hpp"

uint32 Scene::scrWidth = 0;
uint32 Scene::scrHeight = 0;

// ===> UBO
#define MAX_POINT_LIGHTS 10
#define MAX_SPOT_LIGHTS 2
// ===< UBO





void 
Scene::framebufferSizeCallback(GLFWwindow* window, int32 width, int32 height)
{
	glViewport(0, 0, width, height);
	Scene::scrWidth = width;
	Scene::scrHeight = height;
}

Scene::Scene()
	: currentID_("aaaaaaaa")
	, activeSpotLights_(0)
	, activePointLights_(0)
	, numPointLights_(0)
	, numSpotLights_(0)
	, bSkipNormalMap_(false)
	, lightsUBO_(0)
	, bIsBackFaceCullingON_(true)
{}

Scene::Scene(int32 glfwVersionMajor, int32 glfwVersionMinor,
			 const char* title, uint32 scrWidth, uint32 scrHeight)
	: glfwVersionMajor_(glfwVersionMajor)
	, glfwVersionMinor_(glfwVersionMinor)
	, title_(title)
	, activeCameraIdx_(-1)
	, activePointLights_(0)
	, activeSpotLights_(0)
	, numPointLights_(0)
	, numSpotLights_(0)
	, currentID_("aaaaaaaa")
	, bSkipNormalMap_(false)
	, lightsUBO_(0)
	, bIsBackFaceCullingON_(true)
{
	Scene::scrWidth = scrWidth;
	Scene::scrHeight = scrHeight;

	SetBgColor(0.1f, 0.1f, 0.1f ,1.0f);
}

bool
Scene::Init()
{
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, glfwVersionMajor_);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, glfwVersionMinor_);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifndef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	pWindow_ =glfwCreateWindow(scrWidth, scrHeight, title_, NULL, NULL);
	if(pWindow_ == NULL)
	{
		return false;
	}
	glfwMakeContextCurrent(pWindow_);

	// glad
	if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to init GLAD" << std::endl;
		return false;
	}

	glViewport(0, 0, scrWidth, scrHeight);

	// callbacks
	glfwSetFramebufferSizeCallback(pWindow_, framebufferSizeCallback);
	glfwSetKeyCallback(pWindow_, Keyboard::KeyCallback);
	glfwSetCursorPosCallback(pWindow_, Mouse::CursorPosCallback);
	glfwSetMouseButtonCallback(pWindow_, Mouse::MouseButtonCallback);
	glfwSetScrollCallback(pWindow_, Mouse::MouseWheelCallback);

	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	EnableBackfaceCulling();
	glfwSetInputMode(pWindow_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// init octree
	//octree_ = new Octree::Node(BoundingRegion(glm::vec3(-16.0f), glm::vec3(16.0f)));
	octree_ = new Octree::Node(BoundingRegion(glm::vec3(-8.0f), glm::vec3(8.0f)));
	return true;
}


void
Scene::PrepareOctree(Box& octreeDrawable)
{
	octree_->Update(octreeDrawable);
}

void
Scene::UpdateShadersUBOs()
{
	lightsUBO_.Bind();
	lightsUBO_.StartWrite();
	// directional light
	//lightsUBO_.WriteElement<real32>(&dirLight_->strength);
	lightsUBO_.WriteElement<glm::vec3>(&dirLight_->direction);
	lightsUBO_.WriteElement<glm::vec3>(&dirLight_->ambient);
	lightsUBO_.WriteElement<glm::vec3>(&dirLight_->diffuse);
	lightsUBO_.WriteElement<glm::vec3>(&dirLight_->specular);
	// pointlights
	numPointLights_ = std::min<uint32>(pointLights_.size(), MAX_POINT_LIGHTS);
	lightsUBO_.WriteElement<uint32>(&numPointLights_);
	std::cout << "numPointLights: " << numPointLights_ << std::endl;
	uint32 i = 0;
	for (; i < numPointLights_; i++)
	{
		//lightsUBO_.WriteElement<real32>(&pointLights_[i]->strength);
		lightsUBO_.WriteElement<glm::vec3>(&pointLights_[i]->position);
		lightsUBO_.WriteElement<real32>(&pointLights_[i]->constant);
		lightsUBO_.WriteElement<real32>(&pointLights_[i]->linear);
		lightsUBO_.WriteElement<real32>(&pointLights_[i]->quadratic);
		lightsUBO_.WriteElement<glm::vec3>(&pointLights_[i]->ambient);
		lightsUBO_.WriteElement<glm::vec3>(&pointLights_[i]->diffuse);
		lightsUBO_.WriteElement<glm::vec3>(&pointLights_[i]->specular);
	}
	lightsUBO_.AdvanceArray(MAX_POINT_LIGHTS - i);
	// spotlights
	numSpotLights_ = std::min<uint32>(spotLights_.size(), MAX_SPOT_LIGHTS);
	lightsUBO_.WriteElement<uint32>(&numSpotLights_);
	std::cout << "numSpotLights: " << numSpotLights_ << std::endl;
	for (i = 0; i < numSpotLights_; i++)
	{
		lightsUBO_.WriteElement<glm::vec3>(&spotLights_[i]->position);
		lightsUBO_.WriteElement<glm::vec3>(&spotLights_[i]->direction);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->cutOff);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->outerCutOff);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->constant);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->linear);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->quadratic);
		lightsUBO_.WriteElement<glm::vec4>(&spotLights_[i]->ambient);
		lightsUBO_.WriteElement<glm::vec4>(&spotLights_[i]->diffuse);
		lightsUBO_.WriteElement<glm::vec4>(&spotLights_[i]->specular);
	}
	lightsUBO_.UnBind();
}
void
Scene::PrepareShaders(std::vector<Shader> shaders)
{
	lightsUBO_ = UBO::UBO(0, {
		UBO::NewStruct({   // dirLight
							  //UBO::Type::SCALAR, // strength
							  UBO::Type::VEC3, // direction
							  UBO::Type::VEC3, // ambient
							  UBO::Type::VEC3, // diffuse
							  UBO::Type::VEC3, // specular
		}),
		UBO::Type::SCALAR, // numPointLights
		UBO::NewArray(MAX_POINT_LIGHTS, UBO::NewStruct({
							  //UBO::Type::SCALAR, // strength
							  UBO::Type::VEC3, // position
							  UBO::Type::SCALAR, // constant 
							  UBO::Type::SCALAR, // linear
							  UBO::Type::SCALAR, // quadratic
							  UBO::Type::VEC3, // ambient
							  UBO::Type::VEC3, // diffuse
							  UBO::Type::VEC3, // specular
		})),
		UBO::Type::SCALAR, // numSpotLights
		UBO::NewArray(MAX_SPOT_LIGHTS, UBO::NewStruct({
							  UBO::Type::VEC3, // position
							  UBO::Type::VEC3, // direction
							  UBO::Type::SCALAR, // cutoff
							  UBO::Type::SCALAR, // outerCutoff
							  UBO::Type::SCALAR, // constant
							  UBO::Type::SCALAR, // linear
							  UBO::Type::SCALAR, // quadratic
							  UBO::Type::VEC4, // ambient
							  UBO::Type::VEC4, // diffuse
							  UBO::Type::VEC4, // specular
		})),
	});
	for (Shader s : shaders)
	{
		lightsUBO_.AttachToShader(s, "Lights");
	}

	// ===> Setup memory
	lightsUBO_.Generate();
	lightsUBO_.Bind();
	lightsUBO_.InitNullData(GL_STATIC_DRAW);
	lightsUBO_.BindRange();
	// ===< Setup memory

	lightsUBO_.StartWrite();
	// directional light
	//FIXME: WriteElement has bugs!
	//lightsUBO_.WriteElement<real32>(&dirLight_->strength);
	lightsUBO_.WriteElement<glm::vec3>(&dirLight_->direction);
	lightsUBO_.WriteElement<glm::vec3>(&dirLight_->ambient);
	lightsUBO_.WriteElement<glm::vec3>(&dirLight_->diffuse);
	lightsUBO_.WriteElement<glm::vec3>(&dirLight_->specular);
	// pointlights
	numPointLights_ = std::min<uint32>(pointLights_.size(), MAX_POINT_LIGHTS);
	lightsUBO_.WriteElement<uint32>(&numPointLights_);
	uint32 i = 0;
	for (; i < numPointLights_; i++)
	{
		//FIXME: WriteElement has bugs!
		//lightsUBO_.WriteElement<real32>(&pointLights_[i]->strength);
		lightsUBO_.WriteElement<glm::vec3>(&pointLights_[i]->position);
		lightsUBO_.WriteElement<real32>(&pointLights_[i]->constant);
		lightsUBO_.WriteElement<real32>(&pointLights_[i]->linear);
		lightsUBO_.WriteElement<real32>(&pointLights_[i]->quadratic);
		lightsUBO_.WriteElement<glm::vec3>(&pointLights_[i]->ambient);
		lightsUBO_.WriteElement<glm::vec3>(&pointLights_[i]->diffuse);
		lightsUBO_.WriteElement<glm::vec3>(&pointLights_[i]->specular);
	}
	lightsUBO_.AdvanceArray(MAX_POINT_LIGHTS - i);
	// spotlights
	numSpotLights_ = std::min<uint32>(spotLights_.size(), MAX_SPOT_LIGHTS);
	lightsUBO_.WriteElement<uint32>(&numSpotLights_);
	for (i = 0; i < numSpotLights_; i++)
	{
		lightsUBO_.WriteElement<glm::vec3>(&spotLights_[i]->position);
		lightsUBO_.WriteElement<glm::vec3>(&spotLights_[i]->direction);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->cutOff);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->outerCutOff);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->constant);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->linear);
		lightsUBO_.WriteElement<real32>(&spotLights_[i]->quadratic);
		lightsUBO_.WriteElement<glm::vec4>(&spotLights_[i]->ambient);
		lightsUBO_.WriteElement<glm::vec4>(&spotLights_[i]->diffuse);
		lightsUBO_.WriteElement<glm::vec4>(&spotLights_[i]->specular);
	}


	lightsUBO_.UnBind();
}

void
Scene::ProcessInput(real32 dt)
{
	if (activeCameraIdx_ != -1 && activeCameraIdx_ < cameras_.size())
	{
		real64 dx = Mouse::GetDeltaX();
		real64 dy = Mouse::GetDeltaY();
		if (dx != 0 || dy != 0)
		{
			cameras_[activeCameraIdx_]->UpdateDirection(dx, dy);
		}

		// zoom
		real64 scrollDY = Mouse::GetScrollDY();
		if (scrollDY != 0)
		{
			cameras_[activeCameraIdx_]->UpdateZoom(scrollDY);
		}

		// position
		// WASDQE
		if (Keyboard::IsKeyDown(GLFW_KEY_E))
		{
			cameras_[activeCameraIdx_]->UpdatePos(CameraDirection::UP, dt);
		}

		if (Keyboard::IsKeyDown(GLFW_KEY_Q))
		{
			cameras_[activeCameraIdx_]->UpdatePos(CameraDirection::DOWN, dt);
		}

		if (Keyboard::IsKeyDown(GLFW_KEY_W))
		{
			cameras_[activeCameraIdx_]->UpdatePos(CameraDirection::FORWARD, dt);
		}
		if (Keyboard::IsKeyDown(GLFW_KEY_S))
		{
			cameras_[activeCameraIdx_]->UpdatePos(CameraDirection::BACKWARD, dt);
		}
		if (Keyboard::IsKeyDown(GLFW_KEY_A))
		{
			cameras_[activeCameraIdx_]->UpdatePos(CameraDirection::LEFT, dt);
		}
		if (Keyboard::IsKeyDown(GLFW_KEY_D))
		{
			cameras_[activeCameraIdx_]->UpdatePos(CameraDirection::RIGHT, dt);
		}
		// skip normalmapping
		if (Keyboard::KeyWentDown(GLFW_KEY_N))
		{
			bSkipNormalMap_ = !bSkipNormalMap_;
		}

		// update matrices
		viewMat_ = cameras_[activeCameraIdx_]->GetViewMatrix();
		projectionMat_ = glm::perspective(
			glm::radians(cameras_[activeCameraIdx_]->GetZoom()),
			(real32)scrWidth / (real32)scrHeight,
			0.1f, 1000.0f
		);

		cameraPos_ = cameras_[activeCameraIdx_]->GetPos();
	}
}

void
Scene::BeginDraw()
{
	glClearColor(bgColor_[0], bgColor_[1], bgColor_[2], bgColor_[3]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void
Scene::EndDraw(Box& octreeDrawable)
{
	octreeDrawable.positions_.clear();
	octreeDrawable.sizes_.clear();


	octree_->ProcessPending();
	octree_->Update(octreeDrawable);

	glfwSwapBuffers(pWindow_);
	glfwPollEvents();
}

void
Scene::UpdateViewProjectionMatrices(Shader shader)
{
	shader.Use();

	shader.SetMat4("view", viewMat_);
	shader.SetMat4("projection", projectionMat_);
	shader.Set3Float("viewPos", cameraPos_);
}

void
Scene::RenderInstances(string modelID, Shader shader, real32 dt)
{
	shader.SetBool("material.bSkipNormalMap", bSkipNormalMap_);
	Model* model = models_[modelID];
	if (!model)
	{
		std::cout << "Didn't find model to render" << std::endl;
		return;
	}
	// FIXME: logic is messy
	if (model->HasTransparency() &&
		bIsBackFaceCullingON_)
		DisableBackfaceCulling();
	else
		EnableBackfaceCulling();
	models_[modelID]->Render(shader, dt, this);
}

void
Scene::Cleanup()
{
	models_.traverse([](Model* model) -> void {
		model->Cleanup();
	});

	octree_->Destroy();
	glfwTerminate();
}

bool
Scene::ShouldClose()
{
	return glfwWindowShouldClose(pWindow_);
}

void
Scene::SetShouldClose(bool shouldClose)
{
	glfwSetWindowShouldClose(pWindow_, shouldClose);
}

void
Scene::SetBgColor(real32 r, real32 g, real32 b, real32 a)
{
	bgColor_[0] = r;
	bgColor_[1] = g;
	bgColor_[2] = b;
	bgColor_[3] = a;
}

Camera*
Scene::GetActiveCamera()
{
	if (activeCameraIdx_ >= 0 && activeCameraIdx_ < cameras_.size())
		return cameras_[activeCameraIdx_];
	else
		return nullptr;
}

void
Scene::RegisterModel(Model* model)
{
	models_.Insert(model->GetModelID(), model);
}

RigidBody*
Scene::SpawnInstance(string modelID, glm::vec3 pos, glm::vec3 size, glm::vec3 rotation, real32 mass)
{
    RigidBody *newInstance = models_[modelID]->SpawnInstance(pos, size, rotation, mass);
	if(newInstance)
	{
		string id = GetNextID();
		newInstance->SetInstanceID(id);
		instances_.Insert(id, newInstance);
		octree_->AddToPending(newInstance, models_);
		return newInstance;
	}

	return nullptr;
}

void
Scene::InitInstances()
{
	models_.traverse([](Model* model) -> void {
		model->InitInstances();
	});
}

void
Scene::LoadModels()
{
	models_.traverse([](Model* model) -> void {
		model->Init();
	});
}


void
Scene::RemoveInstance(string instanceID)
{
	string modelID = instances_[instanceID]->GetModelID();

	models_[modelID]->RemoveInstance(instanceID);

	instances_[instanceID] = nullptr;

	instances_.erase(instanceID);
}


string Scene::GetNextID()
{
	for (uint32 i  = currentID_.length() - 1;
		 i >= 0;
		 i++)
	{
		if ((int32)currentID_[i] != (int32)'z')
		{
			currentID_[i] = (char)(((int32)currentID_[i] + 1));
			break;
		}
		else
		{
			currentID_[i] = 'a';
		}
	}
	
	return currentID_;
}

void
Scene::MarkForDeletion(string instanceID)
{
	uint8 stateBefore = *(instances_[instanceID]->GetState());
	Bitwise::TurnOnBitByFlag(instances_[instanceID]->GetState(), INSTANCE_DEAD);
	uint8 state = *(instances_[instanceID]->GetState());
	instancesToDel_.push_back(instances_[instanceID]);
}

void
Scene::DeleteDeadInstances()
{
	for (RigidBody* instance : instancesToDel_)
	{
		RemoveInstance(instance->GetInstanceID());
	}
	instancesToDel_.clear();
}

void
Scene::AddCamera(Camera* camera)
{
	cameras_.push_back(camera);
}

void
Scene::SetActiveCamera(int32 index)
{
	activeCameraIdx_ = index;
}

void
Scene::AddPointLight(PointLight* pointLight)
{
	pointLights_.push_back(pointLight);
	Bitwise::TurnOnBitByIndex(&activePointLights_, pointLights_.size() - 1);
}

void
Scene::AddSpotLight(SpotLight* spotLight)
{
	spotLights_.push_back(spotLight);
	Bitwise::TurnOnBitByIndex(&activeSpotLights_, spotLights_.size() - 1);
}
SpotLight*
Scene::GetSpotLight(int32 index)
{
	if (index < 0 || index > spotLights_.size() - 1)
		return nullptr;
	return spotLights_[index];
}

void
Scene::EnableBackfaceCulling()
{
	glEnable(GL_CULL_FACE);
	bIsBackFaceCullingON_ = true;
}

void
Scene::DisableBackfaceCulling()
{
	glDisable(GL_CULL_FACE);
	bIsBackFaceCullingON_ = false;
}
