#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../common.h"

enum class CameraDirection {
	NONE = 0,
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};
class Camera
{
public:
	Camera(glm::vec3 position = glm::vec3(0.0f));

	void UpdateDirection(real64 dx, real64 dy);
	void UpdatePos(CameraDirection dir, real64 dt);
	void UpdateZoom(real64 dt);

	glm::mat4 GetViewMatrix();
	real32 GetZoom() const { return zoom_; }
	glm::vec3 GetPos() const { return pos_; }
	glm::vec3 GetFront() const { return front_; }
	glm::vec3 GetUp() const { return up_; }
	void SetPos(glm::vec3 pos) { pos_ = pos; }
private:
	void UpdateCameraVectors();
	glm::vec3 pos_;
	glm::vec3 front_;
	glm::vec3 up_;
	glm::vec3 right_;

	glm::vec3 worldUp_;

	real32 yaw_;
	real32 pitch_;
	real32 speed_;
	real32 sensitivity_;
	real32 zoom_;
};

#endif
