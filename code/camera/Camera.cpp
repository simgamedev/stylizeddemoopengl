#include "Camera.h"
#include <iostream>
#include "../Utilities.h"

Camera::Camera(glm::vec3 position)
	: pos_(position)
	, worldUp_(glm::vec3(0.0f, 1.0f, 0.0f))
	, yaw_(-90.0f)
	, pitch_(0.0f)
	, speed_(7.5)
	, zoom_(45.0f)
	, front_(glm::vec3(1.0f, 0.0f, 0.0f))
	, sensitivity_(0.05f)
{
	UpdateCameraVectors();
}

void
Camera::UpdateDirection(real64 dx, real64 dy)
{
	yaw_ += dx * sensitivity_;
	pitch_ += dy * sensitivity_;

	if(pitch_ > 89.0f)
	{
		pitch_ = 89.0f;
	}
	else if(pitch_ < -89.0f)
	{
		pitch_ = -89.0f;
	}
	UpdateCameraVectors();
}


void
Camera::UpdateCameraVectors()
{
	glm::vec3 direction;
	direction.x = cos(glm::radians(yaw_)) * cos(glm::radians(pitch_));
	direction.y = sin(glm::radians(pitch_));
	direction.z = sin(glm::radians(yaw_)) * cos(glm::radians(pitch_));
	front_ = glm::normalize(direction);

	right_ = glm::normalize(glm::cross(front_, worldUp_));

	up_ = glm::normalize(glm::cross(right_, front_)); 
}

void
Camera::UpdatePos(CameraDirection dir, real64 dt)
{
	real32 velocity = (real32)dt * speed_;
	switch(dir)
	{
		case CameraDirection::FORWARD:
		{
			pos_ += front_ * velocity;
		} break;
		case CameraDirection::BACKWARD:
		{
			pos_ -=  front_ * velocity;
		} break;
		case CameraDirection::RIGHT:
		{
			pos_ += right_ * velocity;
		} break;
		case CameraDirection::LEFT:
		{
			pos_ -= right_ * velocity;
		} break;
		case CameraDirection::UP:
		{
			pos_ += up_ * velocity;
		} break;
		case CameraDirection::DOWN:
		{
			pos_ -= up_ * velocity;
		} break;
	}
}

void
Camera::UpdateZoom(real64 delta)
{
	if(zoom_ >= 1.0f && zoom_ <= 45.0f)
	{
		zoom_ -= delta;
	}
	else if(zoom_ < 1.0f)
	{
		zoom_ = 1.0f;
	}
	else
	{
		zoom_ = 45.0f;
	}
}

glm::mat4
Camera::GetViewMatrix()
{
	return glm::lookAt(pos_, pos_ + front_, up_);
}


