#include "Mouse.h"

real64 Mouse::x_ = 0;
real64 Mouse::y_ = 0;

real64 Mouse::lastX_ = 0;
real64 Mouse::lastY_ = 0;

real64 Mouse::dx_ = 0;
real64 Mouse::dy_ = 0;

real64 Mouse::scrollDX_ = 0;
real64 Mouse::scrollDY_ = 0;

bool Mouse::firstMouse_ = true;

bool Mouse::buttonsDown_[GLFW_MOUSE_BUTTON_LAST] = { 0 };
bool Mouse::buttonsChanged_[GLFW_MOUSE_BUTTON_LAST] = { 0 };

void
Mouse::CursorPosCallback(GLFWwindow* window, real64 x, real64 y)
{
	x_ = x;
	y_ = y;

	if(firstMouse_)
	{
		lastX_ = x;
		lastY_ = y;
		firstMouse_ = false;
	}

	dx_ = x_ - lastX_;
	dy_ = lastY_ - y_;
	lastX_ = x_;
	lastY_ = y_;
}



void
Mouse::MouseButtonCallback(GLFWwindow* window, int32 button, int32 action, int mods)
{
	if(action != GLFW_RELEASE)
	{
		if(!buttonsDown_[button])
		{
			buttonsDown_[button] = true;
		}
	}
	else
	{
		buttonsDown_[button] = false;
	}
}

void
Mouse::MouseWheelCallback(GLFWwindow* window, real64 dx, real64 dy)
{
	scrollDX_ = dx;
	scrollDY_ = dy;
}

real64
Mouse::GetMouseX()
{
	return x_;
}

real64
Mouse::GetMouseY()
{
	return y_;
}

real64
Mouse::GetDeltaX()
{
	real64 dx = dx_;
	dx_ = 0;
	return dx;
}

real64
Mouse::GetDeltaY()
{
	real64 dy = dy_;
	dy_ = 0;
	return dy;
}

real64
Mouse::GetScrollDX()
{
	return 0;
}

real64
Mouse::GetScrollDY()
{
	return 0;
}

bool
Mouse::IsButtonDown(int32 button)
{
	return buttonsDown_[button];
}

bool
Mouse::ButtonChanged(int32 button)
{
	bool ret = buttonsChanged_[button];
	buttonsChanged_[button] = false;
	return ret;
}

bool
Mouse::ButtonWentUp(int32 button)
{
	return false;
}

bool
Mouse::ButtonWentDown(int32 button)
{
	return false;
}
