#ifndef MOUSE_H
#define MOUSE_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "../common.h"

class Mouse
{
public:
	static void CursorPosCallback(GLFWwindow* window, real64 x, real64 y);
	static void MouseButtonCallback(GLFWwindow* window, int32 button, int32 action, int mods);
	static void MouseWheelCallback(GLFWwindow* window, real64 dx, real64 dy);

	static real64 GetMouseX();
	static real64 GetMouseY();

	static real64 GetDeltaX();
	static real64 GetDeltaY();

	static real64 GetScrollDX();
	static real64 GetScrollDY();

	static bool IsButtonDown(int32 button);
	static bool ButtonChanged(int32 button);
	static bool ButtonWentUp(int32 button);
	static bool ButtonWentDown(int32 button);
private:
	static real64 x_;
	static real64 y_;

	static real64 lastX_;
	static real64 lastY_;

	static real64 dx_;
	static real64 dy_;

	static real64 scrollDX_;
	static real64 scrollDY_;

	static bool firstMouse_;

	static bool buttonsDown_[];
	static bool buttonsChanged_[];
};

#endif MOUSE_H
