#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <GLFW/glfw3.h>
#include "../common.h"

class Keyboard
{
public:
	//
	static void KeyCallback(GLFWwindow* window, int32 key, int32 scancode, int32 action, int32 mods);

	static bool IsKeyDown(int32 key);
	static bool KeyChanged(int32 key);
	static bool KeyWentUp(int32 key);
	static bool KeyWentDown(int32 key);
private:
	static bool keysDown_[];
	static bool keysChanged_[];
};
#endif
