#include "Keyboard.h"

bool Keyboard::keysDown_[GLFW_KEY_LAST] = { 0 };
bool Keyboard::keysChanged_[GLFW_KEY_LAST] = { 0 };



void
Keyboard::KeyCallback(GLFWwindow* window, int32 key, int32 scancode, int32 action, int32 mods)
{
	if(action != GLFW_RELEASE)
	{
		// key went down
		if(!keysDown_[key])
		{
			keysDown_[key] = true;
		}
	}
	else
	{
		keysDown_[key] = false;
	}

	keysChanged_[key] = action != GLFW_REPEAT;
}

bool
Keyboard::IsKeyDown(int32 key)
{
	return keysDown_[key];
}

bool
Keyboard::KeyChanged(int32 key)
{
	bool ret = keysChanged_[key];
	keysChanged_[key] = false;
	return ret;
}

bool
Keyboard::KeyWentUp(int32 key)
{
	return !keysDown_[key] && KeyChanged(key);
}

bool
Keyboard::KeyWentDown(int32 key)
{
	return keysDown_[key] && KeyChanged(key);
}
