#ifndef OCTREE_H
#define OCTREE_H

#include "../common.h"

#include <vector>
#include <queue>
#include <stack>

#include "List.hpp"
#include "Bitwise.hpp"
#include "../graphics/Model.h"
#include "Trie.hpp"

#define NUM_CHILDREN 8
#define MIN_BOUNDS 0.5

class BoundingRegion;
class Box;
namespace Octree
{


	// TODO: rename Octant to OctantPos
enum class Octant : uint8
{
	O1 = 0x01,
	O2 = 0x02,
	O3 = 0x04,
	O4 = 0x08,
	O5 = 0x10,
	O6 = 0x20,
	O7 = 0x40,
	O8 = 0x80,
};

// calculate bonds of specified octant in bounding region
void CalculateBounds(BoundingRegion& result, Octant octant, BoundingRegion parentRegion);

class Node
{
public:
	uint8 activeOctants_ = 0;
public:
	Node();
	Node(BoundingRegion region);
	Node(BoundingRegion region, std::vector<BoundingRegion> objects);


	void AddToPending(RigidBody* instance, Trie::Trie<Model*> models);

	bool Insert(BoundingRegion objectBR);

	void CheckCollisionsSelf(BoundingRegion object);
	void CheckCollisionsChildren(BoundingRegion object);

	void Update(Box& drawable);
	void Build();
	void Destroy();

	Node* GetParent() const { return parent_; }
	void SetParent(Node* parent) { parent_ = parent; }
	void ProcessPending();
private:
	BoundingRegion region_;
	std::vector<BoundingRegion> objects_;
	std::queue<BoundingRegion> pendingQ_;

	Node* parent_ = nullptr;
	Node* children_[NUM_CHILDREN] = { nullptr };
	bool bHasChildren_ = false;
	bool bTreeReady_ = false;
	bool bTreeBuilt_ = false;
	int16 maxLifeSpan_ = 8;
	int16 currentLifeSpan_ = -1;
};





};
#endif
