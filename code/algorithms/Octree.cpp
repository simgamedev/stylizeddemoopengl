#include "Octree.h"
#include "../graphics/primitives/Box.hpp"



// calculate bonds of specified octant in bounding region
// TODO: refactor BoundingRegion to struct BoudingRegion
void
Octree::CalculateBounds(BoundingRegion& result, Octant octant, BoundingRegion parentRegion)
{
    glm::vec3 center = parentRegion.CalcCenter();
    if (octant == Octant::O1) {
        result = BoundingRegion(center, parentRegion.GetMax());
    }
    else if (octant == Octant::O2) {
        result = BoundingRegion(glm::vec3(parentRegion.GetMin().x, center.y, center.z), glm::vec3(center.x, parentRegion.GetMax().y, parentRegion.GetMax().z));
    }
    else if (octant == Octant::O3) {
        result = BoundingRegion(glm::vec3(parentRegion.GetMin().x, parentRegion.GetMin().y, center.z), glm::vec3(center.x, center.y, parentRegion.GetMax().z));
    }
    else if (octant == Octant::O4) {
        result = BoundingRegion(glm::vec3(center.x, parentRegion.GetMin().y, center.z), glm::vec3(parentRegion.GetMax().x, center.y, parentRegion.GetMax().z));
    }
    else if (octant == Octant::O5) {
        result = BoundingRegion(glm::vec3(center.x, center.y, parentRegion.GetMin().z), glm::vec3(parentRegion.GetMax().x, parentRegion.GetMax().y, center.z));
    }
    else if (octant == Octant::O6) {
		result = BoundingRegion(glm::vec3(parentRegion.GetMin().x, center.y, parentRegion.GetMin().z), glm::vec3(center.x, parentRegion.GetMax().y, center.z));
    }
    else if (octant == Octant::O7) {
        result = BoundingRegion(parentRegion.GetMin(), center);
    }
    else if (octant == Octant::O8) {
        result = BoundingRegion(glm::vec3(center.x, parentRegion.GetMin().y, parentRegion.GetMin().z), glm::vec3(parentRegion.GetMax().x, center.y, center.z));
    }
}


Octree::Node::Node()
	: region_(BoundType::AABB)
{
}

Octree::Node::Node(BoundingRegion region)
	: region_(region)
{
}


Octree::Node::Node(BoundingRegion bounds, std::vector<BoundingRegion> objects)
	: region_(bounds)
{
	objects_.insert(objects_.end(), objects.begin(), objects.end());
}

void
Octree::Node::AddToPending(RigidBody* instance, Trie::Trie<Model*> models)
{
	for (BoundingRegion br : models[instance->GetModelID()]->GetBoundingRegions())
	{
		br.SetOwner(instance);
		br.Transform();
		pendingQ_.push(br);
	}
}

void
Octree::Node::Build()
{
	glm::vec3 regionDimensions = region_.CalcDimensions();
	std::vector<BoundingRegion> octantObjLists[NUM_CHILDREN];
	BoundingRegion octantBRs[NUM_CHILDREN];
	// ===> TERMINATION CONDITIONS
	// no more than 1 object
	if (objects_.size() <= 1)
	{
		/*
		bTreeBuilt_ = true;
		bTreeReady_ = true;
		return;
		*/
		goto setVars;
	}

	// node too small
	for(int32 i = 0; i < 3; i++)
	{
		if (regionDimensions[i] < MIN_BOUNDS)
		{
			/*
			bTreeBuilt_ = true;
			bTreeReady_ = true;
			return;
			*/
			goto setVars;
		}
	}
	// ===< TERMINATION CONDITIONS

	// ===> CREATE OCTANTS
	for(int32 i = 0; i < NUM_CHILDREN; i++)
	{
		CalculateBounds(octantBRs[i], (Octant)(1 << i),  region_);
	}
	// ===< CREATE OCTANTS

	// ===> DETERMINE WHICH OCTANT TO PLACE OBJECTS IN
	for (int32 i = 0, length = objects_.size(); i < length; i++)
	{
		BoundingRegion objectBR = objects_[i];
		for(int32 j = 0; j < NUM_CHILDREN; j++)
		{
			if(octantBRs[j].ContainsRegion(objectBR))
			{
				octantObjLists[j].push_back(objectBR);
				objects_.erase(objects_.begin() + i);
				i--;
				length--;
				break;
			}
		}
	}
	// ===< DETERMINE WHICH OCTANT TO PLACE OBJECTS IN

	// ===> POPULATE OCTANTS
	for (int32 i = 0; i < NUM_CHILDREN; i++)
	{
		if (octantObjLists[i].size() != 0)
		{
			children_[i] = new Node(octantBRs[i], octantObjLists[i]);
			Bitwise::TurnOnBitByIndex(&activeOctants_, i);
			children_[i]->SetParent(this);
			children_[i]->Build();
			bHasChildren_ = true;
		}
	}
	// ===< POPULATE OCTANTS

setVars:
	bTreeBuilt_ = true;
	bTreeReady_ = true;
	
	for (int32 i = 0; i < objects_.size(); i++)
	{
		objects_[i].SetCell(this);
	}
}

void
Octree::Node::Update(Box& drawable)
{
	if (bTreeBuilt_ && bTreeReady_)
	{
		// ===> DRAWABLE
		drawable.positions_.push_back(region_.CalcCenter());
		drawable.sizes_.push_back(region_.CalcDimensions());
		// ===< DRAWABLE

		// ===> LIFESPAN COUNTDOWN
		// if no more object occuplies this node, start count down
		if (objects_.size() == 0)
		{
			if (!bHasChildren_)
			{
				if (currentLifeSpan_ == -1)
				{
					currentLifeSpan_ = maxLifeSpan_;
				}
				else if (currentLifeSpan_ > 0)
				{
					currentLifeSpan_--;
				}
			}
		}
		else
		{
			if (currentLifeSpan_ != -1)
			{
				if (maxLifeSpan_ <= 64)
				{
					// extend lifespan for potential "hotspot"
					maxLifeSpan_ <<= 2;
				}
			}
		}
		// ===< LIFESPAN COUNTDOWN


		// ===> REMOVE OBJECTS THAT DONT EXIST ANYMORE
		// remove objects don't exist anymore
		for (int32 i = 0, listSize = objects_.size(); i < listSize; i++)
		{

			if (Bitwise::IsBitOnByFlag(objects_[i].GetOwner()->GetState(), INSTANCE_DEAD))
			{
				objects_.erase(objects_.begin() + i);
				i--;
				listSize--;
			}
		}
		// ===< REMOVE OBJECTS THAT DON'T EXIST ANYMORE


		// ===> UPDATE MOVED OBJECTS
		std::stack<int32> movedObjects;
		if (parent_ == nullptr && objects_.size() == 4)
		{
			int32 a = 100;
		}
		for (int32 i = 0, listSize = objects_.size(); i < listSize; i++)
		{
			if (Bitwise::IsBitOnByFlag(objects_[i].GetOwner()->GetState(), INSTANCE_MOVED))
			{
				objects_[i].Transform();
				movedObjects.push(i);
			}
			drawable.positions_.push_back(objects_[i].CalcCenter());
			drawable.sizes_.push_back(objects_[i].CalcDimensions());
		}
		//std::vector<BoundingRegion> movedObjects(objectBRs_.size());
		// ===< UPDATE MOVED OBJECTS

		// ===> REMOVE EMPTY OCTANTS
		uint8 flags = activeOctants_;
		for (int32 i = 0;
			 flags > 0;
			 flags >>= 1, i++) // looping activeOctants_
		{
			if (Bitwise::IsBitOnByIndex(&flags, 0) && children_[i]->currentLifeSpan_ == 0)
			{
				if (children_[i]->objects_.size() > 0)
				{
					// update octant lifespan
					children_[i]->currentLifeSpan_ = -1;
				}
				else // octant is totally dead(no lifespand && no children)
				{
					children_[i] = nullptr;
					Bitwise::TurnOffBitByIndex(&activeOctants_, i);
				}
			}
		}
		// ===< REMOVE EMPTY OCTANTS

		// ===> UPDATE CHILDREN NODES
		if (children_ != nullptr)
		{
			// iterate each octant
			for (uint8 flags = activeOctants_, i = 0;
				 flags > 0;
				 flags >>= 1, i++)
			{
				if (Bitwise::IsBitOnByIndex(&flags, 0))
				{
					// active octant
					if (children_[i] != nullptr)
					{
						children_[i]->Update(drawable);
					}
				}
			}
		}
		// ===< UPDATE CHILDREN NODES

		// ===> MOVE MOVED OBJECTS TO NEW NODE
		BoundingRegion movedObj;
		while (movedObjects.size() != 0)
		{
			// find new octant(could still be in the same octant)
			// by traversing up the tree
			movedObj = objects_[movedObjects.top()];
			Node* currentNode = this;
			while (!currentNode->region_.ContainsRegion(movedObj))
			{
				if (currentNode->parent_ != nullptr)
				{
					currentNode = currentNode->parent_;
				}
				else
					break; // if current node is root node, then stop
			}

			objects_.erase(objects_.begin() + movedObjects.top());
			movedObjects.pop();
			currentNode->Insert(movedObj);
			// ===<
			// ===< MOVE MOVED OBJECTS TO NEW NODE
			// ===> COLLISION DETECTION
			currentNode = movedObj.GetCell();
			// self
			currentNode->CheckCollisionsSelf(movedObj);
			// children
			currentNode->CheckCollisionsChildren(movedObj);
			// parents
			while (currentNode->GetParent())
			{
				currentNode = currentNode->GetParent();
				currentNode->CheckCollisionsSelf(movedObj);
			}
			// ===< COLLISION DETECTION
		}
	}
	else
	{
		if (pendingQ_.size() > 0)
		{
			ProcessPending();
		}
	}
}

void
Octree::Node::ProcessPending()
{
	if (pendingQ_.size() == 0)
		return;

	if (!bTreeBuilt_)
	{
		while (pendingQ_.size() != 0)
		{
			objects_.push_back(pendingQ_.front());
			pendingQ_.pop();
		}
		Build();
	}
	else
	{
		for (int32 i = 0, length = pendingQ_.size(); i < length; i++)
		{
			BoundingRegion br = pendingQ_.front();
			if (region_.ContainsRegion(br))
			{
				// insert object immediately
				Insert(br);
			}
			else
			{
				// put it back in queue
				br.Transform();
				pendingQ_.push(br);
			}
			pendingQ_.pop();
		}
	}
}

bool
Octree::Node::Insert(BoundingRegion object)
{
	// ===> TERMINATION(SUBDIVIDE FURTHER)
	glm::vec3 dimensions = region_.CalcDimensions();
	if (objects_.size() == 0 ||
		dimensions.x < MIN_BOUNDS ||
		dimensions.y < MIN_BOUNDS ||
		dimensions.z < MIN_BOUNDS)
	{
		object.SetCell(this);
		objects_.push_back(object);
		return true;
	}
	// ===< TERMINATION(SUBDIVIDE FURTHER)

	// ===> SAFEGUARD
	if (!region_.ContainsRegion(object))
	{
		return parent_ == nullptr ? false : parent_->Insert(object);
	}
	// ===< SAFEGUARD


	// ===> CREATE OCTANTS
	BoundingRegion octantBRs[NUM_CHILDREN];
	for (int32 i = 0; i < NUM_CHILDREN; i++)
	{
		if (children_[i] != nullptr)
		{
			octantBRs[i] = children_[i]->region_;
		}
		else
		{
			CalculateBounds(octantBRs[i], (Octant)(1 << i), region_);
		}
	}
	// ===< CREATE OCTANTS

	// ===>
	objects_.push_back(object);
	// ===<

	// ===> DETERMINE WHICH OCTANT TO PUT OBJECTS IN
	std::vector<BoundingRegion> octantObjLists[NUM_CHILDREN];
	for (uint32 i = 0, length = objects_.size(); i < length; i++)
	{
		for (int j = 0; j < NUM_CHILDREN; j++)
		{
			objects_[i].SetCell(this);
			if (octantBRs[j].ContainsRegion(objects_[i]))
			{
				octantObjLists[j].push_back(objects_[i]);
				objects_.erase(objects_.begin() + i);
				i--;
				length--;
				break;
			}
		}
	}
	// ===<

	// ===> POPULATE OCTANTS
	for (int32 i = 0; i < NUM_CHILDREN; i++)
	{
		if (octantObjLists[i].size() != 0)
		{
			// 
			// TODO: children are just octants right?!!
			if (children_[i]) {
				for (BoundingRegion br : octantObjLists[i])
				{
					children_[i]->Insert(br);
				}
			}
			else
			{
				children_[i] = new Node(octantBRs[i], octantObjLists[i]);
				children_[i]->SetParent(this);
				Bitwise::TurnOnBitByIndex(&activeOctants_, i);
				children_[i]->Build();
				bHasChildren_ = true;
			}
		}
	}

	return true;
	// ===< POPULATE OCTANTS
}



void
Octree::Node::CheckCollisionsSelf(BoundingRegion collider)
{
	for (BoundingRegion collidee : objects_)
	{
		if (collidee.IntersectsWith(collider))
		{
			if (collidee.GetOwner()->GetInstanceID() ==
				collider.GetOwner()->GetInstanceID())
				; // COLLIDING WITH SELF
			else
			{
				std::cout << "Instance " 
					<< collidee.GetOwner()->GetInstanceID()
					<< "(" << collidee.GetOwner()->GetModelID() 
					<< ") collides with"
					<< collider.GetOwner()->GetInstanceID() << "("
					<< collider.GetOwner()->GetModelID() << ")" << std::endl;
			}
		}
	}
}


void
Octree::Node::CheckCollisionsChildren(BoundingRegion collider)
{
	if (children_)
	{
		for (uint8 flags = activeOctants_, i = 0;
			 flags > 0;
			 flags >>= 1, i++)
		{
			if (Bitwise::IsBitOnByIndex(&flags, 0) && children_[i])
			{
				children_[i]->CheckCollisionsSelf(collider);
				children_[i]->CheckCollisionsChildren(collider);
			}
		}
	}
}

void
Octree::Node::Destroy()
{
	if (children_ != nullptr)
	{
		for (uint8 flags = activeOctants_, i = 0;
			 flags > 0;
			 flags >>= 1, i++)
		{
			if (Bitwise::IsBitOnByIndex(&flags, 0))
			{
				if (children_[i] != nullptr)
				{
					children_[i]->Destroy();
					children_[i] = nullptr;
				}
			}
		}
	}

	objects_.clear();
	while (pendingQ_.size() != 0)
	{
		pendingQ_.pop();
	}
}
