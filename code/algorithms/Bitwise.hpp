#ifndef BITWISE_H
#define BITWISE_H
#include "../common.h"
/*
    e.g: flasg:0b00000001 then flags[pos=0] = 1
	*/
namespace Bitwise
{
	template<typename T>
	void ToggleBitByIndex(T* flags, int32 pos)
	{
		*flags ^= 1 << pos;
	}

	template<typename T>
	void TurnOnBitByIndex(T* flags, int32 pos)
	{
		*flags |= 1 << pos;
	}

	template<typename T>
	void TurnOnBitByFlag(T* flags, T flag)
	{
		*flags |= flag;
	}

	template<typename T>
	void TurnOffBitByIndex(T* flags, int32 pos)
	{
		*flags &= ~(1 << pos);
	}

	template<typename T>
	void TurnOffBitByFlag(T* flags, T flag)
	{
		*flags &= ~flag;
	}

	template<typename T>
	bool IsBitOnByFlag(T* flags, T flag)
	{
		return (*flags & flag) == flag;
	}

	template<typename T>
	bool IsBitOnByIndex(T* flags, int32 index)
	{
		return (*flags & (1 << index)) == (1 << index);
	}

};
#endif
