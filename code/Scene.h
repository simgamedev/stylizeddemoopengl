#ifndef SCENE_H
#define SCENE_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <vector>
#include <map>
#include <glm/glm.hpp>

#include "graphics/Light.h"
#include "graphics/Shader.h"
#include "graphics/primitives/Box.hpp"
#include "camera/Camera.h"
#include "input/Keyboard.h"
#include "input/Mouse.h"

#include "algorithms/Trie.hpp"
#include "algorithms/Octree.h"
#include "graphics/UniformMemory.hpp"

// forward delcarations
namespace Octree {
	class Node;
}

class Model;

class Scene
{
public:


	Scene();
	Scene(int32 glfwVersionMajor, int32 glfwVersionMinor,
		  const char* titel, uint32 scrWidth, uint32 scrHeight);

	bool Init();
	void PrepareOctree(Box& octreeDrawable);
	void PrepareShaders(std::vector<Shader> shaders);
	void UpdateShadersUBOs();

	void ProcessInput(real32 dt);
	void BeginDraw();
	void EndDraw(Box& octreeDrawable);
	static void framebufferSizeCallback(GLFWwindow* window, int32 width, int32 height);
	// TODO CHANGE NAME TO PREPARE_SHADER?
	void UpdateViewProjectionMatrices(Shader shader);
	void RenderInstances(string modelID, Shader shader, real32 dt);
	void RegisterModel(Model* model);
	RigidBody* SpawnInstance(string modelID,
							 glm::vec3 pos,
							 glm::vec3 size,
							 glm::vec3 rotation = glm::vec3(0.0f),
							 real32 mass = 1.0f);
	void InitInstances();
	void LoadModels();
	void RemoveInstance(string instanceID);
	string currentID_;
	string GetNextID();
	glm::mat4 GetProjectionMatrix() const { return projectionMat_; }

	void MarkForDeletion(string instanceID);
	void DeleteDeadInstances();

	void Cleanup();
	bool ShouldClose();
	Camera* GetActiveCamera();
	int32 activeCameraIdx_;

	void SetShouldClose(bool shouldClose);
	void SetBgColor(real32 r, real32 g, real32 b, real32 a);

	void AddCamera(Camera* camera);
	void SetActiveCamera(int32 index);
	void SetDirLight(DirLight* dirLight) { dirLight_ = dirLight; }
	void AddPointLight(PointLight* pointLight);
	void AddSpotLight(SpotLight* spotLight);
	SpotLight* GetSpotLight(int32 index);
	GLFWwindow* GetWindow() const { return pWindow_; }
	// bitwise switches
	uint32 activePointLights_;
	uint32 activeSpotLights_;
private:
	std::vector<Camera*> cameras_;

	// ===> Lights
	bool dirLightActive_;
	uint32 numPointLights_;
	std::vector<PointLight*> pointLights_;
	uint32 numSpotLights_;
	std::vector<SpotLight*> spotLights_;
	DirLight* dirLight_;

	UBO::UBO lightsUBO_;
	// ===< Lights


	glm::mat4 viewMat_;
	glm::mat4 projectionMat_;
	glm::vec3 cameraPos_;

	GLFWwindow* pWindow_;
	const char* title_;
    real32 bgColor_[4];
	int32 glfwVersionMajor_;
	int32 glfwVersionMinor_;
	static uint32 scrWidth;
	static uint32 scrHeight;

	Octree::Node* octree_;
	Trie::Trie<RigidBody*> instances_;
	Trie::Trie<Model*> models_;
	std::vector<RigidBody*> instancesToDel_;

	bool bSkipNormalMap_;

	bool bIsBackFaceCullingON_;
	void EnableBackfaceCulling();
	void DisableBackfaceCulling();
};

#endif
