#include "RigidBody.h"
#include <iostream>


bool
RigidBody::operator==(RigidBody rb)
{
	return instanceID_ == rb.instanceID_;
}
bool

RigidBody::operator==(string id)
{
	return instanceID_ == id;
}

RigidBody::RigidBody() {}

RigidBody::RigidBody(string modelID,
					 glm::vec3 pos,
					 glm::vec3 size,
					 glm::vec3 rotation,
					 real32 mass)
	: modelID_(modelID)
	, pos_(pos)
	, size_(size)
	, eulerRot_(rotation)
	, mass_(mass)
	, velocity_(0.0f)
	, accel_(0.0f)
{
}


void
RigidBody::Update(real32 dt)
{
	pos_ += velocity_*dt + 0.5f*accel_ * (dt*dt);
	velocity_ += accel_ * dt;
}


void
RigidBody::ApplyForce(glm::vec3 force)
{
	accel_ += force / mass_;
}

void
RigidBody::ApplyForce(glm::vec3 direction, real32 magnitude)
{
	ApplyForce(direction * magnitude);
}

void
RigidBody::ApplyImpulse(glm::vec3 force, real32 dt)
{
	velocity_ += force / mass_ * dt;
}

void
RigidBody::ApplyImpulse(glm::vec3 direction, real32 magnitude, real32 dt)
{
	ApplyImpulse(direction * magnitude, dt);
}

void
RigidBody::ApplyAcceleration(glm::vec3 accel)
{
	accel_ += accel;
}

void
RigidBody::ApplyAcceleration(glm::vec3 direction, real32 magnitude)
{
	ApplyAcceleration(direction * magnitude);
}


void
RigidBody::TransferEnergy(real32 joules, glm::vec3 direction)
{
	if (joules == 0)
	{
		return;
	}


	// formula: KE = 1/2 * m * v^2
	glm::vec3 deltaV = sqrt(2 * abs(joules) / mass_) * direction;

	velocity_ += joules > 0 ? deltaV : -deltaV;
}
	
