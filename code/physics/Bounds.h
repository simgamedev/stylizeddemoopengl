#ifndef BOUNDS_H
#define BOUNDS_H

#include <glm/glm.hpp>
#include "../common.h"
#include "../physics/RigidBody.h"

namespace Octree
{
	class Node;
}

enum class BoundType : uint8 {
	AABB = 0x00,
	SPHERE = 0x01,
};

class BoundingRegion
{
public:
	BoundingRegion(BoundType type = BoundType::AABB);
	BoundingRegion(glm::vec3 center, real32 radius);
	BoundingRegion(glm::vec3 min, glm::vec3 max);


	bool ContainsPoint(glm::vec3 point);
	bool ContainsRegion(BoundingRegion br);
	bool IntersectsWith(BoundingRegion br);

	void Transform();
	void SetOwner(RigidBody* owner) { owner_ = owner; }
	RigidBody* GetOwner() const { return owner_; }
	glm::vec3 GetMin() const { return min_; }
	glm::vec3 GetMax() const { return max_; }
	void SetMin(glm::vec3 min) { min_ = min; originalMin_ = min; }
	void SetMax(glm::vec3 max) { max_ = max; originalMax_ = max; }
	void SetCenter(glm::vec3 center) { center_ = center; originalCenter_ = center; }
	glm::vec3 GetCenter() { return center_; }
	glm::vec3 CalcCenter();
	glm::vec3 CalcDimensions();
	void SetRadius(real32 radius) { radius_ = radius; originalRadius_ = radius; }
	real32 GetRadius() { return radius_; }
	Octree::Node* GetCell() { return octreeNode_; }
	void SetCell(Octree::Node* cell) { octreeNode_ = cell; }
	BoundType GetType() { return type_; }

	// operator overload
	bool operator==(BoundingRegion br);
private:
	BoundType type_;

	// sphere
	glm::vec3 center_;
	real32 radius_;
	glm::vec3 originalCenter_;
	real32 originalRadius_;


	//  aabb
	glm::vec3 min_;
	glm::vec3 max_;
	glm::vec3 originalMin_;
	glm::vec3 originalMax_;

	// 
	RigidBody* owner_;
	Octree::Node* octreeNode_;
};
#endif
