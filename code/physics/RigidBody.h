#ifndef RIGID_BODY_H
#define RIGID_BODY_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include "../common.h"

#define INSTANCE_DEAD (uint8)0b00000001
#define INSTANCE_MOVED (uint8)0b00000010

class RigidBody
{
public:
	bool operator==(RigidBody rb);
	bool operator==(string id);
public:
	RigidBody();
	RigidBody(string modelID,
			  glm::vec3 pos = glm::vec3(0.0f),
			  glm::vec3 size = glm::vec3(1.0f),
			  glm::vec3 rotation = glm::vec3(0.0f),
			  real32 mass = 1.0f);


	void Update(real32 dt);

	void ApplyForce(glm::vec3 force);
	void ApplyForce(glm::vec3 direction, real32 magnitude);
	void ApplyAcceleration(glm::vec3 accel);
	void ApplyAcceleration(glm::vec3 direction, real32 magnitude);
	void ApplyImpulse(glm::vec3 force, real32 dt);
	void ApplyImpulse(glm::vec3 direction, real32 magnitude, real32 dt);
	void TransferEnergy(real32 joules, glm::vec3 direction);

	void SetPos(glm::vec3 pos) { pos_ = pos; }
	glm::vec3 GetPos() const { return pos_; }
	glm::vec3 GetSize() { return size_; }
	glm::vec3 GetEulerRotation() const { return eulerRot_; }
	void SetMass(real32 mass) { mass_ = mass; }
	string GetModelID() const { return modelID_; }
	void SetInstanceID(const string& instanceID) { instanceID_ = instanceID; }
	string GetInstanceID() const { return instanceID_; }
	uint8* GetState() { return &state_; }
	uint8 state_ = 0;
private:
	glm::vec3 pos_;
	glm::vec3 size_;
	glm::vec3 eulerRot_;
	glm::quat quaternionRot_;
	real32 mass_;
	glm::vec3 velocity_;
	glm::vec3 accel_;

	string modelID_;
	string instanceID_;
};

#endif
