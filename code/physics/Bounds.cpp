#include "Bounds.h"
#include "../algorithms/Octree.h"
#include "../Utilities.h"


/****************************** Ctors ******************************
 *
 ********************************************************************************/
BoundingRegion::BoundingRegion(BoundType type)
	: type_(type)
{
}

BoundingRegion::BoundingRegion(glm::vec3 center, real32 radius)
	: type_(BoundType::SPHERE)
	, center_(center)
	, originalCenter_(center)
	, radius_(radius)
	, originalRadius_(radius)
{
}


BoundingRegion::BoundingRegion(glm::vec3 min, glm::vec3 max)
	: type_(BoundType::AABB)
	, min_(min)
	, originalMin_(min)
	, max_(max)
	, originalMax_(max)
{
}

glm::vec3
BoundingRegion::CalcCenter()
{
	return (type_ == BoundType::AABB) ? (min_ + max_) / 2.0f : center_;
}

glm::vec3
BoundingRegion::CalcDimensions()
{
	return (type_ == BoundType::AABB) ? (max_ - min_) : glm::vec3(2.0f*radius_);
}


bool
BoundingRegion::ContainsPoint(glm::vec3 point)
{
	if(type_ == BoundType::AABB)
	{
		return (point.x >= min_.x) && (point.x <= max_.x) &&
			(point.y >= min_.y) && (point.y <= max_.y) &&
			(point.z >= min_.z) && (point.z <= max_.z);
	}
	else
	{
		real32 distSquared = 0.0f;
		for(int32 i = 0; i < 3; i++)
		{
			distSquared  += (center_[i] - point[i]) * (center_[i] = point[i]);
		}	
		return distSquared <= (radius_ * radius_);
	}
}

bool
BoundingRegion::ContainsRegion(BoundingRegion br)
{
	// FIXME:
	// ===> CASE 1
	if (br.GetType() == BoundType::AABB)
	{
		return ContainsPoint(br.GetMin()) && ContainsPoint(br.GetMax());
	}
	// ===< CASE 1
	// ===> CASE 2
	else if (br.GetType() == BoundType::SPHERE &&
			 type_ == BoundType::SPHERE)
	{
		return glm::length(center_ - br.GetCenter()) + br.GetRadius() < radius_;
	}
	// ===< CASE 2
	// ===> CASE 3
	else
	{
		// if this is a box and br is a sphere
		if (!ContainsPoint(br.GetCenter())) {
			// center is outside of the box
			return false;
		}

		// center inside the box
		for (int i = 0; i < 3; i++) {
			if (abs(max_[i] - br.GetCenter()[i]) < br.GetRadius() ||
				abs(br.GetCenter()[i] - min_[i]) < br.GetRadius()) {
				return false;
			}
		}

		return true;
	}
	// ===< CASE 3
}

bool 
BoundingRegion::IntersectsWith(BoundingRegion br)
{
	// overlap on all axes

	if (type_ == BoundType::AABB && br.GetType() == BoundType::AABB) {
		// both boxes
		glm::vec3 rad = CalcDimensions() / 2.0f;	
		glm::vec3 radBr = br.CalcDimensions() / 2.0f;			

		glm::vec3 center = CalcCenter();					
		glm::vec3 centerBr = br.CalcCenter();				

		glm::vec3 dist = abs(center - centerBr);

		for (int i = 0; i < 3; i++) {
			if (dist[i] > rad[i] + radBr[i]) {
				// no overlap on this axis
				return false;
			}
		}

		// failed to prove wrong on each axis
		return true;
	}
	else if (type_ == BoundType::SPHERE && br.GetType() == BoundType::SPHERE) {
		return glm::length(center_ - br.GetCenter()) < (radius_ + br.GetRadius());
	}
	else if (type_ == BoundType::SPHERE) {
		// this is a sphere, br is a box
		real32 distSquared = 0.0f;
		for (int32 i = 0; i < 3; i++) {
			// determine closest side
			real32 closestPt = std::max(br.GetMin()[i],
										std::min(center_[i], br.GetMax()[i]));
			// add squared distance
			distSquared += (closestPt - center_[i]) * (closestPt - center_[i]);
		}

		return distSquared < Math::Square(radius_);
	}
	else {
		// this is a box, br is a sphere
		// call algorithm for br (defined in preceding else if block)
		return br.IntersectsWith(*this);
	}
}

// operator overload
bool 
BoundingRegion::operator==(BoundingRegion br)
{
	if (type_ != br.GetType()) {
		return false;
	}

	if (type_ == BoundType::AABB) {
		return min_ == br.GetMin() && max_ == br.GetMax();
	}
	else {
		return center_ == br.GetCenter() && radius_ == br.GetRadius();
	}
}



void
BoundingRegion::Transform()
{
	if (owner_)
	{
		if (type_ == BoundType::AABB)
		{
			min_ = originalMin_ * owner_->GetSize() + owner_->GetPos();
			max_ = originalMax_ * owner_->GetSize() + owner_->GetPos();
		}
		else
		{
			center_ = originalCenter_ * owner_->GetSize() + owner_->GetPos();
			radius_ = originalRadius_ * owner_->GetSize().x;
		}
	}
}
