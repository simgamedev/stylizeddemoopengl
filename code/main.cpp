#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>


#include "Utilities.h"
#include <fstream>
#include <sstream>
#include <streambuf>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "graphics/Shader.h"
#include "input/Keyboard.h"
#include "input/Mouse.h";
#include "camera/Camera.h"
//#include "Screen.h"
#include "Scene.h"

#include "graphics/Model.h"
#include "graphics/Texture.h"
#include "graphics/primitives/Cube.hpp"
#include "graphics/primitives/Lamp.hpp"
#include "graphics/primitives/Gun.hpp"
#include "graphics/primitives/Sphere.hpp"
#include "graphics/primitives/Box.hpp"
#include "graphics/primitives/Brickwall.hpp"
#include "graphics/Cubemap.h"
#include "graphics/Light.h"
#include "physics/Enviroment.h"
#include "physics/Bounds.h"

#include "algorithms/Bitwise.hpp"
#include "graphics/UniformMemory.hpp"
#include "graphics/Material.h"
#include "graphics/MaterialLand.h"

#include <chrono>
#include <random>


void processInput(real64 dt);
void init_imgui();
void imgui_begindraw();
void imgui_drawdebugpanel();
void imgui_enddraw();
void load_models();
void load_enviroment_models();
void load_foliages_models();
void load_rails_crane_models();
void load_containers_models();
void spawn_rails_crane_models_instances();
void spawn_foliages_models_instances();
void spawn_models_instances();
void spawn_enviroment_models_instances();
void spawn_containers_models_instances();
// NOTE: origin => Mid_Left
void spawn_walls_westeast(glm::vec3 origin);
void spawn_walls_northsouth(glm::vec3 origin);
void draw_models_instances(Shader phongShader);

real64 lastFrame;
real32 mixVal = 0.5f;
uint32 SCR_WIDTH = 1680, SCR_HEIGHT = 1050;
//Screen screen;
Scene scene;
Camera camera(glm::vec3(12.5f, 1.6f, 14.0f));
Camera oldCamera;

real64 dt = 0.0f;
bool bDrawOctree = false;
ImGuiIO imguiIO;
real32 octreeColor[] = { 0.0f, 1.0f, 0.0f, 1.0f };
real32 camSpeed;

bool bDirLightChanged = false;
real32 dirLightStrength = 0.8f;
glm::vec3 dirLightDirection(0.0f, -1.0f, 0.0f);
glm::vec3 dirLightAmbient(0.5f, 0.5f, 0.5f);
glm::vec3 dirLightDiffuse(0.75f, 0.75f, 0.75f);
glm::vec3 dirLightSpecular(1.0f, 1.0f, 1.0f);
bool bSceneHasInput = true;
// ===> MODELS
string industrialAreaAssetsPath = Utils::GetAssetsDirectory() + "packed/IndustrialArea/";
Model blenderFloor1("BlenderFloor1", BoundType::AABB, 64, STATIC_DRAW | IS_LANDSCAPE);
Model blenderFloor2("BlenderFloor2", BoundType::AABB, 64, STATIC_DRAW | IS_LANDSCAPE);
Model blenderFloor3("BlenderFloor3", BoundType::AABB, 64, STATIC_DRAW | IS_LANDSCAPE);
MaterialLand M_land_1(
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/blendMap1.png",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_ground_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_ground_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_ground_normal.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_grass_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_grass_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_grass_normal.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_asphalt_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_asphalt_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_asphalt_normal.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_rock_surface_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_rock_surface_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_rock_surface_normal.TGA",
	glm::vec2(0.125, 0.125));
MaterialLand M_land_2(
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/blendMap2.png",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_ground_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_ground_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_ground_normal.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_grass_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_grass_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_grass_normal.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_asphalt_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_asphalt_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_asphalt_normal.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_rock_surface_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_rock_surface_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_rock_surface_normal.TGA",
	glm::vec2(0.125, 0.125));
MaterialLand M_land_3(
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/blendMap3.png",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_ground_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_ground_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_ground_normal.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_grass_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_grass_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_grass_normal.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_asphalt_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_asphalt_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_asphalt_normal.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_rock_surface_basecolor.TGA",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_rock_surface_roughness.BMP",
	Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/landscape_textures/T_rock_surface_normal.TGA",
	glm::vec2(0.125, 0.125));
// FIXME: trie doesn't work with keys which have underscores in them.
Model SM_brick_building_01("BrickBuilding01", BoundType::AABB, 128, STATIC_DRAW);
Material M_brick_wall("M_brick_wall",
					  industrialAreaAssetsPath + "textures/industrial_base_textures/brick_wall/T_brick_wall_basecolor.TGA",
					  industrialAreaAssetsPath + "textures/industrial_base_textures/brick_wall/T_brick_wall_roughness.BMP",
					  industrialAreaAssetsPath + "textures/industrial_base_textures/brick_wall/T_brick_wall_normal.TGA");
Model SM_brick_building_04("BrickBuilding04", BoundType::AABB, 128, STATIC_DRAW);
Material M_wall_slab_A("M_wall_slab_A",
					 Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_wall_slab_BaseColor.TGA",
					 Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_wall_slab_Normal.TGA");
Model SM_brick_building_08("BrickBuilding08", BoundType::AABB, 128, STATIC_DRAW);
Material M_wall_fence("M_wall_fence",
					  Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_wall_fence_BaseColor.TGA",
					  Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_wall_fence_Normal.TGA");
Model SM_brick_building_07("BrickBuilding07", BoundType::AABB, 128, STATIC_DRAW);
Material M_brick_building("M_brick_building",
						  Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_brick_building_BaseColor.TGA",
						  Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_brick_building_Normal.TGA");
Model SM_brick_building_16("BrickBuilding16", BoundType::AABB, 128, STATIC_DRAW);
Model SM_brick_building_10("BrickBuilding10", BoundType::AABB, 128, STATIC_DRAW);
Model SM_brick_building_17("BrickBuilding17", BoundType::AABB, 128, STATIC_DRAW);
Model SM_stairs_01("Stairs01", BoundType::AABB, 128, STATIC_DRAW);
Material M_wooden_board_C("M_wooden_board_C",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/wooden_board/T_wooden_board_basecolor.TGA",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/wooden_board/T_wooden_board_roughness.BMP",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/wooden_board/T_wooden_board_normal.TGA");
Model SM_brick_building_13("BrickBuilding13", BoundType::AABB, 64, STATIC_DRAW);
Material M_wooden_floor_WPO("M_wooden_floor_WPO",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/wooden_floor/T_wooden_floor_BaseColor.TGA",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/wooden_floor/T_wooden_floor_Roughness.BMP",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/wooden_floor/T_wooden_floor_Normal.TGA");

Material M_asphalt_ground("M_asphalt_ground",
						  Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_asphalt_basecolor.TGA",
						  Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_asphalt_normal.TGA",
						  false,
						  glm::vec2(0.125f, 0.125f));
Model SM_coal("Coal", BoundType::AABB, 64, STATIC_DRAW);
Material M_coal("M_coal",
				Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_coal_BaseColor.TGA",
				Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/T_coal_Normal.TGA",
				false,
				glm::vec2(1/1.8, 1/1.8));
Model SM_shovel("Shovel", BoundType::AABB, 64, STATIC_DRAW);
Material M_shovel_bucket("M_shovel_bucket",
						 Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/interior_props_textures/T_shovel_bucket_BaseColor.TGA",
						 Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/interior_props_textures/T_shovel_bucket_Normal.TGA");
Model SM_stop_rails("StopRails", BoundType::AABB, 64, STATIC_DRAW);
Model SM_rails("Rails", BoundType::AABB, 64, STATIC_DRAW);
Material M_rails_cart("M_rails_cart",
					  Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/interior_props_textures/T_rails_cart_BaseColor.TGA",
					  Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/interior_props_textures/T_rails_cart_Normal.TGA");
Model SM_tree_01("Tree01", BoundType::AABB, 128, STATIC_DRAW);
//Model SM_tree_02("Tree02", BoundType::AABB, 128, STATIC_DRAW);
Material M_foliage("M_foliage",
				   Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/trees/T_foliage_BaseColor.TGA",
				   true);
Material M_bark("M_bark",
				Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/trees/T_bark_BaseColor.TGA",
				Utils::GetAssetsDirectory() + "packed/IndustrialArea/textures/trees/T_bark_Normal.TGA");
Model SM_gantry_crane_01("GantryCrane01", BoundType::AABB, 64, STATIC_DRAW);
Model SM_gantry_crane_02("GantryCrane02", BoundType::AABB, 64, STATIC_DRAW);
Model SM_gantry_crane_03("GantryCrane03", BoundType::AABB, 64, STATIC_DRAW);
Material M_gantry_crane("M_gantry_crane",
						industrialAreaAssetsPath + "textures/gantry_crane_textures/T_gantry_crane_BaseColor.TGA",
						industrialAreaAssetsPath + "textures/gantry_crane_textures/T_gantry_crane_Normal.TGA");
Model SM_bucket_01("Bucket01", BoundType::AABB, 64, STATIC_DRAW);
Model SM_bucket_02("Bucket02", BoundType::AABB, 64, STATIC_DRAW);
Model SM_wooden_box_01("WoodenBox01", BoundType::AABB, 64, STATIC_DRAW);
Model SM_wooden_box_04("WoodenBox04", BoundType::AABB, 64, STATIC_DRAW);
Model SM_container_01("Container01", BoundType::AABB, 64, STATIC_DRAW);
Model SM_container_02("Container02", BoundType::AABB, 64, STATIC_DRAW);
Model SM_container_door("ContainerDoor", BoundType::AABB, 64, STATIC_DRAW);
Material M_container_02_A("M_container_02_A",
						  industrialAreaAssetsPath + "textures/container_textures/container_02/T_container_02_BaseColor.TGA",
						  industrialAreaAssetsPath + "textures/container_textures/container_02/T_container_02_Normal.TGA");
Material M_corrugated_metal_B("M_corrugated_metal_B",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/corrugated_metal/T_corrugated_metal_basecolor.TGA",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/corrugated_metal/T_corrugated_metal_roughness.BMP",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/corrugated_metal/T_corrugated_metal_normal.TGA");
Material M_sheet_steel_A("M_sheet_steel_A",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/sheet_steel/T_sheet_steel_basecolor.TGA",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/sheet_steel/T_sheet_steel_roughness.BMP",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/sheet_steel/T_sheet_steel_normal.TGA");
Material M_floor_covering("M_floor_covering",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/floor_covering/T_floor_covering_basecolor.TGA",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/floor_covering/T_floor_covering_roughness.BMP",
						  industrialAreaAssetsPath + "textures/industrial_base_textures/floor_covering/T_floor_covering_normal.TGA");

Model blenderCube("blenderCube", BoundType::AABB, 64, STATIC_DRAW | NO_TEXTURE);
Model twoMatSphereFBX("TwoMatSphereFBX", BoundType::SPHERE, 64, STATIC_DRAW | NO_TEXTURE);
Model testMatTree("TestMatTree", BoundType::AABB, 64, STATIC_DRAW | NO_TEXTURE);
Model animalCrossingTaxi("AnimalCrossingTaxi", BoundType::AABB, 64, STATIC_DRAW);
Model animalCrossingTaxiFBX("AnimalCrossingTaxiFBX", BoundType::AABB, 64, STATIC_DRAW);
Material animalCrossingTaxiFBXMat("M_animal_crossing_taxi_fbx",
								  Utils::GetAssetsDirectory() + "packed/AnimalcrossingTaxiFBX/textures/daihatsu_bee_mat_BaseColor.jpeg",
								  Utils::GetAssetsDirectory() + "packed/AnimalCrossingTaxiFBX/textures/daihatsu_bee_mat_Normal.jpeg");
Model cyberpunkRoom("cyberpunkRoom", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedTree("StylizedTree", BoundType::AABB, 128, STATIC_DRAW | HAS_TRANSPARENCY);
Model stylizedBox("stylizedBox", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedRock2("stylizedRock2", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedAxe("stylizedAxe", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedFryingPan("stylizedFryingPan", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedGun1("stylizedGun1", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedGun2("stylizedGun2", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedHouse1("stylizedHouse1", BoundType::AABB, 64, STATIC_DRAW);
//Model stylizedHouse2("stylizedHouse2", BoundType::AABB, 64, STATIC_DRAW);
Model ww1Plane3("ww1Plane3", BoundType::AABB, 64, STATIC_DRAW);
//Model ww1Plane1("ww1Plane1", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedBrickWallPlane("stylizedBrickWallPlane", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedCactus("stylizedCactus", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedHackerTable("stylizedHackerTable", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedTrailer("stylizedTrailer", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedMech("stylizedMech", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedCharacter1("stylizedCharacter1", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedCharacter2("stylizedCharacter2", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedCharacter3("stylizedCharacter3", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedCharacter4("StylizedCharacter4", BoundType::AABB, 64, STATIC_DRAW | ALL_MESHES_SHARE_ONE_MAT);
Model stylizedCharacter5("stylizedCharacter5", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedCharacter7("stylizedCharacter7", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedCharacter10("stylizedCharacter10", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedCharacter12("stylizedCharacter12", BoundType::AABB, 64, STATIC_DRAW);
Model stylizedCharacter13("stylizedCharacter13", BoundType::AABB, 64, STATIC_DRAW);
Material stylizedCharacter4Material("M_stylized_character_4",
									Utils::GetAssetsDirectory() + "packed/StylizedCharacter4/textures/Character_Albedo.tga",
								    Utils::GetAssetsDirectory() + "packed/StylizedCharacter4/textures/Character_Normals.png");
Sphere sphere(10);
// ===< MODELS
//std::vector<Sphere> projectiles;
void shoot_projectile();


int main()
{
	// ===> INIT
	std::cout << "Hello, SimToyEngine!" << std::endl;
	scene = Scene(3, 3, "SimToyEngine", 1680, 1050);
	if (!scene.Init())
	{
		std::cout << "Failed to init scene" << std::endl;
		glfwTerminate();
		return -1;
	}
	// ===< INIT

	// ===> CAMERA
	scene.AddCamera(&camera);
	scene.SetActiveCamera(0);
	// ===< CAMERA

	// ===> CREATE SHADERS
	Shader lampShader("LampShader",
					  string(Utils::GetAssetsDirectory() + "shaders/phong.vert"),
					  string(Utils::GetAssetsDirectory() + "shaders/lamp.frag"));
	Shader boxShader("BoxShader",
					 string(Utils::GetAssetsDirectory() + "shaders/box.vert"),
					 string(Utils::GetAssetsDirectory() + "shaders/box.frag"));
	Shader skyboxShader("SkyboxShader",
						string(Utils::GetAssetsDirectory() + "shaders/skybox.vert"),
						string(Utils::GetAssetsDirectory() + "shaders/skybox.frag"));
	Shader phongShader("PhongShader",
					   string(Utils::GetAssetsDirectory() + "shaders/phong.vert"),
					   string(Utils::GetAssetsDirectory() + "shaders/phong.frag"));
	Shader landscapeShader("LandscapeShader",
						   string(Utils::GetAssetsDirectory() + "shaders/landscape.vert"),
						   string(Utils::GetAssetsDirectory() + "shaders/landscape.frag"));
	// ===< CREATE SHADERS

	// ===> SKYBOX
	Cubemap skybox;
	skybox.Init();
	skybox.LoadTextures(string(Utils::GetAssetsDirectory() + "textures/skybox_space"));
	// ===< SKYBOX


	// ===> Init Models
	Lamp lamp(4);
	scene.RegisterModel(&lamp);
	load_models();
	load_enviroment_models();
	load_foliages_models();
	load_rails_crane_models();
	load_containers_models();
	// ===< Init Models

	Box octreeDrawable;
	octreeDrawable.Init();
	scene.LoadModels();
	// ===< Spawn Models Instances
	spawn_models_instances();
	spawn_enviroment_models_instances();
	spawn_foliages_models_instances();
	spawn_rails_crane_models_instances();
	spawn_containers_models_instances();

	// ===> LIGHTS
	// directional light
	DirLight dirLight = {
		//dirLightStrength,              // strength
		dirLightDirection,
		dirLightAmbient,
		dirLightDiffuse,
		dirLightSpecular
	};
	scene.SetDirLight(&dirLight);

	// point lights
	real32 pointLightStr = 1.2f;
	// point lights positions
	glm::vec3 pointLightPositions[] = {
			glm::vec3(0.0f,  3.0f,  0.0f),
			glm::vec3(16.0f,  3.0f,  0.0f),
			glm::vec3(0.0f,  3.0f,  16.0f),
			glm::vec3(16.0f,  3.0f,  16.0f),
	};
	// point lights colors
	glm::vec3 pointLightAmbient = glm::vec3(0.05f, 0.05f, 0.05f);
	glm::vec3 pointLightDiffuse = glm::vec3(0.8f, 0.8f, 0.8f);
	glm::vec3 pointLightSpecular = glm::vec3(1.0f);
	// point lights coefficients
	real32 constant = 1.0f;
	real32 linear = 0.09f;
	real32 quadratic = 0.032f;

	/*
	PointLight pointLights[4];
	for (uint32 i = 0; i < 4; i++)
	{
		pointLights[i] = {
			//pointLightStr,
			pointLightPositions[i],
			constant, linear, quadratic,
			pointLightAmbient, pointLightDiffuse, pointLightSpecular
		};
		scene.SpawnInstance(lamp.GetModelID(), pointLightPositions[i], glm::vec3(0.25f));
		// FIXME:
		scene.AddPointLight(&pointLights[i]);
	}
	*/


	// spot light
	SpotLight spotLight = {
		camera.GetPos(),
		camera.GetFront(),
		glm::cos(glm::radians(12.5f)), // cutoff
		glm::cos(glm::radians(20.0f)), // outer cutoff
		1.0f, 0.0014f, 0.000007f,
		glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), // ambient
		glm::vec4(1.0f), // diffuse
		glm::vec4(1.0f), // specular
	};
	//scene.AddSpotLight(&spotLight);
	//scene.activeSpotLights = 1; // 0b0000001
	// ===< LIGHTINGS



	// instantiate instances
	scene.InitInstances();
	// init imgui
	init_imgui();
	// ===> RENDER LOOP
	scene.PrepareOctree(octreeDrawable);
	scene.PrepareShaders({ phongShader, landscapeShader });
	while (!scene.ShouldClose())
	{
		if (bDirLightChanged)
		{
			//dirLight.strength = dirLightStrength;
			dirLight.direction = dirLightDirection;
			dirLight.ambient = dirLightAmbient;
			dirLight.diffuse = dirLightDiffuse;
			dirLight.specular = dirLightSpecular;
			scene.UpdateShadersUBOs();
		}
		// ===> CALCULATE DT
		real64 currentTime = glfwGetTime();
		dt = currentTime - lastFrame;
		lastFrame = currentTime;
		// ===< CALCULATE DT
		scene.BeginDraw();
		imgui_begindraw();
		processInput(dt);
		if (!imguiIO.WantCaptureMouse)
		{
		}
		imguiIO.MousePos = ImVec2(Mouse::GetMouseX(), Mouse::GetMouseY());

		// ===> SKYBOX
		skybox.Render(skyboxShader, &scene);
		// ===< SKYBOX

		// remove projectile if too fare
		for (int32 i = 0; i < sphere.GetNumInstances(); i++)
		{
			if (glm::length(camera.GetPos() - sphere.GetInstance(i)->GetPos()) > 100.0f)
			{
				// mark for deletion
				scene.MarkForDeletion(sphere.GetInstance(i)->GetInstanceID());
			}
		}

		// TODO: make it automatic!!!!!!!!!!
		scene.UpdateViewProjectionMatrices(phongShader);
		scene.UpdateViewProjectionMatrices(landscapeShader);
		// render projectiles
		if (sphere.GetNumInstances() > 0)
		{
			scene.RenderInstances(sphere.GetModelID(), phongShader, dt);
		}
		// render landscape
		scene.RenderInstances(blenderFloor1.GetModelID(), landscapeShader, dt);
		scene.RenderInstances(blenderFloor2.GetModelID(), landscapeShader, dt);
		scene.RenderInstances(blenderFloor3.GetModelID(), landscapeShader, dt);
		// render all other models
		draw_models_instances(phongShader);


		// render lamps
		scene.UpdateViewProjectionMatrices(lampShader);
		scene.RenderInstances(lamp.GetModelID(), lampShader, dt);

		// render drawables
		scene.UpdateViewProjectionMatrices(boxShader);
		if(bDrawOctree)
			octreeDrawable.Render(boxShader);

		// render imgui
		imgui_drawdebugpanel();
		imgui_enddraw();

		// ===< UPDATEANDRENDER
		scene.EndDraw(octreeDrawable);
		scene.DeleteDeadInstances();
	}

	// ===> CLEANUP
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	skybox.Cleanup();
	scene.Cleanup();
	// ===< CLEANUP
	return 0;
}


void
processInput(real64 dt)
{
	if (Keyboard::KeyWentDown(GLFW_KEY_G))
	{
		bSceneHasInput = !bSceneHasInput;
	}
	if (!bSceneHasInput)
		return;
	oldCamera = camera;
	scene.ProcessInput(dt);
	// update flashlight
	if (Bitwise::IsBitOnByIndex(&scene.activeSpotLights_, 0))
	{
		//scene.spotLights_[0]->position = scene.GetActiveCamera()->GetPos();
		//scene.spotLights_[0]->direction = scene.GetActiveCamera()->GetFront();
		scene.GetSpotLight(0)->position = scene.GetActiveCamera()->GetPos();
		scene.GetSpotLight(0)->direction = scene.GetActiveCamera()->GetFront();
		if(oldCamera.GetPos() != scene.GetActiveCamera()->GetPos() ||
		   oldCamera.GetFront() != scene.GetActiveCamera()->GetFront())
		{
			std::cout << "updatein" << std::endl;
			scene.UpdateShadersUBOs();
		}
	}

	if (Keyboard::IsKeyDown(GLFW_KEY_ESCAPE))
	{
		scene.SetShouldClose(true);
	}

	if(Keyboard::KeyWentDown(GLFW_KEY_SPACE))
	{
		shoot_projectile();
	}

	if (Keyboard::KeyWentDown(GLFW_KEY_F))
	{
		Bitwise::ToggleBitByIndex(&scene.activeSpotLights_, 0);
		scene.UpdateShadersUBOs();
	}

	for (int32 i = 0; i < 4; i++)
	{
		if (Keyboard::KeyWentDown(GLFW_KEY_1 + i))
		{
			Bitwise::ToggleBitByIndex(&scene.activePointLights_, i);
			scene.UpdateShadersUBOs();
		}
	}
}


void shoot_projectile()
{
	RigidBody* newProjectile	= scene.SpawnInstance(sphere.GetModelID(),
													  camera.GetPos(),
													  glm::vec3(0.2f));
	if(newProjectile)
	{
		//
		newProjectile->TransferEnergy(100.0f, camera.GetFront());
		newProjectile->ApplyAcceleration(Enviroment::Gravity);
	}
}

void imgui_drawdebugpanel()
{
	ImGui::Begin("DebugWindow");
	ImGui::Text("DirLight:");
	bDirLightChanged = ImGui::SliderFloat("strength:", &dirLightStrength, 0.0f, 3.0f);
	bDirLightChanged = ImGui::InputFloat3("direction: ", &dirLightDirection[0]);
	ImGui::Text(("MousePos: " + Utils::ToString(imguiIO.MousePos.x) +
				 "," +
				 Utils::ToString(imguiIO.MousePos.y)).c_str());
	ImGui::Text(("CameraPos: " + Utils::ToString(scene.GetActiveCamera()->GetPos().x) +
				 "," + Utils::ToString(scene.GetActiveCamera()->GetPos().y) +
				 "," + Utils::ToString(scene.GetActiveCamera()->GetPos().z)).c_str());

	ImGui::End();
}
void imgui_enddraw()
{
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void imgui_begindraw()
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}
void init_imgui()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	imguiIO = ImGui::GetIO(); 
	(void)imguiIO;
	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForOpenGL(scene.GetWindow(), true);
	ImGui_ImplOpenGL3_Init("#version 330");
}

void load_models()
{
	// ===> Landscape
	blenderFloor1.LoadFromFile(Utils::GetAssetsDirectory() + "models/blenderFloor32x32.fbx");
	blenderFloor2.LoadFromFile(Utils::GetAssetsDirectory() + "models/blenderFloor32x32.fbx");
	blenderFloor3.LoadFromFile(Utils::GetAssetsDirectory() + "models/blenderFloor32x32.fbx");
	//M_asphalt_ground.LoadTextures();
	M_land_1.LoadTextures();
	M_land_2.LoadTextures();
	M_land_3.LoadTextures();
	blenderFloor1.AssignMaterial(&M_land_1);
	blenderFloor2.AssignMaterial(&M_land_2);
	blenderFloor3.AssignMaterial(&M_land_3);
	// ===< Landscape

	blenderCube.LoadFromFile(Utils::GetAssetsDirectory() + "models/cubeAnchored.glb");
	twoMatSphereFBX.LoadFromFile(Utils::GetAssetsDirectory() + "models/blenderTwoMatSphere.fbx");
	testMatTree.LoadFromFile(Utils::GetAssetsDirectory() + "models/Outdoor_prop_tree_01.fbx");
	animalCrossingTaxi.LoadFromFile(Utils::GetAssetsDirectory() + "packed/AnimalCrossingTaxi/scene.gltf");
	// ===> NOTE FBX MODEL
	animalCrossingTaxiFBX.LoadFromFile(Utils::GetAssetsDirectory() + "packed/AnimalCrossingTaxiFBX/source/daihatsu_bee_asset.fbx");
	animalCrossingTaxiFBXMat.LoadTextures();
	animalCrossingTaxiFBX.AssignMaterial(&animalCrossingTaxiFBXMat);
	// ===< NOTE FBX MODEL
	// ===> NOTE: OBJ MODEL
	stylizedCharacter4.LoadFromFile(Utils::GetAssetsDirectory() +
									"packed/StylizedCharacter4/char.obj");
	stylizedCharacter4Material.LoadTextures();
	stylizedCharacter4.AssignMaterial(&stylizedCharacter4Material);

	// ===< NOTE: OBJ MODEL
	//cyberpunkRoom.LoadFromFile(Utils::GetAssetsDirectory() + "packed/CyberpunkRoom/gltf/scene.gltf");
	//stylizedBox.LoadFromFile(Utils::GetAssetsDirectory() + "packed/StylizedBox/gltf/scene.gltf");
	//stylizedRock2.LoadFromFile(Utils::GetAssetsDirectory() + "packed/StylizedRock2/gltf/scene.gltf");
	//stylizedAxe.LoadFromFile(Utils::GetAssetsDirectory() + "packed/StylizedAxe/gltf/scene.gltf");
	/*
	ww1Plane1.LoadFromFile(Utils::GetAssetsDirectory() +
						   "packed/WW1Plane1/gltf/scene.gltf");
	ww1Plane3.LoadFromFile(Utils::GetAssetsDirectory() +
						   "packed/WW1Plane3/gltf/scene.gltf");
	stylizedMech.LoadFromFile(Utils::GetAssetsDirectory() +
									"packed/StylizedMech/gltf/scene.gltf");
	stylizedCharacter1.LoadFromFile(Utils::GetAssetsDirectory() +
									"packed/StylizedCharacter1/gltf/scene.gltf");
	stylizedCharacter3.LoadFromFile(Utils::GetAssetsDirectory() +
									"packed/StylizedCharacter3/gltf/scene.gltf");
	stylizedCharacter5.LoadFromFile(Utils::GetAssetsDirectory() +
									"packed/StylizedCharacter5/gltf/scene.gltf");
	stylizedCharacter7.LoadFromFile(Utils::GetAssetsDirectory() +
									"packed/StylizedCharacter7/gltf/scene.gltf");
	stylizedCharacter10.LoadFromFile(Utils::GetAssetsDirectory() +
									 "packed/StylizedCharacter10/gltf/scene.gltf");
	stylizedCharacter12.LoadFromFile(Utils::GetAssetsDirectory() +
									 "packed/StylizedCharacter12/gltf/scene.gltf");
	stylizedCharacter13.LoadFromFile(Utils::GetAssetsDirectory() +
									 "packed/StylizedCharacter13/gltf/scene.gltf");
	stylizedFryingPan.LoadFromFile(Utils::GetAssetsDirectory() +
								   "packed/StylizedFryingPan/gltf/scene.gltf");
	stylizedGun1.LoadFromFile(Utils::GetAssetsDirectory() +
							  "packed/StylizedGun1/gltf/scene.gltf");
	stylizedGun2.LoadFromFile(Utils::GetAssetsDirectory() +
							  "packed/StylizedGun2/gltf/scene.gltf");
	stylizedHouse1.LoadFromFile(Utils::GetAssetsDirectory() +
								"packed/StylizedHouse1/gltf/scene.gltf");
	stylizedHouse2.LoadFromFile(Utils::GetAssetsDirectory() +
								"packed/StylizedHouse2/gltf/scene.gltf");
	stylizedBrickWallPlane.LoadFromFile(Utils::GetAssetsDirectory() +
										"packed/StylizedBrickWallPlane/gltf/scene.gltf");
	stylizedCactus.LoadFromFile(Utils::GetAssetsDirectory() +
								"packed/StylizedCactus/gltf/scene.gltf");
	stylizedHackerTable.LoadFromFile(Utils::GetAssetsDirectory() +
									 "packed/StylizedHackerTable/gltf/scene.gltf");
	stylizedTrailer.LoadFromFile(Utils::GetAssetsDirectory() +
							     "packed/StylizedTrailer/gltf/scene.gltf");
	stylizedCharacter2.LoadFromFile(Utils::GetAssetsDirectory() +
									"packed/StylizedCharacter2/gltf/scene.gltf");
									*/
	scene.RegisterModel(&blenderFloor1);
	scene.RegisterModel(&blenderFloor2);
	scene.RegisterModel(&blenderFloor3);
	scene.RegisterModel(&sphere);
	scene.RegisterModel(&blenderCube);
	scene.RegisterModel(&twoMatSphereFBX);
	scene.RegisterModel(&testMatTree);
	scene.RegisterModel(&animalCrossingTaxi);
	scene.RegisterModel(&animalCrossingTaxiFBX);
	scene.RegisterModel(&cyberpunkRoom);
	scene.RegisterModel(&stylizedAxe);
	scene.RegisterModel(&stylizedBox);
	scene.RegisterModel(&stylizedRock2);
	scene.RegisterModel(&stylizedFryingPan);
	scene.RegisterModel(&stylizedGun1);
	scene.RegisterModel(&stylizedGun2);
	//scene.RegisterModel(&stylizedHouse2);
	scene.RegisterModel(&stylizedHouse1);
	scene.RegisterModel(&ww1Plane3);
	//scene.RegisterModel(&ww1Plane1);
	scene.RegisterModel(&stylizedBrickWallPlane);
	scene.RegisterModel(&stylizedCactus);
	scene.RegisterModel(&stylizedHackerTable);
	scene.RegisterModel(&stylizedTrailer);
	scene.RegisterModel(&stylizedMech);
	scene.RegisterModel(&stylizedCharacter1);
	scene.RegisterModel(&stylizedCharacter2);
	scene.RegisterModel(&stylizedCharacter3);
	scene.RegisterModel(&stylizedCharacter4);
	scene.RegisterModel(&stylizedCharacter5);
	scene.RegisterModel(&stylizedCharacter7);
	scene.RegisterModel(&stylizedCharacter10);
	scene.RegisterModel(&stylizedCharacter12);
	scene.RegisterModel(&stylizedCharacter13);
}

void spawn_models_instances()
{
	scene.SpawnInstance("BlenderFloor1", glm::vec3(-8.0f, 0.0f, -8.0f), 
						glm::vec3(1.0f),
						glm::vec3(-90, 0, 0));
	scene.SpawnInstance("BlenderFloor2", glm::vec3(24.0f, 0.0f, -8.0f), 
						glm::vec3(1.0f),
						glm::vec3(-90, 0, 0));
	scene.SpawnInstance("BlenderFloor3", glm::vec3(-8.0f, 0.0f, 24.0f), 
						glm::vec3(1.0f),
						glm::vec3(-90, 0, 0));
	scene.SpawnInstance("BlenderFloor3", glm::vec3(24.0f, 0.0f, 24.0f), 
						glm::vec3(1.0f),
						glm::vec3(-90, 0, 0));
	scene.SpawnInstance(blenderCube.GetModelID(),
						glm::vec3(20.0f, 0.0f, 20.0f), // pos
						glm::vec3(1.0f));
	scene.SpawnInstance(twoMatSphereFBX.GetModelID(),
						glm::vec3(23.0f, 0.0f, 20.0f), // pos
						glm::vec3(1.0f));
	scene.SpawnInstance(testMatTree.GetModelID(),
						glm::vec3(25.0f, 0.0f, 20.0f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0));
	scene.SpawnInstance(animalCrossingTaxi.GetModelID(),
						glm::vec3(3.0f, 0.22f, 3.0f), // pos
						glm::vec3(0.02f));
	scene.SpawnInstance(animalCrossingTaxiFBX.GetModelID(),
						glm::vec3(3.0f, 0.2f, 5.0f), // pos
						glm::vec3(0.02f)); // size
	scene.SpawnInstance(stylizedBox.GetModelID(),
						glm::vec3(3.0f, 0.2f, 2.0f), // pos
						glm::vec3(0.2f));
	scene.SpawnInstance(stylizedRock2.GetModelID(),
						glm::vec3(5.0f, 0.25f, 2.0f), // pos
						glm::vec3(1.0f));
	scene.SpawnInstance(cyberpunkRoom.GetModelID(),
						glm::vec3(8.0f, 0.12f, 2.0f), // pos
						glm::vec3(0.5f),// size
						glm::vec3(90, 0, 0)); //rotation
	scene.SpawnInstance(stylizedAxe.GetModelID(),
						glm::vec3(6.0f, 0.0f, 2.0f), // pos
						glm::vec3(0.01f), // size
						glm::vec3(-90, 0, 0)); // rotation
	scene.SpawnInstance(stylizedFryingPan.GetModelID(),
						glm::vec3(7.0f, 1.0f, 2.0f), // pos
						glm::vec3(0.1));
	scene.SpawnInstance(stylizedGun1.GetModelID(),
						glm::vec3(8.0f, 1.0f, 2.0f), // pos
						glm::vec3(0.01));
	scene.SpawnInstance(stylizedGun2.GetModelID(),
						glm::vec3(9.0f, 1.0f, 2.0f), // pos
						glm::vec3(0.0005));
	scene.SpawnInstance(stylizedHouse1.GetModelID(),
						glm::vec3(2.0f, 1.0f, 9.0f), // pos
						glm::vec3(1.0f),
						glm::vec3(-90, 0, 0));
	/*
	scene.SpawnInstance(stylizedHouse2.GetModelID(),
						glm::vec3(11.0f, 1.0f, 2.0f), // pos
						glm::vec3(1.0f));
						*/
	scene.SpawnInstance(ww1Plane3.GetModelID(),
						glm::vec3(13.0f, 0.1f, 3.0f), // pos
						glm::vec3(0.02));
	/*
	scene.SpawnInstance(ww1Plane1.GetModelID(),
						glm::vec3(13.0f, 0.3f, 3.0f), // pos
						glm::vec3(0.1));
	*/
	scene.SpawnInstance(stylizedBrickWallPlane.GetModelID(),
						glm::vec3(1.0f, 0.5f, 6.0f), // pos
						glm::vec3(1.0f),
						glm::vec3(90, 0, 0));
	scene.SpawnInstance(stylizedCactus.GetModelID(),
						glm::vec3(5.0f, 0.8f, 6.0f), // pos
						glm::vec3(1.0f));
	scene.SpawnInstance(stylizedHackerTable.GetModelID(),
						glm::vec3(7.0f, 0.1f, 6.0f), // pos
						glm::vec3(0.005f),
						glm::vec3(0, 180, 0));
	scene.SpawnInstance(stylizedTrailer.GetModelID(),
						glm::vec3(10.0f, 0.0f, 6.0f), // pos
						glm::vec3(0.01),
						glm::vec3(-90, 0, 0));
	scene.SpawnInstance(stylizedMech.GetModelID(),
						glm::vec3(10.0f, 0.0f, 1.0f), // pos
						glm::vec3(2.0f),
						glm::vec3(-90, 0, 0));
	scene.SpawnInstance(stylizedCharacter1.GetModelID(),
						glm::vec3(13.0f, 0.0f, 6.0f), // pos
						glm::vec3(0.01));
	scene.SpawnInstance(stylizedCharacter2.GetModelID(),
						glm::vec3(14.0f, 0.0f, 6.0f), // pos
						glm::vec3(0.003));
	scene.SpawnInstance(stylizedCharacter3.GetModelID(),
						glm::vec3(4.0f, 0.0f, 1.0f), // pos
						glm::vec3(0.003));
	scene.SpawnInstance(stylizedCharacter4.GetModelID(),
						glm::vec3(12.5f, 1.6f, 10.0f), // pos
						glm::vec3(0.003));
	scene.SpawnInstance(stylizedCharacter5.GetModelID(),
						glm::vec3(4.5f, 0.0f, 1.0f), // pos
						glm::vec3(0.001));
	scene.SpawnInstance(stylizedCharacter7.GetModelID(),
						glm::vec3(6.0f, 0.0f, 1.0f), // pos
						glm::vec3(0.3));
	scene.SpawnInstance(stylizedCharacter10.GetModelID(),
						glm::vec3(6.5f, 0.0f, 1.0f), // pos
						glm::vec3(0.01));
	scene.SpawnInstance(stylizedCharacter12.GetModelID(),
						glm::vec3(7.5f, 0.0f, 1.0f), // pos
						glm::vec3(1.0f));
	scene.SpawnInstance(stylizedCharacter13.GetModelID(),
						glm::vec3(7.0f, 0.01f, 1.0f), // pos
						glm::vec3(0.005),
						glm::vec3(-90, 0, 0));
}
void draw_models_instances(Shader phongShader)
{
		scene.RenderInstances(blenderCube.GetModelID(), phongShader, dt);
		scene.RenderInstances(twoMatSphereFBX.GetModelID(), phongShader, dt);
		scene.RenderInstances(testMatTree.GetModelID(), phongShader, dt);
		//scene.RenderInstances(cyberpunkRoom.GetModelID(), phongShader, dt);
		//scene.RenderInstances(animalCrossingTaxi.GetModelID(), phongShader, dt);
		//scene.RenderInstances(animalCrossingTaxiFBX.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_brick_building_01.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_brick_building_04.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_brick_building_07.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_brick_building_08.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_brick_building_10.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_brick_building_13.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_brick_building_17.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_brick_building_16.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_stairs_01.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_tree_01.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_coal.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_shovel.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_stop_rails.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_rails.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_gantry_crane_01.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_gantry_crane_02.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_gantry_crane_03.GetModelID(), phongShader, dt);
		scene.RenderInstances(SM_container_02.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedAxe.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedBox.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedRock2.GetModelID(), phongShader, dt);
		//scene.RenderInstances(SM_tree_02.GetModelID(), phongShader, dt);
		//scene.RenderInstances(ww1Plane1.GetModelID(), phongShader, dt);
		scene.RenderInstances(ww1Plane3.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedMech.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedCharacter1.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedCharacter4.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedCharacter3.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedCharacter5.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedCharacter7.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedCharacter10.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedCharacter12.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedCharacter13.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedFryingPan.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedGun1.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedGun2.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedBrickWallPlane.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedCactus.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedHackerTable.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedTrailer.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedCharacter2.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedHouse1.GetModelID(), phongShader, dt);
		//scene.RenderInstances(stylizedHouse2.GetModelID(), phongShader, dt);
		//scene.RenderInstances(animalCrossingTaxiFBX.GetModelID(), phongShader, dt);
		scene.RenderInstances(stylizedTree.GetModelID(), phongShader, dt);
}

void load_enviroment_models()
{
	SM_brick_building_01.LoadFromFile(industrialAreaAssetsPath + "meshes/brick_building/SM_brick_building_01.fbx");
	M_brick_wall.LoadTextures();
	SM_brick_building_01.AssignMaterial(&M_brick_wall);
	SM_brick_building_01.AddNewMatSlot("M_brick_wall");
	SM_brick_building_01.AssignMaterial(&M_brick_wall);

	SM_brick_building_04.LoadFromFile(industrialAreaAssetsPath + "meshes/brick_building/SM_brick_building_04.fbx");
	M_wall_slab_A.LoadTextures();
	SM_brick_building_04.AssignMaterial(&M_wall_slab_A);

	SM_brick_building_16.LoadFromFile(industrialAreaAssetsPath + "meshes/brick_building/SM_brick_building_16.fbx");
	SM_brick_building_16.AssignMaterial(&M_wall_slab_A);

	SM_brick_building_07.LoadFromFile(industrialAreaAssetsPath + "meshes/brick_building/SM_brick_building_07.fbx");
	M_brick_building.LoadTextures();
	SM_brick_building_07.AssignMaterial(&M_brick_building);

	SM_brick_building_08.LoadFromFile(industrialAreaAssetsPath + "meshes/brick_building/SM_brick_building_08.fbx");
	M_wall_fence.LoadTextures();
	SM_brick_building_08.AssignMaterial(&M_wall_fence);

	SM_brick_building_10.LoadFromFile(industrialAreaAssetsPath + "meshes/brick_building/SM_brick_building_10.fbx");
	M_brick_wall.LoadTextures();
	SM_brick_building_10.AssignMaterial(&M_brick_wall);
	SM_brick_building_10.AssignMaterial(&M_brick_wall);
	SM_brick_building_10.AddNewMatSlot("M_wooden_board_C");
	SM_brick_building_10.AssignMaterial(&M_wooden_board_C);

	SM_brick_building_13.LoadFromFile(industrialAreaAssetsPath + "meshes/brick_building/SM_brick_building_13.fbx");
	M_wooden_floor_WPO.LoadTextures();
	SM_brick_building_13.AssignMaterial(&M_wooden_floor_WPO);
	SM_brick_building_13.AssignMaterial(&M_wooden_floor_WPO);

	SM_brick_building_17.LoadFromFile(industrialAreaAssetsPath + "meshes/brick_building/SM_brick_building_17.fbx");
	SM_brick_building_17.AssignMaterial(&M_brick_wall);
	SM_brick_building_17.AssignMaterial(&M_brick_wall);
	SM_brick_building_17.AddNewMatSlot("M_wooden_board_C");
	SM_brick_building_17.AssignMaterial(&M_wooden_board_C);

	SM_stairs_01.LoadFromFile(industrialAreaAssetsPath + "meshes/brick_building/SM_stairs_01.fbx");
	M_wooden_board_C.LoadTextures();
	SM_stairs_01.AssignMaterial(&M_wooden_board_C);

	SM_coal.LoadFromFile(Utils::GetAssetsDirectory() + "packed/IndustrialArea/meshes/interior_props/SM_coal.fbx");
	M_coal.LoadTextures();
	SM_coal.AssignMaterial(&M_coal);

	SM_shovel.LoadFromFile(Utils::GetAssetsDirectory() + "packed/IndustrialArea/meshes/interior_props/SM_shovel.fbx");
	M_shovel_bucket.LoadTextures();
	SM_shovel.AssignMaterial(&M_shovel_bucket);



	scene.RegisterModel(&SM_brick_building_01);
	scene.RegisterModel(&SM_brick_building_04);
	scene.RegisterModel(&SM_brick_building_07);
	scene.RegisterModel(&SM_brick_building_08);
	scene.RegisterModel(&SM_brick_building_10);
	scene.RegisterModel(&SM_brick_building_16);
	scene.RegisterModel(&SM_brick_building_13);
	scene.RegisterModel(&SM_brick_building_17);
	scene.RegisterModel(&SM_stairs_01);
	scene.RegisterModel(&SM_coal);
	scene.RegisterModel(&SM_shovel);
}
void load_rails_crane_models()
{
	SM_stop_rails.LoadFromFile(Utils::GetAssetsDirectory() + "packed/IndustrialArea/meshes/interior_props/SM_stop_rails.fbx");
	SM_rails.LoadFromFile(Utils::GetAssetsDirectory() + "packed/IndustrialArea/meshes/interior_props/SM_rails.fbx");
	M_rails_cart.LoadTextures();
	SM_stop_rails.AssignMaterial(&M_rails_cart);
	SM_rails.AssignMaterial(&M_rails_cart);

	SM_gantry_crane_01.LoadFromFile(Utils::GetAssetsDirectory() + "packed/IndustrialArea/meshes/gantry_crane/SM_gantry_crane_01.fbx");
	SM_gantry_crane_02.LoadFromFile(Utils::GetAssetsDirectory() + "packed/IndustrialArea/meshes/gantry_crane/SM_gantry_crane_02.fbx");
	SM_gantry_crane_03.LoadFromFile(Utils::GetAssetsDirectory() + "packed/IndustrialArea/meshes/gantry_crane/SM_gantry_crane_03.fbx");
	M_gantry_crane.LoadTextures();
	SM_gantry_crane_01.AssignMaterial(&M_gantry_crane);
	SM_gantry_crane_02.AssignMaterial(&M_gantry_crane);
	SM_gantry_crane_03.AssignMaterial(&M_gantry_crane);
	SM_gantry_crane_03.AssignMaterial(&M_gantry_crane);

	scene.RegisterModel(&SM_stop_rails);
	scene.RegisterModel(&SM_rails);
	scene.RegisterModel(&SM_gantry_crane_01);
	scene.RegisterModel(&SM_gantry_crane_02);
	scene.RegisterModel(&SM_gantry_crane_03);
}
void load_foliages_models()
{
	SM_tree_01.LoadFromFile(Utils::GetAssetsDirectory() + "packed/IndustrialArea/meshes/trees/SM_tree_01.FBX");
	M_bark.LoadTextures();
	M_foliage.LoadTextures();
	SM_tree_01.AssignMaterial(&M_bark);
	SM_tree_01.AssignMaterial(&M_foliage);
	stylizedTree.LoadFromFile(Utils::GetAssetsDirectory() + "packed/StylizedTree/gltf/scene.gltf");

	/*
	// FIXME: order of assinging multiple materials
	SM_tree_02.LoadFromFile(Utils::GetAssetsDirectory() + "packed/IndustrialArea/meshes/trees/SM_tree_02.FBX");
	SM_tree_02.AssignMaterial(&M_foliage);
	SM_tree_02.AssignMaterial(&M_bark);
	*/

	scene.RegisterModel(&stylizedTree);
	scene.RegisterModel(&SM_tree_01);
	//scene.RegisterModel(&SM_tree_02);
}

void spawn_foliages_models_instances()
{
	/*
	scene.SpawnInstance(SM_tree_02.GetModelID(),
						glm::vec3(8.0f, 0.0f, 12.0f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // rotation
	*/
	// stylized tree
	scene.SpawnInstance(stylizedTree.GetModelID(),
						glm::vec3(5.0f, 3.7f, 18.0f), // pos
						glm::vec3(5.0f),
						glm::vec3(0, 0, 0)); // rotation
	uint32 seed = std::chrono::steady_clock::now().time_since_epoch().count();
	std::default_random_engine randomE(seed);
	std::uniform_real_distribution<real32> distrX(0, 60);
	std::uniform_real_distribution<real32> distrZ(0, 7.8);
	std::uniform_real_distribution<real32> distrS(0.008, 0.015);
	for(int32 i = 0; i < 16; i++)
	{
		real32 randomX = distrX(randomE) - 8.0f;
		real32 randomZ = distrZ(randomE) - 8.0f;
		real32 randomScale = distrS(randomE);
		std::cout << randomScale << std::endl;
		// sm_tree_01
		scene.SpawnInstance(SM_tree_01.GetModelID(),
							glm::vec3(randomX, 0.0f, randomZ), // pos
							glm::vec3(randomScale),
							glm::vec3(-90, 0, 0)); // rotation
	}
}

void spawn_enviroment_models_instances()
{
	// coal
	scene.SpawnInstance(SM_coal.GetModelID(),
						glm::vec3(12.0f, -0.1f, 7.0f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // rotation
	scene.SpawnInstance(SM_coal.GetModelID(),
						glm::vec3(15.0f, -0.1f, 7.0f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // rotation
	// shovel
	scene.SpawnInstance(SM_shovel.GetModelID(),
						glm::vec3(12.3f, 1.0f, 8.0f), // pos
						glm::vec3(0.01f),
						glm::vec3(190, 180, 10)); // rotation
	scene.SpawnInstance(SM_shovel.GetModelID(),
						glm::vec3(11.3f, 1.0f, 7.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(200, 170, -10)); // rotation
	// ===> NORTH WALL
	spawn_walls_westeast(glm::vec3(0.0f));
	spawn_walls_westeast(glm::vec3(6.4f, 0.0f, 0.0f));
	spawn_walls_westeast(glm::vec3(12.8f, 0.0f, 0.0f));
	spawn_walls_westeast(glm::vec3(19.2f, 0.0f, 0.0f));
	spawn_walls_westeast(glm::vec3(25.6f, 0.0f, 0.0f));
	spawn_walls_westeast(glm::vec3(32.0f, 0.0f, 0.0f));
	spawn_walls_westeast(glm::vec3(38.4f, 0.0f, 0.0f));
	spawn_walls_westeast(glm::vec3(44.8f, 0.0f, 0.0f));
	// ===> EAST WALL
	spawn_walls_northsouth(glm::vec3(50.8f, 0.0f, 0.0f));
	spawn_walls_northsouth(glm::vec3(50.8f, 0.0f, 6.4f));
	spawn_walls_northsouth(glm::vec3(50.8f, 0.0f, 12.8f));
	spawn_walls_northsouth(glm::vec3(50.8f, 0.0f, 19.2f));
	spawn_walls_northsouth(glm::vec3(50.8f, 0.0f, 25.6f));
	spawn_walls_northsouth(glm::vec3(50.8f, 0.0f, 32.0));
	spawn_walls_northsouth(glm::vec3(50.8f, 0.0f, 38.4f));
	spawn_walls_northsouth(glm::vec3(50.8f, 0.0f, 44.8f));
	// ===> WEST
	spawn_walls_northsouth(glm::vec3(0.4f, 0.0f, 0.0f));
	spawn_walls_northsouth(glm::vec3(0.4f, 0.0f, 6.4f));
	spawn_walls_northsouth(glm::vec3(0.4f, 0.0f, 12.8f));
	spawn_walls_northsouth(glm::vec3(0.4f, 0.0f, 19.2f));
	spawn_walls_northsouth(glm::vec3(0.4f, 0.0f, 25.6f));
	spawn_walls_northsouth(glm::vec3(0.4f, 0.0f, 32.0));
	spawn_walls_northsouth(glm::vec3(0.4f, 0.0f, 38.4f));
	spawn_walls_northsouth(glm::vec3(0.4f, 0.0f, 44.8f));
	// ===> SOUTH WALL
	spawn_walls_westeast(glm::vec3(0.0f, 0.0f, 50.8f));
	spawn_walls_westeast(glm::vec3(6.4f, 0.0f, 50.8f));
	spawn_walls_westeast(glm::vec3(12.8f, 0.0f, 50.8f));
	spawn_walls_westeast(glm::vec3(19.2f, 0.0f, 50.8f));
	spawn_walls_westeast(glm::vec3(25.6f, 0.0f, 50.8f));
	spawn_walls_westeast(glm::vec3(32.0f, 0.0f, 50.8f));
	spawn_walls_westeast(glm::vec3(38.4f, 0.0f, 50.8f));
	spawn_walls_westeast(glm::vec3(44.8f, 0.0f, 50.8f));
	// ===> inner walls and stairs
	// slope wall
	scene.SpawnInstance(SM_brick_building_10.GetModelID(),
						glm::vec3(23.0f, -0.15f, 3.4f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_brick_building_16.GetModelID(),
						glm::vec3(21.3f, 0.0f, 3.4f), // pos
						glm::vec3(0.01f * 0.5f, 0.01f * 0.5f, 0.01f * 0.625f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_brick_building_07.GetModelID(),
						glm::vec3(21.3f, 0.75f, 3.4f), // pos
						glm::vec3(0.01f * 0.5, 0.01f * 0.625f, 0.01f * 0.75f),
						glm::vec3(-90, 0, 0)); // size
	// start straight walls
	for(int32 i = 1; i <= 7; i++)
	{
		scene.SpawnInstance(SM_brick_building_17.GetModelID(),
							glm::vec3(23.0f + 3.2f * i, -0.15f, 3.4f), // pos 
							glm::vec3(0.01f),
							glm::vec3(-90, 0, 0)); // size
	}
	// stair
	scene.SpawnInstance(SM_stairs_01.GetModelID(),
						glm::vec3(23.0f, 0.05f, 1.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	// flat walk way
	scene.SpawnInstance(SM_brick_building_13.GetModelID(),
						glm::vec3(23.0f + 3.2f + 9.8f, 1.5f, 1.75), // pos 
						glm::vec3(0.01f * 7.125f, 0.01f * 0.875f, 0.01f),
						glm::vec3(-90, 0, 0)); // size
	// corner walls(near containers)
	for(int32 i = 0; i < 3; i++)
	{
		scene.SpawnInstance(SM_brick_building_17.GetModelID(),
							glm::vec3(23.0f + 3.2f * 7 + 1.7f, -0.15f, 3.4f + 1.7f + 3.2f * i), // pos 
							glm::vec3(0.01f),
							glm::vec3(-90, -90, 0)); // size
	}
	// first pillar
	scene.SpawnInstance(SM_brick_building_08.GetModelID(),
						glm::vec3(23.0f + 3.2f * 7 + 1.7f, 0.05f, 3.4f), // pos 
						glm::vec3(0.01f * 0.5f, 0.01f * 0.5f, 0.01f * 0.5f),
						glm::vec3(-90, -90, 0)); // size
	scene.SpawnInstance(SM_brick_building_07.GetModelID(),
						glm::vec3(23.0f + 3.2f * 7 + 1.7f, 1.50f, 3.4f), // pos 
						glm::vec3(0.01f * 0.625f, 0.01f * 0.625f, 0.01f * 0.75f),
						glm::vec3(-90, -90, 0)); // size
	// last pillar
	scene.SpawnInstance(SM_brick_building_08.GetModelID(),
						glm::vec3(23.0f + 3.2f * 7 + 1.7f, 0.05f, 3.4f + 1.7f + 3.2f * 2 + 1.7f), // pos 
						glm::vec3(0.01f * 0.5f, 0.01f * 0.5f, 0.01f * 0.5f),
						glm::vec3(-90, -90, 0)); // size
	scene.SpawnInstance(SM_brick_building_07.GetModelID(),
						glm::vec3(23.0f + 3.2f * 7 + 1.7f, 1.50f, 3.4f + 1.7f + 3.2f * 2 + 1.7f), // pos 
						glm::vec3(0.01f * 0.625f, 0.01f * 0.625f, 0.01f * 0.75f),
						glm::vec3(-90, -90, 0)); // size
	scene.SpawnInstance(SM_brick_building_17.GetModelID(),
						glm::vec3(23.0f + 3.2f * 7 + 1.7f + 1.9f, -0.15f, 3.4f + 1.7f + 3.2f * 2 + 1.7f), // pos 
						glm::vec3(0.01f * 1.25f, 0.01f * 1.0f, 0.01f * 1.0f),
						glm::vec3(-90, 0, 0)); // size
	// flat walk way
	scene.SpawnInstance(SM_brick_building_13.GetModelID(),
						glm::vec3(23.0f + 3.2f + 9.8f + 13.3f, 1.5f, 1.75 + 4.7f), // pos 
						glm::vec3(0.01f * 1.25f, 0.01f * 4.0f, 0.01f * 1.0f),
						glm::vec3(-90, 0, 0)); // size

}

void spawn_walls_westeast(glm::vec3 origin)
{
	// body
	scene.SpawnInstance(SM_brick_building_01.GetModelID(),
						glm::vec3(origin.x + 1.6f, 0.0f, origin.z), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	// slap
	scene.SpawnInstance(SM_brick_building_04.GetModelID(),
						glm::vec3(origin.x + 1.6f, 3.1f, origin.z), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	// slap
	scene.SpawnInstance(SM_brick_building_04.GetModelID(),
						glm::vec3(origin.x + 4.8f, 3.1f, origin.z), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	// fence
	scene.SpawnInstance(SM_brick_building_08.GetModelID(),
						glm::vec3(origin.x + 0.4f, 0.0f, origin.z), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	// fence top
	scene.SpawnInstance(SM_brick_building_07.GetModelID(),
						glm::vec3(origin.x + 0.4f, 3.1f, origin.z), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	// body
	scene.SpawnInstance(SM_brick_building_01.GetModelID(),
						glm::vec3(origin.x + 1.6f + 3.2f, 0.0f, origin.z), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
}

void spawn_walls_northsouth(glm::vec3 origin)
{
	// body
	scene.SpawnInstance(SM_brick_building_01.GetModelID(),
						glm::vec3(origin.x, 0.0f, origin.z + 1.6f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 90, 0)); // size
	// slap
	scene.SpawnInstance(SM_brick_building_04.GetModelID(),
						glm::vec3(origin.x, 3.1f, origin.z + 1.6f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, -90, 0)); // size
	// slap
	scene.SpawnInstance(SM_brick_building_04.GetModelID(),
						glm::vec3(origin.x, 3.1f, origin.z + 4.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, -90, 0)); // size
	// fence
	scene.SpawnInstance(SM_brick_building_08.GetModelID(),
						glm::vec3(origin.x, 0.0f, origin.z), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, -90, 0)); // size
	// fence top
	scene.SpawnInstance(SM_brick_building_07.GetModelID(),
						glm::vec3(origin.x, 3.1f, origin.z), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	// body
	scene.SpawnInstance(SM_brick_building_01.GetModelID(),
						glm::vec3(origin.x,0.0f, origin.z + 1.6f + 3.2f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, -90, 0)); // size
}


void spawn_rails_crane_models_instances()
{
	// rails
	scene.SpawnInstance(SM_stop_rails.GetModelID(),
						glm::vec3(19.41f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // rotation
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(21.0f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(24.0f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(27.0f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(30.0f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(33.0f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(36.0f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(39.0f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_stop_rails.GetModelID(),
						glm::vec3(40.75f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // rotation
	// rails 2
	scene.SpawnInstance(SM_stop_rails.GetModelID(),
						glm::vec3(19.41f, 0.0f, 6.5f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // rotation
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(21.0f, 0.0f, 6.5f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(24.0f, 0.0f, 6.5f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(27.0f, 0.0f, 6.5f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(30.0f, 0.0f, 6.5f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(33.0f, 0.0f, 6.5f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(36.0f, 0.0f, 6.5f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_rails.GetModelID(),
						glm::vec3(39.0f, 0.0f, 6.5f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_stop_rails.GetModelID(),
						glm::vec3(40.75f, 0.0f, 6.5f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // rotation
	// crane
	scene.SpawnInstance(SM_gantry_crane_01.GetModelID(),
						glm::vec3(24.0f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_gantry_crane_01.GetModelID(),
						glm::vec3(24.0f, 0.0f, 6.6f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_gantry_crane_02.GetModelID(),
						glm::vec3(24.0f, 0.7f, 7.65f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_gantry_crane_01.GetModelID(),
						glm::vec3(30.2f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_gantry_crane_01.GetModelID(),
						glm::vec3(30.2f, 0.0f, 6.6f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_gantry_crane_02.GetModelID(),
						glm::vec3(30.2f, 0.7f, 7.65f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
	scene.SpawnInstance(SM_gantry_crane_03.GetModelID(),
						glm::vec3(27.35f, 0.0f, 8.8f), // pos
						glm::vec3(0.01f),
						glm::vec3(-90, 0, 0)); // size
}
void load_containers_models()
{
	SM_container_02.LoadFromFile(industrialAreaAssetsPath + "meshes/containers/SM_container_02.FBX");
	M_container_02_A.LoadTextures();
	M_corrugated_metal_B.LoadTextures();
	M_sheet_steel_A.LoadTextures();
	M_floor_covering.LoadTextures();
	SM_container_02.AssignMaterial(&M_corrugated_metal_B);
	SM_container_02.AssignMaterial(&M_container_02_A);
	SM_container_02.AssignMaterial(&M_floor_covering);
	SM_container_02.AssignMaterial(&M_sheet_steel_A);
	SM_container_02.AssignMaterial(&M_sheet_steel_A);
	SM_container_02.AssignMaterial(&M_container_02_A);

	scene.RegisterModel(&SM_container_02);
	scene.RegisterModel(&SM_container_01);
}
void spawn_containers_models_instances()
{
	scene.SpawnInstance(SM_container_02.GetModelID(),
						glm::vec3(23.0f + 3.2f * 7, 0.0f, 3.4f + 1.7f + 3.2f * 2 + 5.7f), // pos 
						glm::vec3(0.01f),
						glm::vec3(-90, -90, 0)); // size
}
