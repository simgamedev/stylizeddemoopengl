#include "Screen.h"

#include "input/Keyboard.h"
#include "input/Mouse.h"

uint32 Screen::SCR_WIDTH = 1680;
uint32 Screen::SCR_HEIGHT = 1050;

void 
Screen::FramebufferSizeCallback(GLFWwindow* window, int32 width, int32 height)
{
	glViewport(0, 0, width, height);

	SCR_WIDTH = width;
	SCR_HEIGHT = height;
}


Screen::Screen()
	: pWindow_(nullptr)
{
}

bool
Screen::Init()
{
	pWindow_ = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "StylizedDemoOpenGL", NULL, NULL);
	if(!pWindow_)
	{
		return false;
	}

	glfwMakeContextCurrent(pWindow_);
	return true;
}

void
Screen::SetParameters()
{
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);

	glfwSetFramebufferSizeCallback(pWindow_, Screen::FramebufferSizeCallback);

	glfwSetKeyCallback(pWindow_, Keyboard::KeyCallback);

	glfwSetInputMode(pWindow_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glfwSetCursorPosCallback(pWindow_, Mouse::CursorPosCallback);
	glfwSetMouseButtonCallback(pWindow_, Mouse::MouseButtonCallback);
	glfwSetScrollCallback(pWindow_, Mouse::MouseWheelCallback);

}

void
Screen::BeginDraw()
{
	glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void
Screen::EndDraw()
{
	glfwSwapBuffers(pWindow_);
	glfwPollEvents();
}

bool
Screen::ShouldClose()
{
	return glfwWindowShouldClose(pWindow_);
}

void
Screen::SetShouldClose(bool shouldClose)
{
	glfwSetWindowShouldClose(pWindow_, shouldClose);
}
