#version 330 core
out vec4 FragColor;


struct MaterialLand {
	int numLayers;
	sampler2D blendMap;
	sampler2D diffuseLayer1;
	sampler2D specularLayer1;
	sampler2D normalLayer1;
	sampler2D diffuseLayer2;
	sampler2D specularLayer2;
	sampler2D normalLayer2;
	sampler2D diffuseLayer3;
	sampler2D specularLayer3;
	sampler2D normalLayer3;
	sampler2D diffuseLayer4;
	sampler2D specularLayer4;
	sampler2D normalLayer4;
	bool bSkipNormalMap;
};

uniform bool hasTransparency;
uniform MaterialLand material;
uniform vec2 uvScale;

//uniform vec3 viewPos;
// ===< UNIFORMS

/********************************************************************************
 * UBO/LIGHTING
 ********************************************************************************/
struct DirLight {
	//float strength;

	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define MAX_POINT_LIGHTS 10
struct PointLight {
	//float strength;
	vec3 position;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define MAX_SPOT_LIGHTS 2
struct SpotLight {
	vec3 position;
	vec3 direction;

	float cutOff;
	float outerCutoff;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

layout (std140) uniform Lights
{
	DirLight dirLight;
	int numPointLights;
	PointLight pointLights[MAX_POINT_LIGHTS];
	int numSpotLights;
	SpotLight spotLights[MAX_SPOT_LIGHTS];
};
/********************************************************************************
 ********************************************************************************/

struct TanSpaceData {
	vec3 fragPos;
	vec3 viewPos;
	vec3 dirLightDirection;
};
// ===> INPUT
in VS_OUT {
	vec3 fragPos;
	vec2 texCoord;
    vec3 normal;
	vec3 viewPos;
	vec2 uvScale;

	TanSpaceData tanSpace;
} fs_in;
// ===< INPUT

vec3 CalcDirLight(DirLight light, vec3 fragNorm, vec3 viewDir, vec3 fragDiffuse, vec3 fragSpecular);
vec3 CalcPointLight(PointLight light, vec3 fragNorm, vec3 viewDir, vec3 fragPos, vec3 fragDiffuse, vec3 fragSpecular);
vec3 CalcSpotLight(SpotLight light, vec3 fragNorm, vec3 viewDir, vec3 fragPos, vec3 fragDiffuse, vec3 fragSpecular);

void main()
{
	vec3 normLayer1 = texture(material.normalLayer1, fs_in.texCoord).rgb;
	vec3 normLayer2 = texture(material.normalLayer2, fs_in.texCoord).rgb;
	vec3 normLayer3 = texture(material.normalLayer3, fs_in.texCoord).rgb;
	vec3 normLayer4 = texture(material.normalLayer4, fs_in.texCoord).rgb;
	normLayer1 = normalize(normLayer1 * 2.0 - 1.0);
	normLayer2 = normalize(normLayer2 * 2.0 - 1.0);
	normLayer3 = normalize(normLayer3 * 2.0 - 1.0);
	normLayer4 = normalize(normLayer4 * 2.0 - 1.0);
	vec3 viewDir;
	viewDir = normalize(fs_in.tanSpace.viewPos - fs_in.tanSpace.fragPos);


	// ====================> SAMPLEING INFO FROM MATERIAL/TEXTURES
	vec3 fragDiffuse;
	float fragAlpha;
	vec3 fragSpecular;
	// ===> NOTE: in blendMap, black == Layer1, r == Layer2, g == Layer3, b == Layer4
	// ===> NOTE: blendMap's uvs shoud not be scaled!!
	vec2 texCoordBlendMap = vec2(fs_in.texCoord.x * uvScale.x,
								 fs_in.texCoord.y * uvScale.y);
	vec4 fragBlendMap = texture(material.blendMap, texCoordBlendMap);
	float layer1Amount = 1 - (fragBlendMap.r + fragBlendMap.g + fragBlendMap.b);
	float layer2Amount = fragBlendMap.r;
	float layer3Amount = fragBlendMap.g;
	float layer4Amount = fragBlendMap.b;

	vec3 diffuseLayer1 = vec3(texture(material.diffuseLayer1, fs_in.texCoord));
	vec3 diffuseLayer2 = vec3(texture(material.diffuseLayer2, fs_in.texCoord));
	vec3 diffuseLayer3 = vec3(texture(material.diffuseLayer3, fs_in.texCoord));
	vec3 diffuseLayer4 = vec3(texture(material.diffuseLayer4, fs_in.texCoord));

	vec3 specularLayer1 = vec3(texture(material.specularLayer1, fs_in.texCoord));
	vec3 specularLayer2 = vec3(texture(material.specularLayer2, fs_in.texCoord));
	vec3 specularLayer3 = vec3(texture(material.specularLayer3, fs_in.texCoord));
	vec3 specularLayer4 = vec3(texture(material.specularLayer4, fs_in.texCoord));


	// ===< SAMPLEING COLOR FROM MATERIAL/TEXTURES

	// ===> 1: CALC DIRECTIONAL LIGHT
	vec3 resultLayer1 = CalcDirLight(dirLight, normLayer1, viewDir, diffuseLayer1, specularLayer1);
	vec3 resultLayer2 = CalcDirLight(dirLight, normLayer2, viewDir, diffuseLayer2, specularLayer2);
	vec3 resultLayer3 = CalcDirLight(dirLight, normLayer3, viewDir, diffuseLayer3, specularLayer3);
	vec3 resultLayer4 = CalcDirLight(dirLight, normLayer4, viewDir, diffuseLayer4, specularLayer4);

	vec3 diffuseFinal = diffuseLayer1 * layer1Amount + diffuseLayer2 * layer2Amount + diffuseLayer3 * layer3Amount +
		diffuseLayer4 * layer4Amount;
	FragColor = vec4(diffuseFinal, 1.0f);
	//FragColor = vec4(layer3Amount, layer3Amount, layer3Amount, 1.0f);
	// ===< 1: CALC DIRECTIONAL LIGHT

	/*
	// ===> 2: CALC POINT LIGHTS
	for(int i = 0; i < numPointLights; i++)
	{
		result += CalcPointLight(pointLights[i], norm, viewDir, fs_in.fragPos, fragDiffuse, fragSpecular);
	}
	// ===< 2: CALC POINT LIGHTS
	// ===> 3: CALC SPOT LIGHTS
	for(int i = 0; i < numSpotLights; i++)
	{
		result += CalcSpotLight(spotLights[i], norm, viewDir, fs_in.fragPos, fragDiffuse, fragSpecular);
	}
	// ===< 3: CALC SPOT LIGHTS
	if(hasTransparency && fragAlpha < 0.5)
		discard;
	else
		FragColor = vec4(result, 1.0f);
	*/
}  


vec3 CalcDirLight(DirLight light, vec3 fragNorm, vec3 viewDir, vec3 fragDiffuse, vec3 fragSpecular)
{
	vec3 lightDir;
	lightDir = normalize(-fs_in.tanSpace.dirLightDirection);
	// ambient
	// diffuse
	float diffuseStr = max(dot(fragNorm, lightDir), 0.0);
	// specuar
	vec3 reflectDir = reflect(-lightDir, fragNorm);
	float shininess;
	shininess = fragSpecular.r;
	shininess = 0.1;
	float specularStr = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
	// ===> RESULT
	vec3 ambient =  light.ambient * fragDiffuse;
	vec3 diffuse =  light.diffuse * diffuseStr * fragDiffuse;
	vec3 specular = light.specular * specularStr * fragSpecular;
	return (ambient + diffuse + specular);
	// ===< RESULT
}

/*
vec3 CalcPointLight(PointLight light, vec3 fragNorm, vec3 viewDir, vec3 fragPos, vec3 fragDiffuse, vec3 fragSpecular)
{
	vec3 lightDir = normalize(light.position - fragPos);
	// ambient
	// diffuse
	float diffuseStr = max(dot(fragNorm, lightDir), 0.0);
	// specular
	vec3 reflectDir = reflect(-lightDir, fragNorm);
	float shininess;
	if(noTex)
		shininess = materialSimple.shininess;
	else
		// FIXME: use specular map
		shininess = material.shininess;
	float specularStr = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
	// attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance +
							   light.quadratic * (distance * distance));

	// ===> RESULT
	vec3 ambient = light.ambient * fragDiffuse;
	vec3 diffuse = light.diffuse * diffuseStr * fragDiffuse;
	vec3 specular = light.specular * specularStr * fragSpecular;
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;
	return (ambient + diffuse + specular);
	// ===< RESULT
}

vec3 CalcSpotLight(SpotLight light, vec3 fragNorm, vec3 viewDir, vec3 fragPos, vec3 fragDiffuse, vec3 fragSpecular)
{
	vec3 lightDir = normalize(light.position - fragPos);
	// ambient
	// diffuse
	float diffuseStr = max(dot(fragNorm, lightDir), 0.0f);
	// specular
	vec3 reflectDir = reflect(-lightDir, fragNorm);
	float shininess;
	if(noTex)
		shininess = materialSimple.shininess;
	else
		shininess = material.shininess;
	float specularStr = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
	// ===> ATTENUATION
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	// ===< ATTENUATION
	// ===> INTENSITY
	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutOff - light.outerCutoff;
	float intensity = clamp((theta - light.outerCutoff) / epsilon, 0.0, 1.0);
	// ===< INTENSITY

	// ===> RESULT
	vec3 ambient = light.ambient * fragDiffuse;
	vec3 diffuse = light.diffuse * diffuseStr * fragDiffuse;
	vec3 specular = light.specular * specularStr * fragSpecular;
	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;
	return (ambient + diffuse + specular);
	// ===< RESULT
}
*/
