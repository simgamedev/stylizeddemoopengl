#version 330 core
out vec4 FragColor;

// ===> UNIFORMS
struct Material {
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

struct MaterialTexed {
	sampler2D diffuse0;
	sampler2D specular0;
	sampler2D normal0;
	float shininess;
	bool noNormalMap;
	bool bSkipNormalMap;
};
uniform bool noTex;
uniform Material material;
uniform MaterialTexed materialTexed;
//uniform vec3 viewPos;
// ===< UNIFORMS

/********************************************************************************
 * UBO/LIGHTING
 ********************************************************************************/
struct DirLight {
	//float strength;

	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define MAX_POINT_LIGHTS 10
struct PointLight {
	//float strength;
	vec3 position;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define MAX_SPOT_LIGHTS 2
struct SpotLight {
	vec3 position;
	vec3 direction;

	float cutOff;
	float outerCutoff;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

layout (std140) uniform Lights
{
	DirLight dirLight;
	int numPointLights;
	PointLight pointLights[MAX_POINT_LIGHTS];
	int numSpotLights;
	SpotLight spotLights[MAX_SPOT_LIGHTS];
};
/********************************************************************************
 ********************************************************************************/

struct TanSpaceData {
	vec3 fragPos;
	vec3 viewPos;
	vec3 dirLightDirection;
};
// ===> INPUT
in VS_OUT {
	vec3 fragPos;
	vec2 texCoord;
    vec3 normal;
	vec3 viewPos;

	TanSpaceData tanSpace;
} fs_in;
// ===< INPUT

vec3 CalcDirLight(DirLight light, vec3 fragNorm, vec3 viewDir, vec3 fragDiffuse, vec3 fragSpecular);
vec3 CalcPointLight(PointLight light, vec3 fragNorm, vec3 viewDir, vec3 fragPos, vec3 fragDiffuse, vec3 fragSpecular);
vec3 CalcSpotLight(SpotLight light, vec3 fragNorm, vec3 viewDir, vec3 fragPos, vec3 fragDiffuse, vec3 fragSpecular);

void main()
{
	vec3 norm = normalize(fs_in.normal);
	vec3 viewDir;
	if(materialTexed.noNormalMap)
		viewDir = normalize(fs_in.viewPos - fs_in.fragPos);
	else
		viewDir = normalize(fs_in.tanSpace.viewPos - fs_in.tanSpace.fragPos);
	// ===> SAMPLEING INFO FROM MATERIAL/TEXTURES
	vec3 fragDiffuse;
	vec3 fragSpecular;
	if(noTex)
	{
		fragDiffuse = vec3(material.diffuse);
		fragSpecular = vec3(material.specular);
	}
	else
	{
		fragDiffuse = vec3(texture(materialTexed.diffuse0, fs_in.texCoord));
		fragSpecular = vec3(texture(materialTexed.specular0, fs_in.texCoord));
		fragSpecular = vec3(0.1, 0.1, 0.1);
		if(!materialTexed.noNormalMap &&
		   !materialTexed.bSkipNormalMap)
		{
			// NOTE: norm is now in tangent space
			norm = texture(materialTexed.normal0, fs_in.texCoord).rgb;
			norm = normalize(norm * 2.0 - 1.0);
		}
	}
	// ===< SAMPLEING COLOR FROM MATERIAL/TEXTURES

	// ===> 1: CALC DIRECTIONAL LIGHT
	vec3 result = CalcDirLight(dirLight, norm, viewDir, fragDiffuse, fragSpecular);
	// ===< 1: CALC DIRECTIONAL LIGHT

	// ===> 2: CALC POINT LIGHTS
	for(int i = 0; i < numPointLights; i++)
	{
		result += CalcPointLight(pointLights[i], norm, viewDir, fs_in.fragPos, fragDiffuse, fragSpecular);
	}
	// ===< 2: CALC POINT LIGHTS
	// ===> 3: CALC SPOT LIGHTS
	for(int i = 0; i < numSpotLights; i++)
	{
		result += CalcSpotLight(spotLights[i], norm, viewDir, fs_in.fragPos, fragDiffuse, fragSpecular);
	}
	// ===< 3: CALC SPOT LIGHTS
	FragColor = vec4(result, 1.0f);
}  


vec3 CalcDirLight(DirLight light, vec3 fragNorm, vec3 viewDir, vec3 fragDiffuse, vec3 fragSpecular)
{
	vec3 lightDir;
	if(!noTex && materialTexed.noNormalMap)
		lightDir = normalize(-light.direction);
	else
		lightDir = normalize(-fs_in.tanSpace.dirLightDirection);
	// ambient
	// diffuse
	float diffuseStr = max(dot(fragNorm, lightDir), 0.0);
	// specuar
	vec3 reflectDir = reflect(-lightDir, fragNorm);
	float shininess;
	if(noTex)
		shininess = material.shininess;
	else
		shininess = materialTexed.shininess;
	float specularStr = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
	// ===> RESULT
	vec3 ambient =  light.ambient * fragDiffuse;
	vec3 diffuse =  light.diffuse * diffuseStr * fragDiffuse;
	vec3 specular = light.specular * specularStr * fragSpecular;
	return (ambient + diffuse + specular);
	// ===< RESULT
}

vec3 CalcPointLight(PointLight light, vec3 fragNorm, vec3 viewDir, vec3 fragPos, vec3 fragDiffuse, vec3 fragSpecular)
{
	vec3 lightDir = normalize(light.position - fragPos);
	// ambient
	// diffuse
	float diffuseStr = max(dot(fragNorm, lightDir), 0.0);
	// specular
	vec3 reflectDir = reflect(-lightDir, fragNorm);
	float shininess;
	if(noTex)
		shininess = material.shininess;
	else
		shininess = materialTexed.shininess;
	float specularStr = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
	// attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance +
							   light.quadratic * (distance * distance));

	// ===> RESULT
	vec3 ambient = light.ambient * fragDiffuse;
	vec3 diffuse = light.diffuse * diffuseStr * fragDiffuse;
	vec3 specular = light.specular * specularStr * fragSpecular;
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;
	return (ambient + diffuse + specular);
	// ===< RESULT
}

vec3 CalcSpotLight(SpotLight light, vec3 fragNorm, vec3 viewDir, vec3 fragPos, vec3 fragDiffuse, vec3 fragSpecular)
{
	vec3 lightDir = normalize(light.position - fragPos);
	// ambient
	// diffuse
	float diffuseStr = max(dot(fragNorm, lightDir), 0.0f);
	// specular
	vec3 reflectDir = reflect(-lightDir, fragNorm);
	float shininess;
	if(noTex)
		shininess = material.shininess;
	else
		shininess = materialTexed.shininess;
	float specularStr = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
	// ===> ATTENUATION
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	// ===< ATTENUATION
	// ===> INTENSITY
	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutOff - light.outerCutoff;
	float intensity = clamp((theta - light.outerCutoff) / epsilon, 0.0, 1.0);
	// ===< INTENSITY

	// ===> RESULT
	vec3 ambient = light.ambient * fragDiffuse;
	vec3 diffuse = light.diffuse * diffuseStr * fragDiffuse;
	vec3 specular = light.specular * specularStr * fragSpecular;
	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;
	return (ambient + diffuse + specular);
	// ===< RESULT
}
