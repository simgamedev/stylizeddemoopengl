#version 330 core
// ===> ATTRIBUTES LOCATIONS
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;
layout (location = 3) in vec3 aTangent;
layout (location = 4) in mat4 aInstanceModelMatrix;
//layout (location = 5) in mat4 aInstanceModelMatrix; //MAT4S TAKE 4 LOCATIONS
//layout (location = 6) in mat4 aInstanceModelMatrix;
//layout (location = 7) in mat4 aInstanceModelMatrix;
layout (location = 8) in mat3 aInstanceNormalMatrix; //MAT3 TAKES 3 LOCATIONS
//layout (location = 9) in mat4 aInstanceNormalModelMatrix;
//layout (location = 10) in mat4 aInstanceNormalModelMatrix;
// ===< ATTRIBUTES LOCATIONS

// ===> UNIFORMS
uniform mat4 view;
uniform mat4 projection;
uniform mat3 normalMatrix;
uniform vec3 viewPos;
uniform vec2 uvScale;
// ===< UNIFORMS


/********************************************************************************
 * UBO/LIGHTING
 ********************************************************************************/
struct DirLight {
	//float strength;

	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define MAX_POINT_LIGHTS 10
struct PointLight {
	//float strength;
	vec3 position;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define MAX_SPOT_LIGHTS 2
struct SpotLight {
	vec3 position;
	vec3 direction;

	float cutOff;
	float outerCutoff;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

layout (std140) uniform Lights
{
	DirLight dirLight;
	int numPointLights;
	PointLight pointLights[MAX_POINT_LIGHTS];
	int numSpotLights;
	SpotLight spotLights[MAX_SPOT_LIGHTS];
};
/********************************************************************************
 ********************************************************************************/

struct TanSpaceData {
	vec3 fragPos;
	vec3 viewPos;
	vec3 dirLightDirection;
};
// ===> OUTPUT
out VS_OUT {
	vec3 fragPos;
	vec2 texCoord;
    vec3 normal;
	vec3 viewPos;
	vec2 uvScale;

	TanSpaceData tanSpace;
} vs_out;
// ===> OUTPUT


void main()
{
	// ===> ModelViewProjection
	gl_Position = projection * view * aInstanceModelMatrix * vec4(aPos, 1.0f);
	// ===< ModelViewProjectino

	// ===========> VS_OUT
	vs_out.texCoord = aTexCoord / uvScale;
	vs_out.uvScale = uvScale;
	vs_out.fragPos = vec3(aInstanceModelMatrix * vec4(aPos, 1.0f));
	vs_out.normal = aInstanceNormalMatrix * aNormal;
	vs_out.viewPos = viewPos;

	// ===> TANGENT SPACE
	vec3 T = normalize(aInstanceNormalMatrix * aTangent);
	vec3 N = normalize(aInstanceNormalMatrix * aNormal);
	T = normalize(T - dot(T, N) * N);
	vec3 B = cross(N, T);
	mat3 TBN = transpose(mat3(T, B, N));

	vs_out.tanSpace.dirLightDirection = TBN * dirLight.direction;
	vs_out.tanSpace.viewPos = TBN * viewPos;
	vs_out.tanSpace.fragPos = TBN * vs_out.fragPos;
	// ===< TANGENT SPACE
}
