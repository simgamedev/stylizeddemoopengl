#version 330 core

out vec4 FragColor;

in vec3 instanceSize;

uniform vec3 lightColor;

void main()
{
	if(instanceSize.x == 10.0f)
		FragColor = vec4(1.0f, 1.0f, 0.0f, 1.0f);
	else
		FragColor = vec4(lightColor, 1.0f);
}
